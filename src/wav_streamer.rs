use std::marker::PhantomData;
use std::path::PathBuf;

use hound;
use num;

use crate::audio_streamer::AudioStreamer;
use crate::control_messages::AudioStreamerControlMessage;
use crate::expiring_mpsc::{Receiver, TryRecvError};
use crate::mixer::Mixer;

const TEMP_HARDCODED_SAMPLE_RATE: u32 = 44100;

// currently only works with f32 samples

pub struct WavStreamer<T> {
    spec: hound::WavSpec,
    out_path: PathBuf,
    phantom: PhantomData<T>,
    control_receiver: Receiver<AudioStreamerControlMessage<T>>,
}

impl<T> WavStreamer<T>
where
    T: num::ToPrimitive + num::FromPrimitive + num::Num + Copy,
{
    pub fn new(
        out_path: PathBuf,
        control_receiver: Receiver<AudioStreamerControlMessage<T>>,
    ) -> WavStreamer<T> {
        // For now, we use some hardcoded settings
        let spec = hound::WavSpec {
            channels: 1,
            sample_rate: TEMP_HARDCODED_SAMPLE_RATE,
            bits_per_sample: 32,
            sample_format: hound::SampleFormat::Float,
        };
        WavStreamer {
            spec,
            out_path,
            control_receiver,
            phantom: PhantomData,
        }
    }
}

impl<T: 'static> AudioStreamer<T> for WavStreamer<T>
where
    T: num::ToPrimitive + num::FromPrimitive + num::Num + Copy,
{
    fn stream(&self, mut mixer: Mixer<T>) {
        let mut writer = hound::WavWriter::create(&self.out_path, self.spec).unwrap();
        let mut expected_max_amp = 0f32;
        loop {
            match mixer.mix_chunk(expected_max_amp) {
                Ok(samples) => {
                    for sample in samples {
                        writer.write_sample(sample.to_f32().unwrap()).unwrap();
                    }
                }
                Err(error) => {
                    error!(
                        "Got error when mixing samples for audio streaming: {:?}",
                        error
                    );
                    break;
                }
            }
            match self.control_receiver.try_recv() {
                Ok(message) => {
                    match message {
                        AudioStreamerControlMessage::Shutdown => {
                            break;
                        }
                        AudioStreamerControlMessage::AdjustExpectedMaxAmp { delta_transition } => {
                            info!("Got AdjustExpectedMaxAmp message. Note that WavStreamer doesn't \
                              support gradual amp transitions. Immediately applying the full delta.");
                            expected_max_amp += delta_transition.end_val;
                        }
                        AudioStreamerControlMessage::DisconnectLayer { id } => {
                            mixer.layer_samples_receivers.remove(&id);
                        }
                        AudioStreamerControlMessage::ConnectLayer {
                            id,
                            layer_samples_receiver,
                        } => {
                            mixer
                                .layer_samples_receivers
                                .insert(id, layer_samples_receiver);
                        }
                    }
                }
                Err(e) => match e {
                    TryRecvError::Empty => (),
                    TryRecvError::Disconnected => {
                        break;
                    }
                },
            }
        }
    }
}
