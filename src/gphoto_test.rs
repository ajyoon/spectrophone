extern crate gphoto;
extern crate stopwatch;

use std::process::Command;

fn main() {
    let mut context = gphoto::Context::new().unwrap();
    let mut camera = gphoto::Camera::autodetect(&mut context).unwrap();
    for _ in 0..10 {
        let sw = stopwatch::Stopwatch::start_new();
        let capture = camera.capture_image_preview(&mut context).unwrap();
        println!("elapsed: {:?}", sw.elapsed());
        Command::new("eog")
            .args(&[capture.path().to_str().unwrap()])
            .output()
            .expect("Failed to preview image");
    }
}
