use std::collections::HashMap;

use num;

use crate::expiring_mpsc::Receiver;
use crate::ids::LayerId;

const EMPTY_CHUNK_LENGTH: usize = 1024;

#[inline]
fn add_chunk_to_maybe_empty<T>(src: &Vec<T>, dest: &mut Vec<T>) -> Result<(), MixerError>
where
    T: num::ToPrimitive + num::FromPrimitive + num::Num + Copy,
{
    if dest.is_empty() {
        dest.reserve_exact(src.len());
        for _ in 0..src.len() {
            dest.push(T::from_f32(0.).unwrap());
        }
    } else {
        if !(dest.len() == src.len()) {
            return Err(MixerError);
        }
    }
    for (i, sample) in src.iter().enumerate() {
        unsafe {
            *dest.get_unchecked_mut(i) = *dest.get_unchecked_mut(i) + *sample;
        }
    }
    Ok(())
}

#[inline]
pub fn add_chunk_to<T>(src: &Vec<T>, dest: &mut Vec<T>)
where
    T: num::ToPrimitive + num::FromPrimitive + num::Num + Copy,
{
    debug_assert_eq!(dest.len(), src.len());
    for (i, sample) in src.iter().enumerate() {
        unsafe {
            *dest.get_unchecked_mut(i) = *dest.get_unchecked_mut(i) + *sample;
        }
    }
}

#[inline]
fn compress<T>(uncompressed_samples: &mut Vec<T>, expected_max_amp: T)
where
    T: num::ToPrimitive + num::FromPrimitive + num::Num + Copy,
{
    for sample in uncompressed_samples {
        *sample = (*sample) / expected_max_amp;
    }
}

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct MixerError;

pub struct Mixer<T> {
    pub layer_samples_receivers: HashMap<LayerId, Receiver<Vec<T>>>,
}

impl<T> Mixer<T> {
    pub fn new() -> Mixer<T> {
        let layer_samples_receivers = HashMap::new();
        Mixer {
            layer_samples_receivers,
        }
    }
}

impl<T> Mixer<T>
where
    T: num::ToPrimitive + num::FromPrimitive + num::Num + Copy,
{
    pub fn mix_chunk(&self, expected_max_amp: f32) -> Result<Vec<T>, MixerError> {
        if self.layer_samples_receivers.is_empty() {
            return Ok(Mixer::no_data_chunk());
        }
        let mut combined_samples: Vec<T> = Vec::new();
        for receiver in self.layer_samples_receivers.values() {
            match receiver.recv() {
                Ok(chunk) => add_chunk_to_maybe_empty(&chunk, &mut combined_samples)?,
                Err(_) => return Err(MixerError),
            }
        }
        compress(
            &mut combined_samples,
            T::from_f32(expected_max_amp).unwrap(),
        );
        Ok(combined_samples)
    }

    fn no_data_chunk() -> Vec<T> {
        vec![T::zero(); EMPTY_CHUNK_LENGTH]
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_utils::*;

    #[test]
    fn test_add_chunk_to_maybe_empty_from_empty() {
        let mut dest = vec![];
        let src = vec![1., 2.];
        add_chunk_to_maybe_empty(&src, &mut dest).unwrap();
        assert_almost_eq_by_element(dest, vec![1., 2.]);
    }

    #[test]
    fn test_add_chunk_to_from_same_size() {
        let mut dest = vec![1.1, 2.2];
        let src = vec![1., 2.];
        add_chunk_to(&src, &mut dest);
        assert_almost_eq_by_element(dest, vec![2.1, 4.2]);
    }

    #[test]
    fn test_compress() {
        let mut samples = vec![-100., 0., 100.];
        compress(&mut samples, 100.);

        assert_almost_eq_by_element(samples, vec![-1., 0., 1.]);
    }
}

#[cfg(test)]
mod benchmarks {
    extern crate rand;
    extern crate test;
    use super::*;
    use crate::test_utils::*;

    #[bench]
    fn add_chunk_to_maybe_empty_with_empty(b: &mut test::Bencher) {
        run_add_chunk_maybe_empty_bench(b, random_f32_vec(44100), vec![]);
    }

    #[bench]
    fn add_chunk_to_maybe_empty_with_nonempty(b: &mut test::Bencher) {
        run_add_chunk_maybe_empty_bench(b, random_f32_vec(44100), random_f32_vec(44100));
    }

    #[bench]
    fn add_chunk_to_filled(b: &mut test::Bencher) {
        run_add_chunk_bench(b, random_f32_vec(44100), random_f32_vec(44100));
    }

    #[bench]
    fn compress_random_data(b: &mut test::Bencher) {
        run_compress_bench(b, &mut random_f32_vec(44100));
    }

    fn run_add_chunk_bench(b: &mut test::Bencher, src: Vec<f32>, dest: Vec<f32>) {
        let mut black_box_dest = test::black_box(dest);
        b.iter(|| {
            add_chunk_to(&src, &mut black_box_dest);
        });
    }

    fn run_add_chunk_maybe_empty_bench(b: &mut test::Bencher, src: Vec<f32>, dest: Vec<f32>) {
        let mut black_box_dest = test::black_box(dest);
        b.iter(|| {
            add_chunk_to_maybe_empty(&src, &mut black_box_dest).unwrap();
        });
    }

    fn run_compress_bench<T>(b: &mut test::Bencher, samples: &mut Vec<T>)
    where
        T: num::ToPrimitive + num::FromPrimitive + num::Num + Copy,
    {
        b.iter(|| {
            compress::<T>(samples, T::from_f32(100.).unwrap());
        });
    }
}
