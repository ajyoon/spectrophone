use std::error::Error;
use std::sync::{Arc, Mutex};
use std::time::Duration;

use regex::Regex;
use serde_json;
use tiny_http;

use crate::controller::{
    CameraConfig, Controller, ControllerConfig, DeleteLayerRequest, PutLayerRequest,
};
use crate::ids::LayerId;

// TODO refactor request handlers to be crash resilient.
#[allow(dead_code)]
fn crash_resilient_call<F>(func: F)
where
    F: FnOnce() -> Result<(), Box<dyn Error>>,
{
    match func() {
        Ok(()) => {}
        Err(error) => {
            error!("Task failed: {:?}", error);
        }
    }
}

pub fn launch(config: ControllerConfig) {
    let controller = bootstrap_controller(config);

    let server = tiny_http::Server::http("0.0.0.0:8000").unwrap();

    loop {
        controller.lock().unwrap().handle_layer_shutdowns();
        match server.recv_timeout(Duration::from_millis(500)) {
            Ok(maybe_request) => match maybe_request {
                Some(request) => handle_request(request, &controller),
                None => {}
            },
            Err(_) => {
                warn!("Got error when attempting to receive requests");
            }
        }
    }
}

fn handle_request(mut request: tiny_http::Request, controller: &Arc<Mutex<Controller>>) {
    lazy_static! {
        static ref GET_STATUS_RE: Regex = Regex::new(r"^/status$").unwrap();
        static ref POST_SHUTDOWN_RE: Regex = Regex::new(r"^/shutdown$").unwrap();
        static ref PUT_LAYER_RE: Regex = Regex::new(r"^/layer/(?P<id>\d+)$").unwrap();
        static ref DELETE_LAYER_RE: Regex = Regex::new(r"^/layer/(?P<id>\d+)$").unwrap();
        static ref PUT_NUMERIC_CAMERA_CONFIG_RE: Regex =
            Regex::new(r"^/camera/config/(?P<name>\w+)$").unwrap();
    }
    if request.method() == &tiny_http::Method::Get && GET_STATUS_RE.is_match(request.url()) {
        let response_data =
            serde_json::to_string(&controller.lock().unwrap().get_status()).unwrap();
        request
            .respond(tiny_http::Response::from_string(response_data))
            .unwrap();
    } else if request.method() == &tiny_http::Method::Post
        && POST_SHUTDOWN_RE.is_match(request.url())
    {
        request.respond(tiny_http::Response::empty(200)).unwrap();
        controller.lock().unwrap().shutdown();
    } else if request.method() == &tiny_http::Method::Put && PUT_LAYER_RE.is_match(request.url()) {
        let id: LayerId = PUT_LAYER_RE
            .captures(request.url())
            .unwrap()
            .name("id")
            .unwrap()
            .as_str()
            .parse()
            .unwrap();
        let mut content = String::new();
        request.as_reader().read_to_string(&mut content).unwrap();

        let PutLayerRequest { layer, transition } = serde_json::from_str(&content).unwrap();
        if layer.voices.is_empty() {
            request.respond(tiny_http::Response::empty(400)).unwrap();
        } else {
            // Respond before execution to make client more responsive, maybe a bad idea..
            request.respond(tiny_http::Response::empty(200)).unwrap();
            controller
                .lock()
                .unwrap()
                .put_layer(id, layer, transition)
                .unwrap();
        }
    } else if request.method() == &tiny_http::Method::Delete
        && DELETE_LAYER_RE.is_match(request.url())
    {
        let id: LayerId = DELETE_LAYER_RE
            .captures(request.url())
            .unwrap()
            .name("id")
            .unwrap()
            .as_str()
            .parse()
            .unwrap();
        let mut content = String::new();
        request.as_reader().read_to_string(&mut content).unwrap();

        // Respond before execution to make client more responsive, maybe a bad idea..
        request.respond(tiny_http::Response::empty(200)).unwrap();

        let DeleteLayerRequest { transition } = serde_json::from_str(&content).unwrap();
        controller.lock().unwrap().delete_layer(id, transition);
    } else if request.method() == &tiny_http::Method::Put
        && PUT_NUMERIC_CAMERA_CONFIG_RE.is_match(request.url())
    {
        let name: String = PUT_NUMERIC_CAMERA_CONFIG_RE
            .captures(request.url())
            .unwrap()
            .name("name")
            .unwrap()
            .as_str()
            .to_string();
        let mut content = String::new();
        request.as_reader().read_to_string(&mut content).unwrap();
        // Respond before execution to make client more responsive, maybe a bad idea..
        request.respond(tiny_http::Response::empty(200)).unwrap();
        let camera_config: CameraConfig = serde_json::from_str(&content).unwrap();
        controller
            .lock()
            .unwrap()
            .put_camera_config(name, camera_config);
    } else {
        request.respond(tiny_http::Response::empty(404)).unwrap();
    }
}

fn bootstrap_controller(config: ControllerConfig) -> Arc<Mutex<Controller>> {
    Arc::new(Mutex::new(Controller::bootstrap(config)))
}
