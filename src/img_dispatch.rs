use std::fmt;
use std::time::Duration;

use opencv::core::Mat;

use crate::control_messages::ImgDispatcherControlMessage;
use crate::expiring_mpsc::{Receiver, Sender};
use crate::layer_interpreter::LayerInterpreterClosure;
use crate::live_img_preview::ImgPacket;

#[derive(Debug, Clone)]
pub struct LayerPacket {
    pub amplitudes: Vec<f32>,
    pub duration: Duration,
}

pub struct LayerDispatcher {
    pub sender: Sender<LayerPacket>,
    pub layer_interpreter: LayerInterpreterClosure,
}

/// We can't automatically derive Debug for LayerDispatcher because LayerInterpreterClosure
/// is just a type alias to a dynamically dispatched function which cannot be Debug-derived,
/// so we have to manually implement this.
impl fmt::Debug for LayerDispatcher {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "LayerDispatcher {{ sender: {:?}, layer_interpreter: <{:p}> }}",
            self.sender, self.layer_interpreter
        )
    }
}

impl LayerDispatcher {
    pub fn dispatch_layer(
        &self,
        img: &mut Mat,
        duration: Duration,
        time_to_live: Option<Duration>,
    ) {
        let layer_packet = LayerPacket {
            amplitudes: (self.layer_interpreter)(img),
            duration,
        };
        let send_result = match time_to_live {
            Some(ttl) => self.sender.send_with_expiration(layer_packet, ttl),
            None => self.sender.send(layer_packet),
        };

        if let Err(e) = send_result {
            error!("failed to send layer packet");
            trace!("failed error packet: {:?}", e);
        }
    }
}

pub trait ImgDispatcher {
    fn begin_dispatch(
        &mut self,
        control_channel: Receiver<ImgDispatcherControlMessage>,
        preview_sender: Option<Sender<ImgPacket>>,
    );
}
