use crate::color_key::ColorKey;
use crate::expiring_mpsc::Receiver;
use crate::ids::{LayerId, WindowId};
use crate::img_dispatch::LayerDispatcher;
use crate::live_img_preview::ImgPacket;
use crate::transition::Transition;

#[derive(Debug)]
pub enum ImgDispatcherControlMessage {
    Shutdown,
    DisconnectLayer {
        id: LayerId,
    },
    ConnectLayer {
        id: LayerId,
        layer_dispatcher: LayerDispatcher,
    },
    SetNumericCameraConfig {
        name: String,
        value: i32,
    },
}

#[derive(Debug)]
pub enum LayerSynthControlMessage {
    Shutdown,
    AdjustLayerAmp { transition: Transition },
}

#[derive(Debug)]
pub enum LiveImgPreviewControlMessage {
    Shutdown,
    DisconnectLayer {
        id: LayerId,
    },
    ConnectLayer {
        id: LayerId,
        layer: (Box<dyn ColorKey>, usize, usize, String),
    },
}

#[derive(Debug)]
pub enum AudioStreamerControlMessage<T> {
    Shutdown,
    AdjustExpectedMaxAmp {
        delta_transition: Transition,
    },
    DisconnectLayer {
        id: LayerId,
    },
    ConnectLayer {
        id: LayerId,
        layer_samples_receiver: Receiver<Vec<T>>,
    },
}

#[derive(Debug)]
pub enum SpectrogramPreviewControlMessage {
    Shutdown,
    DisconnectLayer {
        id: LayerId,
    },
    ConnectLayer {
        id: LayerId,
        key: Box<dyn ColorKey>,
        sample_receiver: Receiver<Vec<f32>>,
    },
}

#[derive(Debug)]
pub enum WindowControlMessage {
    Shutdown,
    DisconnectWindow {
        id: WindowId,
    },
    ConnectWindow {
        id: WindowId,
        name: String,
        packet_receiver: Receiver<ImgPacket>,
    },
}
