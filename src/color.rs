use crate::math::partial_min;
use crate::opencv_helpers;
use regex::Regex;
use std::cmp::Ordering::Greater;
use std::f32;
use std::u8;

pub type RgbF32 = (f32, f32, f32);

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Rgb(pub u8, pub u8, pub u8);

pub trait Color: From<Rgb> + Into<Rgb> {}

impl Color for Rgb {}

impl Rgb {
    /// Convert a hex RGB string (optionally prefixed with a `#`) into an Rgb tuple
    ///
    /// Returns None if the given string is invalid.
    pub fn from_hex(hex: &str) -> Option<Self> {
        lazy_static! {
            static ref RE: Regex = Regex::new(
                r"^(#?)(?P<r>[a-fA-F0-9]{2})(?P<g>[a-fA-F0-9]{2})(?P<b>[a-fA-F0-9]{2})$"
            )
            .unwrap();
        }
        let captures = RE.captures(hex)?;

        Some(Rgb(
            u8::from_str_radix(captures.name("r")?.as_str(), 16).ok()?,
            u8::from_str_radix(captures.name("g")?.as_str(), 16).ok()?,
            u8::from_str_radix(captures.name("b")?.as_str(), 16).ok()?,
        ))
    }

    pub fn to_hex(&self) -> String {
        format!("#{:0>2x}{:0>2x}{:0>2x}", self.0, self.1, self.2).to_string()
    }

    pub fn at_intensity(&self, intensity: f32) -> Self {
        Self(
            (self.0 as f32 * intensity) as u8,
            (self.1 as f32 * intensity) as u8,
            (self.2 as f32 * intensity) as u8,
        )
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Hsv {
    pub h: f32,
    pub s: f32,
    pub v: f32,
}

impl Hsv {
    #[inline]
    pub fn new(h: f32, s: f32, v: f32) -> Hsv {
        Hsv { h, s, v }
    }
}

impl std::convert::From<Rgb> for Hsv {
    /// adapted from https://stackoverflow.com/a/6930407/5615927
    fn from(obj: Rgb) -> Self {
        let rgb = (
            obj.0 as f32 / 255.0,
            obj.1 as f32 / 255.0,
            obj.2 as f32 / 255.0,
        );
        let mut min = if rgb.0 < rgb.1 { rgb.0 } else { rgb.1 };
        min = if min < rgb.2 { min } else { rgb.2 };

        let mut max = if rgb.0 > rgb.1 { rgb.0 } else { rgb.1 };
        max = if max > rgb.2 { max } else { rgb.2 };

        let out_v = max;
        let delta = max - min;
        if delta < 0.00001 || max == 0.0 {
            return Hsv::new(0.0, 0.0, out_v);
        }
        let out_s = delta / max;
        let mut out_h = if rgb.0 >= max {
            // between yellow & magenta
            (rgb.1 - rgb.2) / delta
        } else if rgb.1 >= max {
            // between cyan & yellow
            2.0 + (rgb.2 - rgb.0) / delta
        } else {
            // between magenta & cyan
            4.0 + (rgb.0 - rgb.1) / delta
        } / 6.0; // Scale H value to 0-1 ratio

        if out_h < 0.0 {
            out_h += 1.0;
        }

        Hsv {
            h: out_h,
            s: out_s,
            v: out_v,
        }
    }
}

/// adapted from https://stackoverflow.com/a/6930407/5615927
impl std::convert::Into<Rgb> for Hsv {
    fn into(self) -> Rgb {
        if self.s <= 0.0 {
            return Rgb(
                (self.v * 255.0) as u8,
                (self.v * 255.0) as u8,
                (self.v * 255.0) as u8,
            );
        }

        let hh = self.h * 6.0;
        let i = hh as i32;
        let ff = hh - i as f32;

        let p = self.v * (1.0 - self.s);
        let q = self.v * (1.0 - (self.s * ff));
        let t = self.v * (1.0 - (self.s * (1.0 - ff)));

        let (r, g, b) = match i {
            0 => (self.v, t, p),
            1 => (q, self.v, p),
            2 => (p, self.v, t),
            3 => (p, q, self.v),
            4 => (t, p, self.v),
            _ => (self.v, p, q),
        };
        Rgb((r * 255.0) as u8, (g * 255.0) as u8, (b * 255.0) as u8)
    }
}

impl Color for Hsv {}

#[derive(PartialEq, Clone, Copy, Debug)]
pub struct Luv(pub f32, pub f32, pub f32);

impl std::convert::From<Rgb> for Luv {
    /// Convert a 24-bit RGB color to a 96-bit CIE L*u*v* color
    ///
    /// Formula and magic constants from
    /// https://docs.opencv.org/3.1.0/de/d25/imgproc_color_conversions.html
    fn from(obj: Rgb) -> Self {
        let rgb = (
            obj.0 as f32 / 255.0,
            obj.1 as f32 / 255.0,
            obj.2 as f32 / 255.0,
        );
        let (x, y, z) = (
            (rgb.0 * 0.412453) + (rgb.1 * 0.357580) + (rgb.2 * 0.180423),
            (rgb.0 * 0.212671) + (rgb.1 * 0.715160) + (rgb.2 * 0.072169),
            (rgb.0 * 0.019334) + (rgb.1 * 0.119193) + (rgb.2 * 0.950227),
        );
        let l = match y.partial_cmp(&0.008856) {
            Some(Greater) => (116.0 * y.cbrt()) - 16.0,
            _ => 903.3 * y,
        };
        let u_prime = (4.0 * x) / (x + (15.0 * y) + (3.0 * z));
        let v_prime = (9.0 * y) / (x + (15.0 * y) + (3.0 * z));
        let u = match u_prime.is_nan() {
            true => 0.0,
            false => 13.0 * l * (u_prime - 0.19793943),
        };
        let v = match v_prime.is_nan() {
            true => 0.0,
            false => 13.0 * l * (v_prime - 0.46831096),
        };
        Self(l, u, v)
    }
}

impl std::convert::Into<Rgb> for Luv {
    fn into(self) -> Rgb {
        opencv_helpers::convert_luv_to_rgb(&self)
    }
}

impl Color for Luv {}

impl Luv {
    #[inline]
    pub fn as_tuple(&self) -> (f32, f32, f32) {
        (self.0, self.1, self.2)
    }
}

/// Calculate the average value of an RGB color, scaled between 0 and 1.
pub fn luminosity(color: Rgb) -> f32 {
    (color.0 as f32 + color.1 as f32 + color.2 as f32) / 765.0
}

pub fn euclidean_distance(left: (f32, f32, f32), right: (f32, f32, f32)) -> f32 {
    ((left.0 - right.0).powi(2) + (left.1 - right.1).powi(2) + (left.2 - right.2).powi(2)).sqrt()
}

/// Linearly scale a color to a given intensity.
///
/// This is kind of a dumb operation, it's equivalent to
/// `(color.0 * intensity, color.1 * intensity, color.2 * intensity)`
pub fn color_at_intensity(color: Rgb, intensity: f32) -> RgbF32 {
    (
        (color.0 as f32 * intensity) / 255.0,
        (color.1 as f32 * intensity) / 255.0,
        (color.2 as f32 * intensity) / 255.0,
    )
}

#[cfg(test)]
mod tests {
    extern crate test;
    #[allow(unused_imports)]
    use super::*;
    use crate::test_utils::*;
    use rand::Rng;

    #[test]
    fn test_rgb_from_hex_with_leading_hash() {
        assert_eq!(Rgb::from_hex("#ffffff"), Some(Rgb(255, 255, 255)));
    }

    #[test]
    fn test_rgb_from_hex_without_leading_hash() {
        assert_eq!(Rgb::from_hex("ffffff"), Some(Rgb(255, 255, 255)));
    }

    #[test]
    fn test_rgb_from_hex_invalid_strings() {
        // Too short
        assert_eq!(Rgb::from_hex("fffff"), None);
        // Too long
        assert_eq!(Rgb::from_hex("fffffff"), None);
        // Non-alphanumeric characters
        assert_eq!(Rgb::from_hex("$%^&^*"), None);
        // Out-of-range alphabetic characters
        assert_eq!(Rgb::from_hex("fffffg"), None);
    }

    #[test]
    fn test_rgb_to_hex_single_digit() {
        assert_eq!(Rgb(111, 1, 2).to_hex(), "#6f0102");
    }

    #[test]
    fn test_rgb_at_intensity() {
        let actual = Rgb(255, 255, 255).at_intensity(0.5);
        assert_eq!(actual.0, 127);
        assert_eq!(actual.1, 127);
        assert_eq!(actual.2, 127);
    }

    #[test]
    fn test_rgb_to_luv_black() {
        assert_almost_eq_3_tuple(Luv::from(Rgb(0, 0, 0)).as_tuple(), (0.0, 0.0, 0.0));
    }

    #[test]
    fn test_hsv_from_rgb() {
        // A few arbitrarily chosen conversions checked against an online converter
        assert_eq_hsv(
            Hsv::from(Rgb(34, 85, 89)),
            Hsv::new(0.512121194444, 0.6179775, 0.34901962),
        );
        assert_eq_hsv(
            Hsv::from(Rgb(255, 151, 52)),
            Hsv::new(0.0812807944444, 0.79607844, 1.0),
        );
        assert_eq_hsv(
            Hsv::from(Rgb(153, 134, 40)),
            Hsv::new(0.138643075, 0.7385621, 0.6),
        );
    }

    #[test]
    fn test_hsv_from_rgb_edge_cases() {
        assert_eq_hsv(Hsv::from(Rgb(0, 0, 0)), Hsv::new(0.0, 0.0, 0.0));
        assert_eq_hsv(Hsv::from(Rgb(255, 255, 255)), Hsv::new(0.0, 0.0, 1.0));
    }

    #[test]
    fn test_hsv_from_rgb_to_rgb() {
        let mut rng = rand::thread_rng();
        for _ in 0..1000 {
            let original_rgb = Rgb(
                rng.gen_range(0, 255),
                rng.gen_range(0, 255),
                rng.gen_range(0, 255),
            );
            let hsv = Hsv::from(original_rgb);
            let new_rgb: Rgb = hsv.into();

            let tolerance: i16 = 1;
            if (original_rgb.0 as i16 - new_rgb.0 as i16).abs() > tolerance
                || (original_rgb.1 as i16 - new_rgb.1 as i16).abs() > tolerance
                || (original_rgb.2 as i16 - new_rgb.2 as i16).abs() > tolerance
            {
                panic!("identity RGB transformation did not meet tolernace {}. before: {:?}, after: {:?}",
                       tolerance, original_rgb, new_rgb);
            }
        }
    }

    #[test]
    fn test_luminosity_all_zeros() {
        assert_almost_eq(luminosity(Rgb(0, 0, 0)), 0.);
    }

    #[test]
    fn test_luminosity_all_255() {
        assert_almost_eq(luminosity(Rgb(255, 255, 255)), 1.);
    }

    #[test]
    fn test_euclidian_distance_same_point() {
        let left = (1.23, 1.23, 1.23);
        let right = left.clone();
        assert_almost_eq(euclidean_distance(left, right), 0.0);
    }

    #[test]
    fn test_euclidian_distance_different_points() {
        let left = (1.23, 1.23, 1.23);
        let right = (4.56, 4.56, 4.56);
        assert_almost_eq(euclidean_distance(left, right), 5.7677293);
    }

    #[test]
    fn test_luv() {
        let luv = Luv::from(Rgb(25, 50, 100));
        assert_almost_eq_3_tuple(luv.as_tuple(), (50.614487, -17.049667, -42.118065))
    }

    #[test]
    #[ignore]
    fn fuzz_luv_range() {
        let mut min_l = f32::INFINITY;
        let mut max_l = f32::NEG_INFINITY;
        let mut min_u = f32::INFINITY;
        let mut max_u = f32::NEG_INFINITY;
        let mut min_v = f32::INFINITY;
        let mut max_v = f32::NEG_INFINITY;
        let mut rng = rand::thread_rng();
        for _ in 0..100_000 {
            let Luv(l, u, v) = Luv::from(Rgb(
                rng.gen_range(0, 255),
                rng.gen_range(0, 255),
                rng.gen_range(0, 255),
            ));
            if l < min_l {
                min_l = l;
            }
            if l > max_l {
                max_l = l;
            }
            if u < min_u {
                min_u = u;
            }
            if u > max_u {
                max_u = u;
            }
            if v < min_v {
                min_v = v;
            }
            if v > max_v {
                max_v = v;
            }
        }
        info!(
            "min_l: {}, max_l: {}
                  min_u: {}, max_u: {}
                  min_v: {}, max_v: {}",
            min_l, max_l, min_u, max_u, min_v, max_v
        );
        assert!(false);
    }

    #[test]
    fn test_color_at_intensity() {
        let actual = color_at_intensity(Rgb(255, 255, 255), 0.5);
        assert_almost_eq(actual.0, 0.5);
        assert_almost_eq(actual.1, 0.5);
        assert_almost_eq(actual.2, 0.5);
    }

    #[bench]
    fn bench_rgb_to_luv(b: &mut test::Bencher) {
        b.iter(move || {
            for _ in 0..1_000 {
                test::black_box(Luv::from(Rgb(10, 20, 30)));
            }
        });
    }

    #[bench]
    fn bench_hsv_from_rgb(b: &mut test::Bencher) {
        b.iter(move || {
            for _ in 0..1_000 {
                test::black_box(Hsv::from(Rgb(10, 20, 30)));
            }
        });
    }
}
