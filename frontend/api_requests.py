import jsons


class PutLayerRequest:

    def __init__(self, layer, transition):
        self.layer = layer
        self.transition = transition

    def as_json(self):
        return jsons.dump(self)


class DeleteLayerRequest:

    def __init__(self, transition):
        self.transition = transition

    def as_json(self):
        return jsons.dump(self)
