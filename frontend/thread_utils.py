import threading

def run_in_thread(fn):
    def thread_fn(stop_event):
            fn(stop_event)
    stop_event = threading.Event()
    thread = threading.Thread(target=thread_fn, args=(stop_event,))
    thread.start()
    return stop_event
