use std::cmp;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::thread;
use std::time::Duration;

use opencv::core;
use opencv::core::Mat;
use opencv::imgcodecs;
use opencv::prelude::*;
use opencv::types;

use crate::color::{color_at_intensity, Rgb};
use crate::color_key::ColorKey;
use crate::control_messages::{SpectrogramPreviewControlMessage, WindowControlMessage};
use crate::expiring_mpsc::{Receiver, Sender, TryRecvError};
use crate::fft::{compute_fft, FFTBin};
use crate::gui::Window;
use crate::heartbeat::Heartbeat;
use crate::ids::LayerId;
use crate::opencv_helpers;

const LOG_SCALE_BASE: f32 = 50f32;
const SLEEP_DUR: Duration = Duration::from_millis(1000);
const DISPLAY_POWER_MULTIPLIER: f32 = 0.5;

/// Derive a single-column spectrogram image from an audio sample
///
/// This uses an FFT based on a naive rectangular window function,
/// so its results may be noisy, especially with the relatively
/// small sample rates used in practice here.
///
/// It may be worth trying to use a Hanning function or sim instead
/// See https://en.wikipedia.org/wiki/Window_function
fn create_spectrogram_column(
    samples: Vec<f32>,
    height: usize,
    sample_rate: u32,
    key: &Box<dyn ColorKey>,
) -> Mat {
    let fft_bins: Vec<FFTBin> = compute_fft(samples, sample_rate);
    let mut spectrogram_column =
        unsafe { Mat::new_rows_cols(height as i32, 1, core::CV_32FC3).unwrap() };
    let max_exponent = (fft_bins.len() as f32).log(LOG_SCALE_BASE);

    for y in 0..height {
        let bin_position = (height - y) as f32;
        let next_bin_position = ((height + 1) - y) as f32;
        let pixel_start_bin =
            (LOG_SCALE_BASE.powf((bin_position / height as f32) * max_exponent)) as usize;
        let pixel_end_bin = cmp::min(
            cmp::max(
                (LOG_SCALE_BASE.powf((next_bin_position / height as f32) * max_exponent)) as usize,
                pixel_start_bin + 1,
            ),
            fft_bins.len(),
        );
        let sum_power: f32 = fft_bins[pixel_start_bin..pixel_end_bin]
            .iter()
            .map(|bin| bin.power)
            .sum();
        let avg_power = sum_power / cmp::max(pixel_end_bin - pixel_start_bin, 1) as f32;
        let adjusted_power = avg_power * DISPLAY_POWER_MULTIPLIER;
        let rgb_pixel_color = key.as_rgb().at_intensity(adjusted_power);
        opencv_helpers::write_rgbu8_pixel_to_bgrf32_mat(
            &mut spectrogram_column,
            0,
            y,
            rgb_pixel_color,
        );
    }
    spectrogram_column
}

fn mix_spectrogram_layers(layers: Vec<Mat>) -> Mat {
    opencv_helpers::sum_mats(layers)
}

pub struct AudioLayer {
    pub sample_receiver: Receiver<Vec<f32>>,
    pub key: Box<dyn ColorKey>,
}

pub struct LiveSpectrogramDisplay {
    window: Window,
    #[allow(dead_code)]
    window_controller: Sender<WindowControlMessage>,
}

impl LiveSpectrogramDisplay {
    pub fn new(window_controller: Sender<WindowControlMessage>) -> LiveSpectrogramDisplay {
        let window =
            Window::new("Live spectrogram display".to_string(), &window_controller).unwrap();
        LiveSpectrogramDisplay {
            window,
            window_controller,
        }
    }

    pub fn display_spectrogram_column(&self, spectrogram_column: Mat) {
        self.window.show_frame(spectrogram_column);
    }
}

pub struct LiveSpectrogramPreview {
    layers: HashMap<LayerId, AudioLayer>,
    sample_rate: u32,
    height: usize,
    display: Option<LiveSpectrogramDisplay>,
    out_path: Option<PathBuf>,
    control_receiver: Receiver<SpectrogramPreviewControlMessage>,
}

impl LiveSpectrogramPreview {
    pub fn new(
        control_receiver: Receiver<SpectrogramPreviewControlMessage>,
        sample_rate: u32,
        height: usize,
        display: Option<LiveSpectrogramDisplay>,
        out_path: Option<PathBuf>,
    ) -> LiveSpectrogramPreview {
        let layers = HashMap::new();
        LiveSpectrogramPreview {
            layers,
            sample_rate,
            height,
            display,
            control_receiver,
            out_path,
        }
    }

    fn disconnect_layer(&mut self, id: LayerId) {
        self.layers.remove(&id);
    }

    fn connect_layer(
        &mut self,
        id: LayerId,
        key: Box<dyn ColorKey>,
        sample_receiver: Receiver<Vec<f32>>,
    ) {
        let audio_layer = AudioLayer {
            key,
            sample_receiver,
        };
        self.layers.insert(id, audio_layer);
    }

    pub fn live_preview(&mut self) {
        let mut columns: Vec<Mat> = vec![];
        let mut heartbeat = Heartbeat::new(
            Duration::from_secs(5),
            "live spectrogram preview".to_string(),
        );
        loop {
            heartbeat.mark();

            match self.control_receiver.try_recv() {
                Ok(message) => match message {
                    SpectrogramPreviewControlMessage::Shutdown => {
                        info!("Received shutdown signal. Shutting down.");
                        break;
                    }
                    SpectrogramPreviewControlMessage::DisconnectLayer { id } => {
                        self.disconnect_layer(id);
                    }
                    SpectrogramPreviewControlMessage::ConnectLayer {
                        id,
                        key,
                        sample_receiver,
                    } => {
                        self.connect_layer(id, key, sample_receiver);
                    }
                },
                Err(e) => match e {
                    TryRecvError::Empty => (),
                    TryRecvError::Disconnected => {
                        break;
                    }
                },
            }

            let mut spectrogram_layers: Vec<Mat> = vec![];
            for layer in self.layers.values() {
                match layer.sample_receiver.recv() {
                    Ok(samples) => {
                        let column = create_spectrogram_column(
                            samples,
                            self.height,
                            self.sample_rate,
                            &layer.key,
                        );
                        spectrogram_layers.push(column);
                    }
                    Err(e) => {
                        error!("failed to receive samples: {:?}", e);
                    }
                }
            }
            if !spectrogram_layers.is_empty() {
                let spectrogram_column = mix_spectrogram_layers(spectrogram_layers);
                if self.out_path.is_some() {
                    columns.push(spectrogram_column.clone().unwrap());
                }
                if let Some(display) = &self.display {
                    display.display_spectrogram_column(spectrogram_column);
                }
            } else {
                info!("No layer data received. Sleeping {:?}", SLEEP_DUR);
                thread::sleep(SLEEP_DUR);
            }
        }
        if self.out_path.is_some() {
            Self::save_spectrogram(self.out_path.as_ref().unwrap().as_path(), columns);
        }
    }

    fn save_spectrogram(path: &Path, columns: Vec<Mat>) {
        info!("Saving spectrogram results");
        let mut f32_image = opencv_helpers::combine_columns(columns);
        let u8_image = opencv_helpers::convert_f32_img_to_u8(&mut f32_image);

        imgcodecs::imwrite(
            path.to_str().unwrap(),
            &u8_image,
            &types::VectorOfint::new(),
        )
        .unwrap();
        info!("Finished saving spectrogram results");
    }
}
