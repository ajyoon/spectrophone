use opencv::core;
use opencv::core::Mat;
use opencv::imgproc;

use crate::color::Rgb;
use crate::color_key::{ColorKey, HsvKey};
use crate::opencv_helpers;

/// A boxed function which takes an opencv matrix, does some transformation on
/// it, and outputs an amplitude for some number of segments defined within
/// the closure.
pub type LayerInterpreterClosure = Box<dyn Fn(&mut Mat) -> Vec<f32> + Send>;

pub fn color_similarity_interpreter(
    image: &mut Mat,
    horizontal_regions: usize,
    num_segments: usize,
    key: &Box<dyn ColorKey>,
    highest_similarity_region_weight: f32,
) -> Vec<f32> {
    debug_assert_eq!(image.typ().unwrap(), core::CV_32FC3);
    let bgr_regions = downsample_to_regions(image, horizontal_regions, num_segments);
    let bgr_vec = opencv_helpers::mat_32fc3_to_vec_u8(bgr_regions);
    let result: Vec<f32> = bgr_vec
        .iter()
        .map(|row| {
            let row_len = row.len() as f32;
            let (mean, max) = row.iter().fold((0f32, 0f32), |acc, pixel| {
                let similarity = key.similarity(Rgb(pixel.2, pixel.1, pixel.0));
                (
                    acc.0 + (similarity / row_len),
                    if similarity > acc.1 {
                        similarity
                    } else {
                        acc.1
                    },
                )
            });
            (highest_similarity_region_weight * max)
                + ((1.0 - highest_similarity_region_weight) * mean)
        })
        .collect();
    result
}

pub fn color_similarity_preview(
    src: &mut Mat,
    horizontal_regions: usize,
    num_segments: usize,
    color_key: &Box<dyn ColorKey>,
) -> Mat {
    debug_assert_eq!(src.typ().unwrap(), core::CV_32FC3);
    let reference_rgb: Rgb = color_key.as_rgb();
    let r_float_factor = (reference_rgb.0 as f32) / 255.0;
    let g_float_factor = (reference_rgb.1 as f32) / 255.0;
    let b_float_factor = (reference_rgb.2 as f32) / 255.0;
    let _output_size = src.size().unwrap();
    let bgr_regions = downsample_to_regions(src, horizontal_regions, num_segments);
    let rows = bgr_regions.size().unwrap().height;
    let cols = bgr_regions.size().unwrap().width;
    let mut similarity_mat = unsafe { Mat::new_rows_cols(rows, cols, core::CV_32FC3).unwrap() };
    for y in 0..rows {
        for x in 0..cols {
            unsafe {
                let bgr_pixel = *bgr_regions.at_2d_unchecked::<core::Vec3f>(y, x).unwrap();
                let rgb_pixel = Rgb(
                    (bgr_pixel[2] * 255.0) as u8,
                    (bgr_pixel[1] * 255.0) as u8,
                    (bgr_pixel[0] * 255.0) as u8,
                );
                let similarity = color_key.similarity(rgb_pixel);
                *similarity_mat
                    .at_2d_mut_unchecked::<core::Vec3f>(y, x)
                    .unwrap() = core::Vec3f::from([
                    similarity * b_float_factor,
                    similarity * g_float_factor,
                    similarity * r_float_factor,
                ]);
            }
        }
    }
    similarity_mat
}

const DERIVE_FACTOR_FROM_DST_IMAGE: f64 = 0.0;

/// Scale an image down to an image with width `horizontal_regions` and height `num_segments`
pub fn downsample_to_regions(
    image: &mut Mat,
    horizontal_regions: usize,
    num_segments: usize,
) -> Mat {
    let mut scaled_img = unsafe {
        Mat::new_rows_cols(
            num_segments as i32,
            horizontal_regions as i32,
            image.typ().unwrap(),
        )
        .unwrap()
    };
    let size = scaled_img.size().unwrap();
    imgproc::resize(
        image,
        &mut scaled_img,
        size,
        DERIVE_FACTOR_FROM_DST_IMAGE,
        DERIVE_FACTOR_FROM_DST_IMAGE,
        imgproc::INTER_LINEAR,
    )
    .unwrap();
    scaled_img
}

pub fn generate_color_similarity_interpreter(
    key: Box<dyn ColorKey>,
    horizontal_regions: usize,
    num_segments: usize,
    highest_similarity_region_weight: f32,
) -> LayerInterpreterClosure {
    Box::new(move |image: &mut Mat| {
        color_similarity_interpreter(
            image,
            horizontal_regions,
            num_segments,
            &key,
            highest_similarity_region_weight,
        )
    })
}
