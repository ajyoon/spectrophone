extern crate clap;
extern crate num;
extern crate spectrophone;

use std::path::{Path, PathBuf};

use clap::{App, Arg, ArgMatches, SubCommand};

use spectrophone::camera::{CameraParams, OpencvVideoSource};
use spectrophone::color_key::HsvKey;
use spectrophone::control_server;
use spectrophone::controller::ControllerConfig;
use spectrophone::preview;

const PREVIEW_SUBCOMMAND: &str = "preview";
const SRC_IMAGE: &str = "SRC_IMAGE";

const HUE: &str = "HUE";
const SATURATION: &str = "SATURATION";
const VALUE: &str = "VALUE";
const HUE_TOLERANCE: &str = "HUE_TOLERANCE";
const SATURATION_TOLERANCE: &str = "SATURATION_TOLERANCE";
const VALUE_TOLERANCE: &str = "VALUE_TOLERANCE";

const OUT_PATH_ARG: &str = "OUT_PATH";
const SPECTROGRAM_OUT_PATH_ARG: &str = "SPECTROGRAM_OUT_PATH";
const USE_WEBCAM: &str = "USE_WEBCAM";
const USE_VIDEO_FILE: &str = "USE_VIDEO_FILE";
const USE_GPHOTO_CAMERA: &str = "USE_GPHOTO";
const USE_SCREEN_CAMERA: &str = "USE_SCREEN_CAMERA";
const RESIZE_INPUT: &str = "RESIZE_INPUT";

pub fn main() {
    spectrophone::runtime_setup::setup();
    let matches = App::new("spectrophone")
        .subcommand(
            SubCommand::with_name(PREVIEW_SUBCOMMAND)
                .about("Preview an image color mapping")
                .arg(Arg::with_name(SRC_IMAGE).index(1).required(true))
                .arg(Arg::with_name(HUE).index(2).help("degrees").required(true))
                .arg(
                    Arg::with_name(SATURATION)
                        .index(3)
                        .help("percent")
                        .required(true),
                )
                .arg(
                    Arg::with_name(VALUE)
                        .index(4)
                        .help("percent")
                        .required(true),
                )
                .arg(
                    Arg::with_name(HUE_TOLERANCE)
                        .index(5)
                        .help("degrees")
                        .required(true),
                )
                .arg(
                    Arg::with_name(SATURATION_TOLERANCE)
                        .help("percent")
                        .index(6)
                        .required(true),
                )
                .arg(
                    Arg::with_name(VALUE_TOLERANCE)
                        .help("percent")
                        .index(7)
                        .required(true),
                ),
        )
        .arg(
            Arg::with_name(OUT_PATH_ARG)
                .short("o")
                .long("output")
                .help("Target path to write audio, must be *.wav")
                .required(false)
                .takes_value(true),
        )
        .arg(
            Arg::with_name(SPECTROGRAM_OUT_PATH_ARG)
                .short("s")
                .long("spectrogram-output")
                .help(
                    "Target image path to write a spectrogram rendering from the generated audio.",
                )
                .required(false)
                .takes_value(true),
        )
        .arg(
            Arg::with_name(USE_WEBCAM)
                .long("webcam")
                .help("Use a webcam for video input, with an optional numeric device id")
                .required(false)
                .takes_value(true)
                .min_values(0)
                .max_values(1)
                .conflicts_with_all(&[USE_VIDEO_FILE, USE_GPHOTO_CAMERA, USE_SCREEN_CAMERA]),
        )
        .arg(
            Arg::with_name(USE_VIDEO_FILE)
                .long("video-file")
                .help("Use a file for video input, which may be a stream URI")
                .required(false)
                .takes_value(true)
                .min_values(1)
                .max_values(1)
                .conflicts_with_all(&[USE_WEBCAM, USE_GPHOTO_CAMERA, USE_SCREEN_CAMERA]),
        )
        .arg(
            Arg::with_name(USE_GPHOTO_CAMERA)
                .long("gphoto")
                .help("Use a gphoto-bound camera for video input. Only tested on a Nikon D5000")
                .required(false)
                .conflicts_with_all(&[USE_VIDEO_FILE, USE_WEBCAM, USE_SCREEN_CAMERA]),
        )
        .arg(
            Arg::with_name(USE_SCREEN_CAMERA)
                .long("screen")
                .help("Use a screen capture for video input.")
                .required(false)
                .takes_value(true)
                .min_values(0)
                .max_values(1)
                .conflicts_with_all(&[USE_VIDEO_FILE, USE_WEBCAM, USE_GPHOTO_CAMERA]),
        )
        .arg(
            Arg::with_name(RESIZE_INPUT)
                .long("resize")
                .help(
                    "width and height to resize input frames. currently only usable with --screen",
                )
                .required(false)
                .takes_value(true)
                .number_of_values(2)
                .requires(USE_SCREEN_CAMERA),
        )
        .get_matches();

    if let Some(matches) = matches.subcommand_matches(PREVIEW_SUBCOMMAND) {
        let img_path = matches.value_of(SRC_IMAGE).unwrap();
        if !Path::new(img_path).exists() {
            panic!("{} is not a valid path", img_path);
        }
        let key = HsvKey::new(
            matches.value_of(HUE).unwrap().parse::<f32>().unwrap() / 360.0,
            matches
                .value_of(SATURATION)
                .unwrap()
                .parse::<f32>()
                .unwrap()
                / 100.0,
            matches.value_of(VALUE).unwrap().parse::<f32>().unwrap() / 100.0,
            matches
                .value_of(HUE_TOLERANCE)
                .unwrap()
                .parse::<f32>()
                .unwrap()
                / 360.0,
            matches
                .value_of(SATURATION_TOLERANCE)
                .unwrap()
                .parse::<f32>()
                .unwrap()
                / 100.0,
            matches
                .value_of(VALUE_TOLERANCE)
                .unwrap()
                .parse::<f32>()
                .unwrap()
                / 100.0,
        );
        preview::preview_image(img_path, key);
    } else {
        let spectrogram_out_path =
            unpack_option_path_string(matches.value_of(SPECTROGRAM_OUT_PATH_ARG));
        let audio_out_path = matches.value_of(OUT_PATH_ARG).map(|p| PathBuf::from(p));
        let camera_params = resolve_camera_params(&matches);

        let config = ControllerConfig {
            spectrogram_out_path,
            audio_out_path,
            camera_params,
        };

        control_server::launch(config);
    }
}

fn unpack_option_path_string(maybe_path_string: Option<&str>) -> Option<PathBuf> {
    match maybe_path_string {
        Some(path_string) => Some(Path::new(path_string).to_path_buf()),
        None => None,
    }
}

fn resolve_camera_params(matches: &ArgMatches) -> CameraParams {
    if matches.is_present(USE_WEBCAM) {
        let device_id = matches
            .value_of(USE_WEBCAM)
            .map(|id| id.parse::<i32>().unwrap());
        CameraParams::Opencv {
            video_source: OpencvVideoSource::DeviceId(device_id),
        }
    } else if matches.is_present(USE_VIDEO_FILE) {
        let filename = matches.value_of(USE_VIDEO_FILE).unwrap();
        CameraParams::Opencv {
            video_source: OpencvVideoSource::Filename(filename.to_string()),
        }
    } else if matches.is_present(USE_SCREEN_CAMERA) {
        let capture_src = matches
            .value_of(USE_SCREEN_CAMERA)
            .map(|id| id.parse::<usize>().unwrap());
        let scale_to = matches.values_of(RESIZE_INPUT).map(|vals| {
            let vec_vals: Vec<&str> = vals.collect();
            (
                vec_vals[0].parse::<usize>().unwrap(),
                vec_vals[1].parse::<usize>().unwrap(),
            )
        });
        CameraParams::Screen {
            capture_src,
            scale_to,
        }
    } else {
        CameraParams::Gphoto
    }
}
