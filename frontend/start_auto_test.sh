#!/usr/bin/env bash

while inotifywait -e modify -r *;
do
    nice -n 19 pytest --ignore-glob=workspace_*;
done
