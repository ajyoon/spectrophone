use std::borrow::Cow;
use std::ffi::{CStr, CString};
use std::fs::File;
use std::io::Write;
use std::mem;
use std::ptr;
use std::slice;
use std::thread;
use std::time::Duration;

use libc;
use tempfile::{Builder, NamedTempFile};

use abilities::Abilities;
use context::Context;
use media::{FileMedia, Media};
use port::Port;
use storage::Storage;

use handle::prelude::*;

/// A structure representing a camera connected to the system.
pub struct Camera {
    camera: *mut ::gphoto2::Camera,
}

impl Drop for Camera {
    fn drop(&mut self) {
        let reset_rude_configs_result = self.reset_rude_configs();
        if reset_rude_configs_result.iter().any(|r| r.is_err()) {
            println!(
                "Got error while attempting to reset configs. {:?}",
                reset_rude_configs_result
            );
        }
        unsafe {
            ::gphoto2::gp_camera_unref(self.camera);
        }
    }
}

impl Camera {
    /// Opens the first detected camera.
    pub fn autodetect(context: &mut Context) -> ::Result<Self> {
        let mut ptr = unsafe { mem::uninitialized() };

        try_unsafe!(::gphoto2::gp_camera_new(&mut ptr));

        let camera = Camera { camera: ptr };

        try_unsafe!(::gphoto2::gp_camera_init(
            camera.camera,
            context.as_mut_ptr()
        ));

        Ok(camera)
    }

    /// Captures an image.
    pub fn capture_image(&mut self, context: &mut Context) -> ::Result<CameraFile> {
        let mut file_path = unsafe { mem::uninitialized() };

        try_unsafe! {
            ::gphoto2::gp_camera_capture(self.camera,
                                         ::gphoto2::GP_CAPTURE_IMAGE,
                                         &mut file_path,
                                         context.as_mut_ptr())
        };

        Ok(CameraFile { inner: file_path })
    }

    /// Captures an image preview directly into memory
    pub fn capture_image_preview(&mut self, context: &mut Context) -> ::Result<NamedTempFile> {
        let mut camera_file: *mut ::gphoto2::CameraFile = unsafe { mem::uninitialized() };
        try_unsafe! {
            ::gphoto2::gp_file_new(&mut camera_file)
        };
        try_unsafe! {
            ::gphoto2::gp_camera_capture_preview(self.camera,
                                                 camera_file,
                                                 context.as_mut_ptr())
        };

        let mut bytes_pointer: *const u8 = unsafe { mem::uninitialized() };
        let bytes_pointer_pointer: *mut *const u8 = &mut bytes_pointer;
        let mut length: u64 = 0;

        try_unsafe! {
            ::gphoto2::gp_file_get_data_and_size(camera_file,
                                                 bytes_pointer_pointer as *mut *const i8,
                                                 &mut length)
        };

        let bytes = unsafe { slice::from_raw_parts(bytes_pointer, length as usize) };
        let mut file = Builder::new().suffix(".jpg").tempfile().unwrap();
        file.write(bytes);
        Ok(file)
    }

    /// Downloads a file from the camera.
    pub fn download<T: Media>(
        &mut self,
        context: &mut Context,
        source: &CameraFile,
        destination: &mut T,
    ) -> ::Result<()> {
        try_unsafe! {
            ::gphoto2::gp_camera_file_get(self.camera,
                                          source.inner.folder.as_ptr(),
                                          source.inner.name.as_ptr(),
                                          ::gphoto2::GP_FILE_TYPE_NORMAL,
                                          destination.as_mut_ptr(),
                                          context.as_mut_ptr())
        };

        Ok(())
    }

    /// Returns information about the port the camera is connected to.
    pub fn port<'a>(&'a self) -> Port<'a> {
        let mut ptr = unsafe { mem::uninitialized() };

        unsafe {
            assert_eq!(
                ::gphoto2::GP_OK,
                ::gphoto2::gp_camera_get_port_info(self.camera, &mut ptr)
            );
        }

        ::port::from_libgphoto2(self, ptr)
    }

    /// Retrieves the camera's abilities.
    pub fn abilities(&self) -> Abilities {
        let mut abilities = unsafe { mem::uninitialized() };

        unsafe {
            assert_eq!(
                ::gphoto2::GP_OK,
                ::gphoto2::gp_camera_get_abilities(self.camera, &mut abilities)
            );
        }

        ::abilities::from_libgphoto2(abilities)
    }

    /// Retrieves information about the camera's storage.
    ///
    /// Returns a `Vec` containing one `Storage` for each filesystem on the device.
    pub fn storage(&mut self, context: &mut Context) -> ::Result<Vec<Storage>> {
        let mut ptr = unsafe { mem::uninitialized() };
        let mut len = unsafe { mem::uninitialized() };

        try_unsafe! {
            ::gphoto2::gp_camera_get_storageinfo(self.camera,
                                                 &mut ptr,
                                                 &mut len,
                                                 context.as_mut_ptr())
        };

        let storage = ptr as *mut Storage;
        let length = len as usize;

        Ok(unsafe { Vec::from_raw_parts(storage, length, length) })
    }

    /// Returns the camera's summary.
    ///
    /// The summary typically contains non-configurable information about the camera, such as
    /// manufacturer and number of pictures taken.
    ///
    /// ## Errors
    ///
    /// This function returns an error if the summary could not be retrieved:
    ///
    /// * `NotSupported` if there is no summary available for the camera.
    /// * `CorruptedData` if the summary is invalid UTF-8.
    pub fn summary(&mut self, context: &mut Context) -> ::Result<String> {
        let mut summary = unsafe { mem::uninitialized() };

        try_unsafe!(::gphoto2::gp_camera_get_summary(
            self.camera,
            &mut summary,
            context.as_mut_ptr()
        ));

        util::camera_text_to_string(summary)
    }

    /// Returns the camera's manual.
    ///
    /// The manual contains information about using the camera.
    ///
    /// ## Errors
    ///
    /// This function returns an error if the manual could not be retrieved:
    ///
    /// * `NotSupported` if there is no manual available for the camera.
    /// * `CorruptedData` if the summary is invalid UTF-8.
    pub fn manual(&mut self, context: &mut Context) -> ::Result<String> {
        let mut manual = unsafe { mem::uninitialized() };

        try_unsafe!(::gphoto2::gp_camera_get_manual(
            self.camera,
            &mut manual,
            context.as_mut_ptr()
        ));

        util::camera_text_to_string(manual)
    }

    /// Returns information about the camera driver.
    ///
    /// This text typically contains information about the driver's author, acknowledgements, etc.
    ///
    /// ## Errors
    ///
    /// This function returns an error if the about text could not be retrieved:
    ///
    /// * `NotSupported` if there is no about text available for the camera's driver.
    /// * `CorruptedData` if the summary is invalid UTF-8.
    pub fn about_driver(&mut self, context: &mut Context) -> ::Result<String> {
        let mut about = unsafe { mem::uninitialized() };

        try_unsafe!(::gphoto2::gp_camera_get_about(
            self.camera,
            &mut about,
            context.as_mut_ptr()
        ));

        util::camera_text_to_string(about)
    }

    fn get_root_config(&mut self, context: &mut Context) -> ::Result<CameraWidget> {
        let mut main_widget = CameraWidget::initialize_struct();
        try_unsafe!(::gphoto2::gp_camera_get_config(
            self.camera,
            &mut main_widget,
            context.as_mut_ptr()
        ));
        Ok(CameraWidget { inner: main_widget })
    }

    pub fn get_config_string(
        &mut self,
        context: &mut Context,
        config_name: &str,
    ) -> ::Result<String> {
        let mut root_config = self.get_root_config(context)?;
        let mut resolved_config = root_config.find_descendent(context, config_name)?;
        resolved_config.get_string()
    }

    fn commit_config_change(
        &mut self,
        widget: &mut CameraWidget,
        context: &mut Context,
    ) -> ::Result<()> {
        try_unsafe!(::gphoto2::gp_camera_set_config(
            self.camera,
            widget.inner,
            context.as_mut_ptr()
        ));
        Ok(())
    }

    pub fn get_config_toggle(
        &mut self,
        context: &mut Context,
        config_name: &str,
    ) -> ::Result<bool> {
        let mut root_config = self.get_root_config(context)?;
        let mut resolved_config = root_config.find_descendent(context, config_name)?;
        resolved_config.get_toggle()
    }

    pub fn set_config_toggle(
        &mut self,
        context: &mut Context,
        config_name: &str,
        value: bool,
    ) -> ::Result<()> {
        let mut root_config = self.get_root_config(context)?;
        let mut resolved_config = root_config.find_descendent(context, config_name)?;
        resolved_config.set_toggle(value)?;
        self.commit_config_change(&mut root_config, context);
        unsafe {
            ::gphoto2::gp_widget_unref(root_config.inner);
        }
        Ok(())
    }

    pub fn get_config_numeric(&mut self, context: &mut Context, config_name: &str) -> ::Result<i32> {
        let mut root_config = self.get_root_config(context)?;
        let mut resolved_config = root_config.find_descendent(context, config_name)?;
        resolved_config.get_numeric_value()
    }

    pub fn set_config_numeric(&mut self, context: &mut Context, config_name: &str, value: i32) -> ::Result<()> {
        self.disable_viewfinder(context);
        let mut root_config = self.get_root_config(context)?;
        let mut resolved_config = root_config.find_descendent(context, config_name)?;
        resolved_config.set_numeric_value(value)?;
        self.commit_config_change(&mut root_config, context);
        unsafe {
            ::gphoto2::gp_widget_unref(root_config.inner);
        }
        Ok(())
    }

    pub fn get_image_size(&mut self, context: &mut Context) -> ::Result<(usize, usize)> {
        let raw_size = self.get_config_string(context, "/main/imgsettings/imagesize")?;
        let parts: Vec<&str> = raw_size.split('x').collect();
        match parts.len() {
            2 => {
                let width = parts[0].parse::<usize>().unwrap();
                let height = parts[0].parse::<usize>().unwrap();
                Ok((width, height))
            }
            _ => Err(::error::from_libgphoto2(::gphoto2::GP_ERROR_NOT_SUPPORTED)),
        }
    }

    /// Turn off the viewfinder.
    ///
    /// Changing some configs at runtime may require disabling the viewfinder
    /// before changing the config. The viewfinder will be automatically re-enabled
    /// the next time a preview image is captured.
    pub fn disable_viewfinder(&mut self, context: &mut Context) -> ::Result<()> {
        self.set_config_toggle(
            &mut Context::new().unwrap(),
            "/main/actions/viewfinder",
            false,
        )
    }

    /// Reset problematic configs to a good state.
    ///
    /// Some config fields, like `viewfinder` can leave the camera in a bad
    /// state even after the camera is released. On a Nikon D5000, for instance,
    /// capture preview will set `viewfinder=1` and will not reset it when
    /// finished. This leaves a lock on the state of the camera after program
    /// shutdown. This problem is even reproducible with the `gphoto2` CLI:
    ///
    /// ```sh
    /// # first capture succeeds
    /// gphoto2 --capture-preview --force-overwrite
    /// # second capture attempt hangs
    /// gphoto2 --capture-preview --force-overwrite
    /// ```
    ///
    /// This function is a catch-all fixer method for known rude configs.
    fn reset_rude_configs(&mut self) -> Vec<::Result<()>> {
        let mut context = Context::new().unwrap();
        vec![
            self.disable_viewfinder(&mut context),
        ]
    }
}

/// A file stored on a camera's storage.
pub struct CameraFile {
    inner: ::gphoto2::CameraFilePath,
}

impl CameraFile {
    /// Returns the directory that the file is stored in.
    pub fn directory(&self) -> Cow<str> {
        unsafe { String::from_utf8_lossy(CStr::from_ptr(self.inner.folder.as_ptr()).to_bytes()) }
    }

    /// Returns the name of the file without the directory.
    pub fn basename(&self) -> Cow<str> {
        unsafe { String::from_utf8_lossy(CStr::from_ptr(self.inner.name.as_ptr()).to_bytes()) }
    }
}

struct CameraWidget {
    inner: *mut ::gphoto2::CameraWidget,
}

impl CameraWidget {
    pub fn initialize_struct() -> *mut ::gphoto2::CameraWidget {
        unsafe { mem::uninitialized() }
    }

    pub fn find_descendent(
        &mut self,
        context: &mut Context,
        relative_path: &str,
    ) -> ::Result<CameraWidget> {
        let split: Vec<&str> = relative_path.trim_matches('/').splitn(2, "/").collect();
        match split.len() {
            1 => Ok(self.get_child(context, split[0])?),
            2 => {
                let mut child = self.get_child(context, split[0])?;
                Ok(child.find_descendent(context, split[1])?)
            }
            _ => Err(::error::from_libgphoto2(::gphoto2::GP_ERROR_BAD_PARAMETERS)),
        }
    }

    pub fn get_child(&mut self, context: &mut Context, child_name: &str) -> ::Result<CameraWidget> {
        let mut child_widget = Self::initialize_struct();
        let config_c_str = CString::new(child_name).unwrap();
        try_unsafe!(::gphoto2::gp_widget_get_child_by_name(
            self.inner,
            config_c_str.as_ptr(),
            &mut child_widget
        ));
        Ok(CameraWidget {
            inner: unsafe { child_widget },
        })
    }

    pub fn get_string(&mut self) -> ::Result<String> {
        let mut str_pointer: *mut i8 = unsafe { mem::uninitialized() };
        let value: *mut *mut i8 = &mut str_pointer;
        try_unsafe!(::gphoto2::gp_widget_get_value(
            self.inner,
            value as *mut libc::c_void
        ));
        let value_as_str = unsafe {
            String::from_utf8_lossy(CStr::from_ptr(*value as *const i8).to_bytes()).to_string()
        };
        Ok(value_as_str)
    }

    pub fn set_numeric_value(&mut self, value: i32) -> ::Result<()> {
        let int_value: libc::c_int = value;
        try_unsafe!(::gphoto2::gp_widget_set_value(
            self.inner,
            &int_value as *const libc::c_int as *const libc::c_void,
        ));
        Ok(())
    }

    pub fn get_numeric_value(&mut self) -> ::Result<i32> {
        let mut result: libc::c_int = 1337;
        try_unsafe!(::gphoto2::gp_widget_get_value(
            self.inner,
            &mut result as *mut libc::c_int as *mut libc::c_void
        ));
        Ok(result as i32)
    }

    pub fn set_toggle(&mut self, value: bool) -> ::Result<()> {
        self.set_numeric_value(match value { true => 1, false => 0 })?;
        Ok(())
    }

    pub fn get_toggle(&mut self) -> ::Result<bool> {
        let numeric_value = self.get_numeric_value()?;
        Ok(match numeric_value {
            0 => false,
            _ => true,
        })
    }
}

mod util {
    use std::ffi::CStr;

    pub fn camera_text_to_string(mut camera_text: ::gphoto2::CameraText) -> ::Result<String> {
        let length = unsafe { CStr::from_ptr(camera_text.text.as_ptr()).to_bytes().len() };

        let vec = unsafe {
            Vec::<u8>::from_raw_parts(
                camera_text.text.as_mut_ptr() as *mut u8,
                length,
                camera_text.text.len(),
            )
        };

        String::from_utf8(vec)
            .map_err(|_| ::error::from_libgphoto2(::gphoto2::GP_ERROR_CORRUPTED_DATA))
    }
}
