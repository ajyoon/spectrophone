use std::time::{Duration, SystemTime};

use crate::math;

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum Interpolation {
    Linear,
    SquareRoot,
}

#[derive(Debug, Clone)]
pub struct Transition {
    pub start_time: SystemTime,
    pub duration: Duration,
    pub start_val: f32,
    pub end_val: f32,
    pub interpolation: Interpolation,
}

impl Transition {
    pub fn starting_now(
        duration: Duration,
        start_val: f32,
        end_val: f32,
        interpolation: Interpolation,
    ) -> Transition {
        Self::new(
            SystemTime::now(),
            duration,
            start_val,
            end_val,
            interpolation,
        )
    }

    pub fn new(
        start_time: SystemTime,
        duration: Duration,
        start_val: f32,
        end_val: f32,
        interpolation: Interpolation,
    ) -> Transition {
        Transition {
            start_time,
            duration,
            start_val,
            end_val,
            interpolation,
        }
    }

    pub fn current_val(&self) -> f32 {
        self.val_at_time(SystemTime::now())
    }

    #[inline]
    fn val_at_time(&self, time: SystemTime) -> f32 {
        if time < self.start_time {
            return self.start_val;
        }
        let elapsed = time.duration_since(self.start_time).unwrap_or_default();
        if elapsed > self.duration {
            return self.end_val;
        }
        if self.duration.as_millis() == 0 {
            return self.end_val;
        }
        let progress = elapsed.div_duration_f32(self.duration);
        match self.interpolation {
            Interpolation::Linear => math::lerp(self.start_val, self.end_val, progress),
            Interpolation::SquareRoot => math::sqrt_interp(self.start_val, self.end_val, progress),
        }
    }

    pub fn is_complete(&self) -> bool {
        self.is_complete_at_time(SystemTime::now())
    }

    fn is_complete_at_time(&self, time: SystemTime) -> bool {
        time > self.start_time + self.duration
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::test_utils::*;

    #[test]
    fn test_val_before_start_time() {
        let transition = Transition::new(
            SystemTime::now(),
            Duration::from_millis(1),
            1.23,
            4.56,
            Interpolation::Linear,
        );
        assert_almost_eq(
            transition.val_at_time(SystemTime::now() - Duration::from_secs(1000)),
            1.23,
        );
    }

    #[test]
    fn test_val_after_end_of_duration() {
        let transition = Transition::new(
            SystemTime::now(),
            Duration::from_millis(1),
            1.23,
            4.56,
            Interpolation::Linear,
        );
        assert_almost_eq(
            transition.val_at_time(SystemTime::now() + Duration::from_secs(1000)),
            4.56,
        );
    }

    #[test]
    fn test_val_after_start_time_dur_zero() {
        let start_time = SystemTime::now();
        let transition = Transition::new(
            start_time,
            Duration::from_millis(0),
            1.23,
            4.56,
            Interpolation::Linear,
        );
        assert_almost_eq(
            transition.val_at_time(start_time + Duration::from_millis(1)),
            4.56,
        );
    }

    #[test]
    fn test_val_at_middle_of_linear_transition() {
        let start_time = SystemTime::now();
        let transition = Transition::new(
            start_time,
            Duration::from_millis(1000),
            0.0,
            10.0,
            Interpolation::Linear,
        );
        assert_almost_eq(
            transition.val_at_time(start_time + Duration::from_millis(600)),
            6.0,
        );
    }

    #[test]
    fn test_is_complete_when_false() {
        let start_time = SystemTime::now();
        let transition = Transition::new(
            start_time,
            Duration::from_millis(1000),
            0.0,
            10.0,
            Interpolation::Linear,
        );
        assert!(!transition.is_complete_at_time(start_time + Duration::from_millis(999)));
    }

    #[test]
    fn test_is_complete_when_true() {
        let start_time = SystemTime::now();
        let transition = Transition::new(
            start_time,
            Duration::from_millis(1000),
            0.0,
            10.0,
            Interpolation::Linear,
        );
        assert!(transition.is_complete_at_time(start_time + Duration::from_millis(1001)));
    }

    // use std::ops::Add;

    // #[test]
    // fn plot() {
    //     let start_time = SystemTime::now();
    //     let transition = Transition::new(
    //         start_time,
    //         Duration::from_millis(10_000),
    //         10.0,
    //         0.0,
    //         Interpolation::SquareRoot,
    //     );
    //     for i in 0..40 {
    //         println!(
    //             "{}",
    //             transition.val_at_time(start_time.add(Duration::from_millis(i * 250)))
    //         );
    //     }
    //     assert!(false);
    // }
}
