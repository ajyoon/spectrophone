use std::fmt::Debug;

use ndarray::prelude::*;
use opencv::core::Mat;

use crate::color::Hsv;
use crate::color::Rgb;

const F32_EPSILON: f32 = 1.0e-4;

pub fn assert_almost_eq_by_element(left: Vec<f32>, right: Vec<f32>) {
    if left.len() != right.len() {
        panic!(
            "lengths differ: left.len() = {}, right.len() = {}",
            left.len(),
            right.len()
        );
    }
    for (left_val, right_val) in left.iter().zip(right.iter()) {
        assert!(
            f32_almost_eq(*left_val, *right_val),
            "{} is not approximately equal to {}. \
             complete left vec: {:?}. complete right vec: {:?}",
            *left_val,
            *right_val,
            left,
            right
        );
    }
}

#[allow(dead_code)]
pub fn assert_img_data_eq_by_element(left: ArrayView2<f32>, right: ArrayView2<f32>) {
    if left.len() != right.len() {
        panic!(
            "lengths differ: left.len() = {}, right.len() = {}",
            left.len(),
            right.len()
        );
    }
    for (left_val, right_val) in left.iter().zip(right.iter()) {
        assert!(
            left_val == right_val,
            "{} is not equal to {}. \
             complete left array: \n{:?} \n \
             complete right array: \n{:?} \n",
            *left_val,
            *right_val,
            left,
            right
        );
    }
}

#[allow(dead_code)]
pub fn assert_eq_by_element<T>(left: Vec<T>, right: Vec<T>)
where
    T: PartialEq + Debug,
{
    if left.len() != right.len() {
        panic!(
            "lengths differ: left.len() = {:?}, right.len() = {:?}",
            left.len(),
            right.len()
        );
    }
    for (left_val, right_val) in left.iter().zip(right.iter()) {
        assert!(
            left_val == right_val,
            "{:?} is not equal to {:?}. \
             complete left side: \n{:?} \n \
             complete right side: \n{:?} \n",
            *left_val,
            *right_val,
            left,
            right
        );
    }
}

pub fn assert_almost_eq(left: f32, right: f32) {
    assert!(
        f32_almost_eq(left, right),
        "{} is not approximately equal to {}.",
        left,
        right,
    );
}

pub fn assert_almost_eq_3_tuple(left: (f32, f32, f32), right: (f32, f32, f32)) {
    if !f32_almost_eq(left.0, right.0) {
        tuple_assert_eq_fail_at_pos(left.0, right.0, left, right);
    }
    if !f32_almost_eq(left.1, right.1) {
        tuple_assert_eq_fail_at_pos(left.1, right.1, left, right);
    }
    if !f32_almost_eq(left.2, right.2) {
        tuple_assert_eq_fail_at_pos(left.2, right.2, left, right);
    }
}

pub fn assert_eq_hsv(left: Hsv, right: Hsv) {
    if !(f32_almost_eq(left.h, right.h)
        && f32_almost_eq(left.s, right.s)
        && f32_almost_eq(left.v, right.v))
    {
        panic!(
            "HSV values differ. \
             left: {:?} \n\
             right: {:?}",
            left, right
        );
    }
}

pub fn set_mat_pixel(mat: &mut Mat, x: usize, y: usize, color: Rgb) {
    // Handle mapping of RGB -> opencv BGR as well
    unsafe {
        let pixel_base_pointer: *mut u8 = mat.at_2d_mut_unchecked(y as i32, x as i32).unwrap();
        *pixel_base_pointer = color.2;
        *(pixel_base_pointer.offset(1)) = color.1;
        *(pixel_base_pointer.offset(2)) = color.0;
    }
}

pub fn random_f32_vec(len: usize) -> Vec<f32> {
    let mut vec = Vec::<f32>::new();
    for _ in 0..len {
        vec.push(rand::random::<f32>());
    }
    vec
}

fn tuple_assert_eq_fail_at_pos(
    left_failed_val: f32,
    right_failed_val: f32,
    left: (f32, f32, f32),
    right: (f32, f32, f32),
) {
    panic!(
        "{} != {}.\n\
         complete left tuple: {:?}\n\
         complete right tuple: {:?}",
        left_failed_val, right_failed_val, left, right
    );
}

fn f32_almost_eq(left: f32, right: f32) -> bool {
    (left - right).abs() < F32_EPSILON
}
