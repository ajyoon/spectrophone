use std::ops::DerefMut;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

use cpal::{
    self,
    traits::{EventLoopTrait, HostTrait},
    Format, SampleFormat, SampleRate, StreamData, UnknownTypeOutputBuffer,
};
use stopwatch::Stopwatch;

use crate::audio_streamer::AudioStreamer;
use crate::constants;
use crate::control_messages::AudioStreamerControlMessage;
use crate::expiring_mpsc::Receiver;
use crate::max_amp_manager::MaxAmpManager;
use crate::mixer::Mixer;
use crate::sample_buffer::SampleBuffer;

const CHANNELS: u16 = 1;
const SAMPLE_RATE: u32 = 44_100;
const FRAMES_PER_BUFFER: u32 = 2048;
const CALLBACK_DEADLINE: Duration = Duration::from_millis(
    (((FRAMES_PER_BUFFER as f32 * 0.5) / SAMPLE_RATE as f32) * 1000.0) as u64,
);

pub struct CpalAudioStreamer {
    control_receiver: Receiver<AudioStreamerControlMessage<f32>>,
}

impl CpalAudioStreamer {
    pub fn new(control_receiver: Receiver<AudioStreamerControlMessage<f32>>) -> CpalAudioStreamer {
        CpalAudioStreamer { control_receiver }
    }
}

impl AudioStreamer<f32> for CpalAudioStreamer {
    fn stream(&self, mixer: Mixer<f32>) {
        let initial_queue_buffer = SampleBuffer::new(Vec::<f32>::new());
        let queued_received_samples = Arc::new(Mutex::new(initial_queue_buffer));
        let max_amp_manager = Arc::new(Mutex::new(MaxAmpManager::new(0.0)));
        let shared_mixer = Arc::new(Mutex::new(mixer));

        let cloned_queued_received_samples = Arc::clone(&queued_received_samples);
        let cloned_shared_mixer = Arc::clone(&shared_mixer);
        let cloned_max_amp_manager = Arc::clone(&max_amp_manager);

        let host = cpal::default_host();
        let event_loop = host.event_loop();
        let device = host
            .default_output_device()
            .expect("no output device available");
        let format = Format {
            channels: CHANNELS,
            sample_rate: SampleRate(SAMPLE_RATE),
            data_type: SampleFormat::F32,
        };
        let stream_id = event_loop
            .build_output_stream(&device, &format)
            .expect("could not open audio stream");
        event_loop
            .play_stream(stream_id.clone())
            .expect("could not play audio stream");

        let event_loop_arc = Arc::new(event_loop);
        let event_loop_arc_for_run = Arc::clone(&event_loop_arc);

        thread::Builder::new()
            .name("CpalEventLoopThread".to_string())
            .spawn(move || {
                event_loop_arc_for_run.run(move |_stream_id, stream_data| {
                    let mut buffer = match stream_data {
                        Ok(res) => match res {
                            StreamData::Output {
                                buffer: UnknownTypeOutputBuffer::F32(buffer),
                            } => buffer,
                            _ => panic!("unexpected buffer type"),
                        },
                        Err(e) => {
                            panic!("failed to fetch get audio stream: {:?}", e);
                        }
                    };
                    fill_buffer(
                        &cloned_queued_received_samples,
                        &cloned_shared_mixer,
                        &cloned_max_amp_manager,
                        buffer.deref_mut(),
                    );
                });
            })
            .unwrap();

        for control_message in &self.control_receiver {
            match control_message {
                AudioStreamerControlMessage::Shutdown => {
                    info!("got shutdown signal");
                    event_loop_arc.destroy_stream(stream_id);
                    return;
                }
                AudioStreamerControlMessage::AdjustExpectedMaxAmp { delta_transition } => {
                    (*max_amp_manager.lock().unwrap()).add_delta_transition(delta_transition);
                }
                AudioStreamerControlMessage::DisconnectLayer { id } => {
                    shared_mixer
                        .lock()
                        .unwrap()
                        .layer_samples_receivers
                        .remove(&id);
                }
                AudioStreamerControlMessage::ConnectLayer {
                    id,
                    layer_samples_receiver,
                } => {
                    shared_mixer
                        .lock()
                        .unwrap()
                        .layer_samples_receivers
                        .insert(id, layer_samples_receiver);
                }
            }
        }
    }
}

fn fill_buffer(
    queued_received_samples: &Arc<Mutex<SampleBuffer<f32>>>,
    mixer: &Arc<Mutex<Mixer<f32>>>,
    _max_amp_manager: &Arc<Mutex<MaxAmpManager>>,
    out_buffer: &mut [f32],
) {
    let callback_sw = Stopwatch::start_new();
    let queued_samples: &mut SampleBuffer<f32> = &mut queued_received_samples.lock().unwrap();
    let locked_mixer: &mut Mixer<f32> = &mut mixer.lock().unwrap();
    let mut buffer_index = 0;
    let mut times_received_chunks = 0;
    loop {
        let queued_elements_remaining = queued_samples.elements_remaining();
        if queued_elements_remaining >= out_buffer.len() - buffer_index {
            // Enough samples in the queue to fill the buffer completely
            queued_samples.consume_into(&out_buffer[buffer_index..]);
            break;
        }

        // Not enough samples in the queue to fill the buffer, but take what we can
        queued_samples
            .consume_into(&out_buffer[buffer_index..buffer_index + queued_elements_remaining]);

        buffer_index += queued_elements_remaining;
        // let current_max_amp = (*max_amp_manager.lock().unwrap()).current_max_amp();
        let current_max_amp = constants::TEMP_HARD_CODED_MAX_AMP;
        match locked_mixer.mix_chunk(current_max_amp) {
            Ok(samples) => queued_samples.overwrite(samples),
            Err(error) => {
                error!(
                    "Got error when mixing samples for audio streaming: {:?}",
                    error
                );
            }
        }
        times_received_chunks += 1;
    }
    let callback_elapsed = callback_sw.elapsed();
    if callback_elapsed > CALLBACK_DEADLINE {
        warn!(
            "Callback took too long ({:?}). \
             Received chunks {} times.",
            callback_elapsed, times_received_chunks
        );
    }
}
