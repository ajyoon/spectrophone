#![feature(test)]
#![feature(optin_builtin_traits)]
#![feature(proc_macro_hygiene, decl_macro)]
#![feature(drain_filter)]
#![feature(nll)]
#![feature(core_intrinsics)]
#![feature(div_duration)]

#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate glium;
extern crate captrs;

mod arrays;
mod constants;
mod control_messages;
mod fft;
mod gui;
mod ids;
mod img_dispatch;
mod layer;
mod layer_interpreter;
mod live_img_dispatch;
mod math;
mod mixer;
mod opencv_helpers;
mod sample_buffer;
mod spectrogram_preview;
//mod static_img_dispatch;
mod max_amp_manager;
mod synth;
mod transition;

pub mod audio_streamer;
pub mod camera;
pub mod color;
pub mod color_key;
pub mod control_server;
pub mod controller;
pub mod cpal_streamer;
pub mod expiring_mpsc;
pub mod heartbeat;
pub mod live_img_preview;
pub mod preview;
pub mod runtime_setup;
pub mod wav_streamer;

#[cfg(test)]
mod test_utils;
