use crate::mixer::Mixer;

pub trait AudioStreamer<T> {
    fn stream(&self, mixer: Mixer<T>);
}
