use crate::audio_streamer::AudioStreamer;
use crate::camera::CameraParams;
use crate::color_key::{ColorKey, HsvKey};
use crate::control_messages::*;
use crate::cpal_streamer::CpalAudioStreamer;
use crate::expiring_mpsc::{channel, Sender};
use crate::gui::WindowManager;
use crate::ids::LayerId;
use crate::img_dispatch::ImgDispatcher;
use crate::img_dispatch::LayerPacket;
use crate::layer::build_layer;
use crate::layer_interpreter::generate_color_similarity_interpreter;
use crate::live_img_dispatch::LiveImgDispatcher;
#[allow(unused_imports)]
use crate::live_img_preview::{ImgPacket, LiveImgPreview, OnlyMonitorLiveImgPreview};
use crate::mixer::Mixer;
use crate::opencv_helpers::log_opencv_build_info;
use crate::spectrogram_preview::{LiveSpectrogramDisplay, LiveSpectrogramPreview};
use crate::synth::Waveform;
use crate::transition::{Interpolation, Transition};
use std::clone::Clone;
use std::collections::HashMap;
use std::path::PathBuf;
use std::process;
use std::thread;
use std::thread::JoinHandle;
use std::time::{Duration, SystemTime, UNIX_EPOCH};

pub type SampleType = f32;
pub const SAMPLE_RATE: u32 = 44100;
pub const SPECTROGRAM_PREVIEW_HEIGHT: usize = 1280 / 3;
pub const FPS: f32 = 30.0;
const GLOBAL_AMP_MULTIPIER: f32 = 1.0;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct VoiceDescription {
    pub waveform: Waveform,
    pub frequency: f32,
    pub amplitude_factor: f32,
}

impl VoiceDescription {
    pub fn new(waveform: Waveform, frequency: f32, amplitude_factor: f32) -> VoiceDescription {
        VoiceDescription {
            waveform,
            frequency,
            amplitude_factor,
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct LayerDescription {
    pub key: Box<dyn ColorKey>,
    pub voices: Vec<VoiceDescription>,
    pub horizontal_regions: usize,
    pub highest_similarity_region_weight: f32,
    pub name: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct TransitionDescription {
    pub start_timestamp: u64,
    pub duration_ms: u64,
    pub interpolation: Interpolation,
}

impl TransitionDescription {
    pub fn to_transition(&self, start_val: f32, end_val: f32) -> Transition {
        Transition::new(
            UNIX_EPOCH + Duration::from_secs(self.start_timestamp),
            Duration::from_millis(self.duration_ms),
            start_val,
            end_val,
            self.interpolation,
        )
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct PutLayerRequest {
    pub layer: LayerDescription,
    pub transition: TransitionDescription,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct DeleteLayerRequest {
    pub transition: TransitionDescription,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CameraConfig {
    pub value: i32,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ControllerConfig {
    pub spectrogram_out_path: Option<PathBuf>,
    pub audio_out_path: Option<PathBuf>,
    pub camera_params: CameraParams,
}

pub struct Controller {
    layer_descriptions: HashMap<LayerId, LayerDescription>,
    img_dispatcher_controller: Sender<ImgDispatcherControlMessage>,
    layer_synth_controllers: HashMap<LayerId, Sender<LayerSynthControlMessage>>,
    live_img_preview_controller: Sender<LiveImgPreviewControlMessage>,
    audio_streamer_controller: Sender<AudioStreamerControlMessage<SampleType>>,
    spectrogram_preview_controller: Sender<SpectrogramPreviewControlMessage>,
    window_controller: Sender<WindowControlMessage>,
    join_handles: Vec<JoinHandle<()>>,
    scheduled_layer_shutdowns: Vec<(SystemTime, LayerId)>,
}

impl Controller {
    /// One-time setup for image dispatcher thread
    ///
    /// TODO only supports live img dispatch for now
    fn bootstrap_img_dispatcher(
        img_packet_sender: Option<Sender<ImgPacket>>,
        camera_params: CameraParams,
    ) -> (Sender<ImgDispatcherControlMessage>, JoinHandle<()>) {
        let mut dispatcher = LiveImgDispatcher::new(FPS, camera_params);
        let (control_sender, control_receiver) = channel::<ImgDispatcherControlMessage>();

        let join_handle = thread::Builder::new()
            .name("ImageDispatch".to_string())
            .spawn(move || {
                dispatcher.begin_dispatch(control_receiver, img_packet_sender);
            })
            .unwrap();

        (control_sender, join_handle)
    }

    /// One-time setup for spectrogram preview thread
    fn bootstrap_spectrogram_preview(
        out_path: Option<PathBuf>,
        window_controller: Sender<WindowControlMessage>,
    ) -> (Sender<SpectrogramPreviewControlMessage>, JoinHandle<()>) {
        let spectrogram_display = Some(LiveSpectrogramDisplay::new(window_controller));
        let (control_sender, control_receiver) = channel::<SpectrogramPreviewControlMessage>();
        let mut spectrogram_preview = LiveSpectrogramPreview::new(
            control_receiver,
            SAMPLE_RATE,
            SPECTROGRAM_PREVIEW_HEIGHT,
            spectrogram_display,
            out_path,
        );

        let join_handle = thread::Builder::new()
            .name("LiveSpectrogramPreview".to_string())
            .spawn(move || {
                spectrogram_preview.live_preview();
            })
            .unwrap();

        (control_sender, join_handle)
    }

    /// One-time setup for the live image preview thread
    fn bootstrap_live_img_preview(
        window_controller: Sender<WindowControlMessage>,
    ) -> (
        Sender<LiveImgPreviewControlMessage>,
        Sender<ImgPacket>,
        JoinHandle<()>,
    ) {
        let (control_sender, control_receiver) = channel::<LiveImgPreviewControlMessage>();

        // let (mut live_img_preview, img_packet_sender) =
        //     OnlyMonitorLiveImgPreview::new(control_receiver, window_controller);
        let (mut live_img_preview, img_packet_sender) =
            LiveImgPreview::new(control_receiver, window_controller);

        let join_handle = thread::Builder::new()
            .name("LiveImgPreview".to_string())
            .spawn(move || live_img_preview.begin())
            .unwrap();
        (control_sender, img_packet_sender, join_handle)
    }

    /// One-time setup for the audio streamer thread
    ///
    /// TODO only supports cpal streamer for now
    fn bootstrap_audio_streamer() -> (Sender<AudioStreamerControlMessage<f32>>, JoinHandle<()>) {
        let mixer = Mixer::<f32>::new();

        let (control_sender, control_receiver) = channel::<AudioStreamerControlMessage<f32>>();

        let streamer = CpalAudioStreamer::new(control_receiver);

        let join_handle = thread::Builder::new()
            .name("AudioStreamer".to_string())
            .spawn(move || streamer.stream(mixer))
            .unwrap();

        (control_sender, join_handle)
    }

    fn bootstrap_window_manager() -> (Sender<WindowControlMessage>, JoinHandle<()>) {
        let (control_sender, control_receiver) = channel::<WindowControlMessage>();
        let join_handle = thread::Builder::new()
            .name("WindowManager".to_string())
            .spawn(move || {
                let mut window_manager = WindowManager::new(control_receiver);
                window_manager.run();
            })
            .unwrap();
        (control_sender, join_handle)
    }

    pub fn bootstrap(config: ControllerConfig) -> Controller {
        Self::startup_logs();
        let (audio_streamer_controller, audio_streamer_join_handle) =
            Self::bootstrap_audio_streamer();
        let (window_controller, window_manager_join_handle) = Self::bootstrap_window_manager();
        let (live_img_preview_controller, img_packet_sender, live_img_preview_join_handle) =
            Self::bootstrap_live_img_preview(window_controller.clone());
        let (img_dispatcher_controller, img_dispatcher_join_handle) =
            Self::bootstrap_img_dispatcher(Some(img_packet_sender), config.camera_params);
        let (spectrogram_preview_controller, spectrogram_preview_join_handle) =
            Self::bootstrap_spectrogram_preview(
                config.spectrogram_out_path,
                window_controller.clone(),
            );

        let layer_synth_controllers = HashMap::new();

        let join_handles = vec![
            audio_streamer_join_handle,
            live_img_preview_join_handle,
            img_dispatcher_join_handle,
            spectrogram_preview_join_handle,
            window_manager_join_handle,
        ];

        let layer_descriptions = HashMap::new();
        let scheduled_layer_shutdowns = vec![];

        Controller {
            layer_descriptions,
            img_dispatcher_controller,
            layer_synth_controllers,
            live_img_preview_controller,
            audio_streamer_controller,
            spectrogram_preview_controller,
            window_controller,
            join_handles,
            scheduled_layer_shutdowns,
        }
    }

    pub fn get_status(&self) -> HashMap<LayerId, LayerDescription> {
        self.layer_descriptions.clone()
    }

    pub fn shutdown(&mut self) {
        info!("initiating shutdown");
        self.send_shutdown_signals_to_all_controllers();
        self.join_all_handles();
        info!("all threads successfully shutdown. killing process. goodbye :')");
        process::exit(0);
    }

    pub fn put_layer(
        &mut self,
        id: LayerId,
        layer_description: LayerDescription,
        transition_description: TransitionDescription,
    ) -> Result<(), &str> {
        if self.layer_descriptions.contains_key(&id) {
            warn!(
                "Got request to put a layer at id {}, but it already exists. Ignoring.",
                id
            );
            return Ok(()); // Should really do an error return, but the caller unwraps this...
        }

        let prev_max_amp = self.current_max_amp();

        let LayerDescription {
            key,
            voices,
            horizontal_regions,
            highest_similarity_region_weight,
            name,
        } = layer_description.clone();
        let voice_count = voices.len();
        self.layer_descriptions.insert(id, layer_description);
        self.adjust_max_amp(prev_max_amp, transition_description.clone());

        let (_layer_packet_sender, _layer_packet_receiver) = channel::<LayerPacket>();
        let (layer_control_sender, layer_control_receiver) = channel::<LayerSynthControlMessage>();
        let (layer_samples_sender, layer_samples_receiver) = channel::<Vec<f32>>();
        let (layer_preview_samples_sender, layer_preview_samples_receiver) = channel::<Vec<f32>>();

        self.layer_synth_controllers
            .insert(id, layer_control_sender);
        let color_similarity_interpreter = generate_color_similarity_interpreter(
            key.clone(),
            horizontal_regions,
            voice_count,
            highest_similarity_region_weight,
        );

        let (layer_dispatcher, layer_synth) = build_layer(
            layer_control_receiver,
            color_similarity_interpreter,
            voices,
            layer_samples_sender,
            layer_preview_samples_sender,
            SAMPLE_RATE,
            Duration::from_millis(3),
            transition_description,
        );

        self.img_dispatcher_controller
            .send(ImgDispatcherControlMessage::ConnectLayer {
                id,
                layer_dispatcher,
            })
            .unwrap();
        self.live_img_preview_controller
            .send(LiveImgPreviewControlMessage::ConnectLayer {
                id,
                layer: (key.clone(), horizontal_regions, voice_count, name),
            })
            .unwrap();
        thread::sleep(Duration::from_millis(3500));
        self.audio_streamer_controller
            .send(AudioStreamerControlMessage::ConnectLayer {
                id,
                layer_samples_receiver,
            })
            .unwrap();
        self.spectrogram_preview_controller
            .send(SpectrogramPreviewControlMessage::ConnectLayer {
                id,
                key,
                sample_receiver: layer_preview_samples_receiver,
            })
            .unwrap();

        self.join_handles.push(layer_synth.launch_thread().unwrap());

        Ok(())
    }

    pub fn delete_layer(&mut self, id: LayerId, transition_description: TransitionDescription) {
        if !self.layer_descriptions.contains_key(&id) {
            warn!("Got delete request for nonexistent id {}. Ignoring.", id);
            return;
        }
        // Begin amplitude fades
        let prev_max_amp = self.current_max_amp();
        self.layer_descriptions.remove(&id);

        let transition = transition_description.to_transition(1.0, 0.0);

        self.adjust_max_amp(prev_max_amp, transition_description.clone());
        self.layer_synth_controllers
            .get(&id)
            .unwrap()
            .send(LayerSynthControlMessage::AdjustLayerAmp {
                transition: transition.clone(),
            })
            .unwrap();
        // Schedule actual layer deletion after transition completion time
        self.scheduled_layer_shutdowns.push((
            transition.start_time + Duration::from_millis(transition_description.duration_ms),
            id,
        ));
    }

    pub fn handle_layer_shutdowns(&mut self) {
        let now = SystemTime::now();
        let due_shutdowns: Vec<(SystemTime, LayerId)> = self
            .scheduled_layer_shutdowns
            .drain_filter(|s| s.0 < now)
            .collect();
        for due_shutdown in due_shutdowns {
            self.complete_layer_shutdown(due_shutdown.1);
        }
    }

    fn complete_layer_shutdown(&mut self, id: LayerId) {
        self.layer_synth_controllers
            .get(&id)
            .unwrap()
            .send(LayerSynthControlMessage::Shutdown)
            .unwrap();
        self.layer_synth_controllers.remove(&id);
        self.img_dispatcher_controller
            .send(ImgDispatcherControlMessage::DisconnectLayer { id })
            .unwrap();
        self.live_img_preview_controller
            .send(LiveImgPreviewControlMessage::DisconnectLayer { id })
            .unwrap();
        self.audio_streamer_controller
            .send(AudioStreamerControlMessage::DisconnectLayer { id })
            .unwrap();
        self.spectrogram_preview_controller
            .send(SpectrogramPreviewControlMessage::DisconnectLayer { id })
            .unwrap();
    }

    pub fn put_camera_config(&mut self, name: String, camera_config: CameraConfig) {
        self.img_dispatcher_controller
            .send(ImgDispatcherControlMessage::SetNumericCameraConfig {
                name,
                value: camera_config.value,
            })
            .unwrap();
    }

    fn current_max_amp(&self) -> f32 {
        self.layer_descriptions.values().fold(0f32, |sum, layer| {
            sum + layer
                .voices
                .iter()
                .fold(0f32, |sum, voice| sum + voice.amplitude_factor)
        }) / GLOBAL_AMP_MULTIPIER
    }

    fn adjust_max_amp(&mut self, prev_max_amp: f32, transition_description: TransitionDescription) {
        let current_max_amp = self.current_max_amp();
        let max_amp_delta = current_max_amp - prev_max_amp;
        let delta_transition = transition_description.to_transition(0.0, max_amp_delta);
        self.audio_streamer_controller
            .send(AudioStreamerControlMessage::AdjustExpectedMaxAmp { delta_transition })
            .unwrap();
    }

    fn send_shutdown_signals_to_all_controllers(&self) {
        self.img_dispatcher_controller
            .send(ImgDispatcherControlMessage::Shutdown)
            .unwrap();
        self.layer_synth_controllers
            .values()
            .for_each(|controller| controller.send(LayerSynthControlMessage::Shutdown).unwrap());
        self.live_img_preview_controller
            .send(LiveImgPreviewControlMessage::Shutdown)
            .unwrap();
        self.audio_streamer_controller
            .send(AudioStreamerControlMessage::Shutdown)
            .unwrap();
        self.spectrogram_preview_controller
            .send(SpectrogramPreviewControlMessage::Shutdown)
            .unwrap();
        self.window_controller
            .send(WindowControlMessage::Shutdown)
            .unwrap();
    }

    fn join_all_handles(&mut self) {
        self.join_handles
            .drain(..)
            .for_each(|handle: thread::JoinHandle<()>| {
                info!(
                    "Waiting for thread to finish: {:#?}",
                    handle.thread().name()
                );
                match handle.join() {
                    Ok(()) => (),
                    Err(h) => error!("Failed to join handle: {:?}", h),
                }
            });
    }

    fn startup_logs() {
        log_opencv_build_info();
    }
}
