use std::borrow::Cow;
use std::collections::HashMap;
use std::slice;
use std::thread;
use std::time::Duration;

use glium::{glutin, Surface};
use opencv::core;
use opencv::core::Mat;
use rand::random;

use crate::constants::{DEFAULT_FRAME_TTL, MAX_IMG_QUEUE_DEPTH};
use crate::control_messages::WindowControlMessage;
use crate::expiring_mpsc::{channel, Receiver, Sender, TryRecvError};
use crate::heartbeat::Heartbeat;
use crate::ids::WindowId;
use crate::live_img_preview::ImgPacket;

const WINDOW_SLEEP: Duration = Duration::from_millis(5);
const WINDOW_DISCONNECTED_SLEEP: Duration = Duration::from_secs(1);

impl<'a> glium::texture::Texture2dDataSource<'a> for ImgPacket {
    type Data = f32;

    fn into_raw(self) -> glium::texture::RawImage2d<'a, Self::Data> {
        let mat = ImgPacket::into_image(self);

        debug_assert_eq!(mat.typ().unwrap(), core::CV_32FC3);
        // TODO this should be handled more gracefully; non-continuous mats here
        // will cause a segfault
        debug_assert_eq!(mat.is_continuous().unwrap(), true);

        let size = mat.size().unwrap();
        let raw_buffer = unsafe {
            let ptr: *const f32 = &(*mat.at_2d_unchecked::<core::Vec3f>(0, 0).unwrap())[0];
            slice::from_raw_parts(ptr, (size.width * size.height * 3) as usize)
        };

        glium::texture::RawImage2d {
            data: Cow::Owned(raw_buffer.to_vec()),
            width: size.width as u32,
            height: size.height as u32,
            format: glium::texture::ClientFormat::F32F32F32,
        }
    }
}

#[derive(Copy, Clone)]
struct Vertex {
    position: [f32; 2],
    tex_coords: [f32; 2],
}

implement_vertex!(Vertex, position, tex_coords);

struct GlImageDisplayWindow {
    events_loop: glutin::EventsLoop,
    display: glium::Display,
    gl_program: glium::Program,
    texture: glium::texture::Texture2d,
    vertex_buffer: glium::VertexBuffer<Vertex>,
}

impl GlImageDisplayWindow {
    fn new(name: String) -> GlImageDisplayWindow {
        let events_loop = glutin::EventsLoop::new();
        let gl_window = glutin::WindowBuilder::new().with_title(name);
        let context = glutin::ContextBuilder::new()
            .with_vsync(true)
            .with_double_buffer(Some(true));
        let display = glium::Display::new(gl_window, context, &events_loop).unwrap();

        let v_top_left = Vertex {
            position: [-1.0, 1.0],
            tex_coords: [0.0, 0.0],
        };
        let v_bottom_left = Vertex {
            position: [-1.0, -1.0],
            tex_coords: [0.0, 1.0],
        };
        let v_top_right = Vertex {
            position: [1.0, 1.0],
            tex_coords: [1.0, 0.0],
        };
        let v_bottom_right = Vertex {
            position: [1.0, -1.0],
            tex_coords: [1.0, 1.0],
        };
        let shape = vec![
            v_top_left,
            v_bottom_left,
            v_top_right,
            v_top_right,
            v_bottom_left,
            v_bottom_right,
        ];

        let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();

        let vertex_shader_src = r#"
        #version 140
        in vec2 position;
        in vec2 tex_coords;
        out vec2 v_tex_coords;
        uniform mat4 matrix;
        void main() {
            v_tex_coords = tex_coords;
            gl_Position = vec4(position, 0.0, 1.0);
        }
        "#;

        let fragment_shader_src = r#"
        #version 140
        in vec2 v_tex_coords;
        out vec4 color;
        uniform sampler2D tex;
        void main() {
            color = texture(tex, v_tex_coords).zyxw;
        }
        "#;

        let source = glium::program::ProgramCreationInput::SourceCode {
            tessellation_control_shader: None,
            tessellation_evaluation_shader: None,
            geometry_shader: None,
            transform_feedback_varyings: None,
            uses_point_size: false,
            vertex_shader: vertex_shader_src,
            fragment_shader: fragment_shader_src,
            outputs_srgb: true,
        };
        let gl_program = glium::Program::new(&display, source).unwrap();
        let gl_image =
            glium::texture::RawImage2d::from_raw_rgb(vec![0f32; 640 * 426 * 3], (640, 426));
        let texture = glium::texture::Texture2d::with_format(
            &display,
            gl_image,
            glium::texture::UncompressedFloatFormat::F32F32F32,
            glium::texture::MipmapsOption::NoMipmap,
        )
        .unwrap();

        GlImageDisplayWindow {
            events_loop,
            display,
            gl_program,
            texture,
            vertex_buffer,
        }
    }

    fn show_frame(&mut self, frame: ImgPacket) -> Result<(), Box<dyn std::error::Error>> {
        self.texture = glium::texture::Texture2d::with_format(
            &self.display,
            frame,
            glium::texture::UncompressedFloatFormat::F32F32F32,
            glium::texture::MipmapsOption::NoMipmap,
        )?;
        let mut target = self.display.draw();
        target.clear_color(0.0, 0.0, 0.0, 1.0);

        let uniforms = uniform! {
            tex: &self.texture,
        };
        target.draw(
            &self.vertex_buffer,
            &glium::index::NoIndices(glium::index::PrimitiveType::TrianglesList),
            &self.gl_program,
            &uniforms,
            &Default::default(),
        )?;
        target.finish()?;
        Ok(())
    }

    #[allow(dead_code)]
    fn handle_events(&mut self) {
        self.events_loop.poll_events(|event| match event {
            glutin::Event::WindowEvent { event, .. } => {
                info!("received gl event_loop event: {:?}", event);
            }
            _ => (),
        });
    }
}

pub struct WindowManager {
    windows: HashMap<WindowId, (Receiver<ImgPacket>, GlImageDisplayWindow)>,
    control_receiver: Receiver<WindowControlMessage>,
}

impl WindowManager {
    pub fn new(control_receiver: Receiver<WindowControlMessage>) -> WindowManager {
        WindowManager {
            windows: HashMap::new(),
            control_receiver,
        }
    }

    pub fn run(&mut self) {
        let mut heartbeat = Heartbeat::new(Duration::from_secs(5), "WindowManager".to_string());
        loop {
            heartbeat.mark();
            match self.control_receiver.try_recv() {
                Ok(message) => match message {
                    WindowControlMessage::Shutdown => {
                        info!("Received shutdown signal. Shutting down.");
                        break;
                    }
                    WindowControlMessage::DisconnectWindow { id } => {
                        self.disconnect_window(id);
                    }
                    WindowControlMessage::ConnectWindow {
                        id,
                        name,
                        packet_receiver,
                    } => {
                        self.connect_window(id, name, packet_receiver);
                    }
                },
                Err(e) => match e {
                    TryRecvError::Empty => (),
                    TryRecvError::Disconnected => {
                        break;
                    }
                },
            }

            for (packet_receiver, window) in self.windows.values_mut() {
                match packet_receiver.try_recv() {
                    Ok(packet) => match window.show_frame(packet) {
                        Ok(()) => (),
                        Err(e) => {
                            error!("Failed to render frame in GL window: {:?}", e);
                        }
                    },
                    Err(e) => match e {
                        TryRecvError::Empty => (),
                        TryRecvError::Disconnected => {
                            warn!("Window was unexpectedly disconnected");
                            thread::sleep(WINDOW_DISCONNECTED_SLEEP);
                        }
                    },
                }
            }
            thread::sleep(WINDOW_SLEEP);
        }
    }

    fn disconnect_window(&mut self, id: WindowId) {
        self.windows.remove(&id);
    }

    fn connect_window(&mut self, id: WindowId, name: String, packet_receiver: Receiver<ImgPacket>) {
        let window = GlImageDisplayWindow::new(name);
        self.windows.insert(id, (packet_receiver, window));
    }
}

pub struct Window {
    pub id: WindowId,
    pub name: String,
    packet_sender: Sender<ImgPacket>,
}

impl Window {
    pub fn new(name: String, controller: &Sender<WindowControlMessage>) -> Result<Window, String> {
        let id: WindowId = random();
        let (packet_sender, packet_receiver) = channel::<ImgPacket>();
        match controller.send(WindowControlMessage::ConnectWindow {
            id,
            name: name.clone(),
            packet_receiver,
        }) {
            Err(e) => {
                return Err(format!("{:?}", e));
            }
            _ => (),
        }
        Ok(Window {
            id,
            name,
            packet_sender,
        })
    }

    pub fn show_frame(&self, frame: Mat) {
        let img_packet = ImgPacket::from_mat(frame);
        match self.packet_sender.send_if_depth_below(
            img_packet,
            DEFAULT_FRAME_TTL,
            MAX_IMG_QUEUE_DEPTH,
        ) {
            Err(_) => {
                error!("Got error while sending preview packet");
            }
            Ok(maybe_rejected_packet) => match maybe_rejected_packet {
                Some(_) => {
                    warn!("Could not send packet because queue was backed up.");
                }
                None => (),
            },
        };
    }

    pub fn disconnect(&self, controller: &Sender<WindowControlMessage>) {
        match controller.send(WindowControlMessage::DisconnectWindow { id: self.id }) {
            Err(e) => {
                error!(
                    "Failed to send DisconnectWindow signal for id {}: {:?}",
                    self.id, e
                );
            }
            _ => (),
        }
    }
}
