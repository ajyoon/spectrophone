use std::collections::HashMap;
use std::mem::forget;
use std::sync::Arc;
use std::thread;
use std::time::Duration;

use opencv::core::Mat;

use crate::color_key::ColorKey;
use crate::control_messages::{LiveImgPreviewControlMessage, WindowControlMessage};
use crate::expiring_mpsc::{channel, Receiver, Sender, TryRecvError};
use crate::gui::Window;
use crate::heartbeat::Heartbeat;
use crate::ids::LayerId;
use crate::layer_interpreter::color_similarity_preview;

static SLEEP: Duration = Duration::from_millis(10);

#[derive(Debug)]
pub struct ImgPacket {
    image_ptr: Arc<*mut libc::c_void>,
}

unsafe impl Send for ImgPacket {}

impl ImgPacket {
    pub fn from_mat(mat: Mat) -> ImgPacket {
        let packet = ImgPacket {
            image_ptr: Arc::new(mat.as_raw_Mat()),
        };
        forget(mat);
        packet
    }

    pub fn into_image(mut packet: ImgPacket) -> Mat {
        let raw_ptr = *Arc::get_mut(&mut packet.image_ptr).unwrap();
        let mat = unsafe { Mat::from_raw_ptr(raw_ptr) };
        forget(packet);
        mat
    }
}

impl Drop for ImgPacket {
    fn drop(&mut self) {
        // Create mat on pointer so the drop() on it will decrement the refcount
        let ptr = *Arc::get_mut(&mut self.image_ptr).unwrap();
        drop(unsafe { Mat::from_raw_ptr(ptr) });
    }
}

struct PreviewLayer {
    window: Window,
    key: Box<dyn ColorKey>,
    horizontal_regions: usize,
    segment_count: usize,
}

pub struct LiveImgPreview {
    layers: HashMap<LayerId, PreviewLayer>,
    monitor_window: Window,
    inbound_packet_receiver: Receiver<ImgPacket>,
    control_receiver: Receiver<LiveImgPreviewControlMessage>,
    #[allow(dead_code)]
    window_controller: Sender<WindowControlMessage>,
}

impl LiveImgPreview {
    pub fn new(
        control_receiver: Receiver<LiveImgPreviewControlMessage>,
        window_controller: Sender<WindowControlMessage>,
    ) -> (LiveImgPreview, Sender<ImgPacket>) {
        let (inbound_packet_sender, inbound_packet_receiver) = channel::<ImgPacket>();
        let layers = HashMap::new();
        let monitor_window = Window::new("monitor".to_string(), &window_controller).unwrap();
        (
            LiveImgPreview {
                layers,
                monitor_window,
                inbound_packet_receiver,
                control_receiver,
                window_controller,
            },
            inbound_packet_sender,
        )
    }

    pub fn connect_layer(
        &mut self,
        layer_id: LayerId,
        layer: (Box<dyn ColorKey>, usize, usize, String),
    ) {
        let name = layer.3;
        let window = Window::new(name, &self.window_controller).unwrap();
        let prepared_layer = PreviewLayer {
            window,
            key: layer.0,
            horizontal_regions: layer.1,
            segment_count: layer.2,
        };
        self.layers.insert(layer_id, prepared_layer);
    }

    pub fn disconnect_layer(&mut self, layer_id: LayerId) {
        self.layers
            .get(&layer_id)
            .unwrap()
            .window
            .disconnect(&self.window_controller);
        self.layers.remove(&layer_id);
    }

    pub fn begin(&mut self) {
        let mut heartbeat =
            Heartbeat::new(Duration::from_secs(5), "live image preview".to_string());
        loop {
            heartbeat.mark();
            match self.control_receiver.try_recv() {
                Ok(message) => match message {
                    LiveImgPreviewControlMessage::Shutdown => {
                        info!("Received shutdown signal. Shutting down.");
                        break;
                    }
                    LiveImgPreviewControlMessage::DisconnectLayer { id } => {
                        self.disconnect_layer(id);
                    }
                    LiveImgPreviewControlMessage::ConnectLayer { id, layer } => {
                        self.connect_layer(id, layer);
                    }
                },
                Err(e) => match e {
                    TryRecvError::Empty => (),
                    TryRecvError::Disconnected => {
                        break;
                    }
                },
            }

            match self.inbound_packet_receiver.try_recv() {
                Ok(packet) => {
                    self.handle_inbound_img_packet(packet);
                }
                Err(e) => match e {
                    TryRecvError::Empty => {
                        thread::sleep(SLEEP);
                    }
                    TryRecvError::Disconnected => {
                        break;
                    }
                },
            }
        }
    }

    fn handle_inbound_img_packet(&self, packet: ImgPacket) {
        let mut frame = ImgPacket::into_image(packet);
        // Individual color layers
        for layer in self.layers.values() {
            let filtered_image = color_similarity_preview(
                &mut frame,
                layer.horizontal_regions,
                layer.segment_count,
                &layer.key,
            );
            layer.window.show_frame(filtered_image);
        }
        // Camera monitor
        self.monitor_window.show_frame(frame);
    }
}

pub struct OnlyMonitorLiveImgPreview {
    monitor_window: Window,
    inbound_packet_receiver: Receiver<ImgPacket>,
    control_receiver: Receiver<LiveImgPreviewControlMessage>,
    #[allow(dead_code)]
    window_controller: Sender<WindowControlMessage>,
}

// big hack
impl OnlyMonitorLiveImgPreview {
    pub fn new(
        control_receiver: Receiver<LiveImgPreviewControlMessage>,
        window_controller: Sender<WindowControlMessage>,
    ) -> (OnlyMonitorLiveImgPreview, Sender<ImgPacket>) {
        let (inbound_packet_sender, inbound_packet_receiver) = channel::<ImgPacket>();
        let monitor_window = Window::new("monitor".to_string(), &window_controller).unwrap();
        (
            OnlyMonitorLiveImgPreview {
                monitor_window,
                inbound_packet_receiver,
                control_receiver,
                window_controller,
            },
            inbound_packet_sender,
        )
    }

    pub fn begin(&mut self) {
        let mut heartbeat =
            Heartbeat::new(Duration::from_secs(5), "live image preview".to_string());
        loop {
            heartbeat.mark();
            match self.control_receiver.try_recv() {
                Ok(message) => match message {
                    LiveImgPreviewControlMessage::Shutdown => {
                        info!("Received shutdown signal. Shutting down.");
                        break;
                    }
                    LiveImgPreviewControlMessage::DisconnectLayer { id: _ } => {
                        info!("ignoring noop'ed DisconnectLayer message");
                    }
                    LiveImgPreviewControlMessage::ConnectLayer { id: _, layer: _ } => {
                        info!("ignoring noop'ed ConnectLayer message");
                    }
                },
                Err(e) => match e {
                    TryRecvError::Empty => (),
                    TryRecvError::Disconnected => {
                        break;
                    }
                },
            }

            match self.inbound_packet_receiver.try_recv() {
                Ok(packet) => {
                    self.handle_inbound_img_packet(packet);
                }
                Err(e) => match e {
                    TryRecvError::Empty => {
                        thread::sleep(SLEEP);
                    }
                    TryRecvError::Disconnected => {
                        break;
                    }
                },
            }
        }
    }

    fn handle_inbound_img_packet(&self, packet: ImgPacket) {
        let frame = ImgPacket::into_image(packet);
        // Camera monitor
        self.monitor_window.show_frame(frame);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::color::Rgb;
    use crate::test_utils::*;
    use opencv::core;

    #[test]
    fn test_img_packet_pointer_identities() {
        let mut mat = unsafe { Mat::new_rows_cols(2, 3, core::CV_8UC1).unwrap() };
        set_mat_pixel(&mut mat, 0, 0, Rgb(1, 2, 3));
        set_mat_pixel(&mut mat, 1, 0, Rgb(4, 2, 3));
        set_mat_pixel(&mut mat, 2, 0, Rgb(5, 2, 2));
        set_mat_pixel(&mut mat, 0, 1, Rgb(6, 2, 3));
        set_mat_pixel(&mut mat, 1, 1, Rgb(7, 2, 3));
        set_mat_pixel(&mut mat, 2, 1, Rgb(8, 2, 3));
        assert_eq!(mat.size().unwrap().width, 3);
        assert_eq!(mat.size().unwrap().height, 2);
        let img_packet = ImgPacket::from_mat(mat);
        let should_be_same = ImgPacket::into_image(img_packet);
        assert_eq!(should_be_same.size().unwrap().width, 3);
        assert_eq!(should_be_same.size().unwrap().height, 2);
    }
}
