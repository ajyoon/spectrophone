import matplotlib.pyplot as plt
import matplotlib.ticker

from layer import Waveform


def show_freqs(layer):
    fig, axis = plt.subplots()
    freqs = [
        (waveform,
         [v.frequency for v in layer.voices if v.waveform == waveform])
        for waveform in Waveform
    ]
    freqs = [f for f in freqs if f[1]]
    labels = [str(f[0]).split('.')[-1] for f in freqs]
    weights = [
        [v.amplitude_factor for v in layer.voices if v.waveform == f[0]]
        for f in freqs
    ]
    freqs = [f[1] for f in freqs]
    plt.xscale('log', basex=1.5)
    plt.hist(
        freqs,
        weights=weights,
        label=labels,
        bins=80,
        stacked=True)
    axis.get_xaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
    plt.legend()
    plt.show()
