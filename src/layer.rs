use std::io;
use std::thread;
use std::thread::JoinHandle;
use std::time::Duration;

use crate::control_messages::LayerSynthControlMessage;
use crate::controller::{TransitionDescription, VoiceDescription};
use crate::expiring_mpsc::{channel, Receiver, RecvTimeoutError, Sender, TryRecvError};
use crate::img_dispatch::{LayerDispatcher, LayerPacket};
use crate::layer_interpreter::LayerInterpreterClosure;
use crate::mixer;
use crate::synth::Oscillator;
use crate::transition::Transition;

const INITIAL_AMPLITUDE: Amplitude = 0.0;
const SYNTH_NO_DATA_SLEEP: Duration = Duration::from_millis(5);
const MAX_SAMPLE_PACKET_QUEUE_DEPTH: isize = 1;
const SAMPLE_PACKET_QUEUE_FULL_SLEEP: Duration = Duration::from_millis(5);
const MAX_PREVIEW_SAMPLE_PACKETS_PER_SEC: f32 = 60.0;

type Amplitude = f32;

/// Construct a layer from its various components
///
/// # Note
///
/// `voices` is given in order from visually lowest to visually highest. In most
/// cases this also means lowest in frequency to highest in frequency. Voices
/// are, however, stored in the layer synth in the opposite order (to align with
/// image coordinate systems where y=0 is the top of the image). This function
/// takes care of reversing the order of voices given.
pub fn build_layer(
    control_receiver: Receiver<LayerSynthControlMessage>,
    prepared_layer_interpreter: LayerInterpreterClosure,
    voices: Vec<VoiceDescription>,
    samples_sender: Sender<Vec<f32>>,
    preview_samples_sender: Sender<Vec<f32>>,
    sample_rate: u32,
    packet_recv_timeout: Duration,
    transition_description: TransitionDescription,
) -> (LayerDispatcher, LayerSynth) {
    let (packet_sender, packet_receiver) = channel::<LayerPacket>();
    let layer_dispatcher = LayerDispatcher {
        sender: packet_sender,
        layer_interpreter: prepared_layer_interpreter,
    };
    let segment_synths = voices
        .into_iter()
        .rev()
        .map(|v| SegmentSynth::new(Oscillator::new(v, sample_rate)))
        .collect();
    let transition = transition_description.to_transition(0.0, 1.0);
    let layer_synth = LayerSynth::new(
        control_receiver,
        packet_receiver,
        samples_sender,
        preview_samples_sender,
        segment_synths,
        sample_rate,
        packet_recv_timeout,
        transition,
    );
    (layer_dispatcher, layer_synth)
}

pub struct SegmentSynth {
    pub oscillator: Oscillator,
    last_amplitude: Amplitude,
}

pub struct LayerSynth {
    control_receiver: Receiver<LayerSynthControlMessage>,
    packet_receiver: Receiver<LayerPacket>,
    samples_sender: Sender<Vec<f32>>,
    preview_samples_sender: Sender<Vec<f32>>,
    segment_synths: Vec<SegmentSynth>,
    sample_rate: u32,
    last_layer_packet: Option<LayerPacket>,
    packet_recv_timeout: Duration,
    accumulated_preview_packet_samples: Vec<f32>,
    layer_amp: Amplitude,
    active_layer_amp_transition: Option<Transition>,
}

impl SegmentSynth {
    pub fn new(oscillator: Oscillator) -> SegmentSynth {
        SegmentSynth {
            oscillator,
            last_amplitude: INITIAL_AMPLITUDE,
        }
    }

    fn get_samples(&mut self, num_samples: usize, amplitude: Amplitude) -> Vec<f32> {
        let samples = self.oscillator.get_samples_with_interpolated_amp(
            num_samples,
            self.last_amplitude,
            amplitude,
        );
        self.last_amplitude = amplitude;
        samples
    }
}

impl LayerSynth {
    pub fn new(
        control_receiver: Receiver<LayerSynthControlMessage>,
        packet_receiver: Receiver<LayerPacket>,
        samples_sender: Sender<Vec<f32>>,
        preview_samples_sender: Sender<Vec<f32>>,
        segment_synths: Vec<SegmentSynth>,
        sample_rate: u32,
        packet_recv_timeout: Duration,
        initial_layer_amp_transition: Transition,
    ) -> LayerSynth {
        LayerSynth {
            control_receiver,
            packet_receiver,
            samples_sender,
            preview_samples_sender,
            segment_synths,
            sample_rate,
            packet_recv_timeout,
            last_layer_packet: None,
            accumulated_preview_packet_samples: vec![],
            layer_amp: initial_layer_amp_transition.current_val(),
            active_layer_amp_transition: Some(initial_layer_amp_transition),
        }
    }

    pub fn begin(&mut self) {
        loop {
            match self.control_receiver.try_recv() {
                Ok(message) => match message {
                    LayerSynthControlMessage::Shutdown => {
                        info!("Received shutdown signal. Shutting down.");
                        break;
                    }
                    LayerSynthControlMessage::AdjustLayerAmp { transition } => {
                        self.active_layer_amp_transition = Some(transition);
                    }
                },
                Err(e) => match e {
                    TryRecvError::Empty => (),
                    TryRecvError::Disconnected => {
                        break;
                    }
                },
            }

            let sample_packet_queue_depth = self.samples_sender.queue_depth.get_approx();
            if sample_packet_queue_depth > MAX_SAMPLE_PACKET_QUEUE_DEPTH {
                trace!(
                    "samples_sender.queue_depth ~= {}; pausing layer synthesis for {:?}",
                    sample_packet_queue_depth,
                    SAMPLE_PACKET_QUEUE_FULL_SLEEP
                );
                thread::sleep(SAMPLE_PACKET_QUEUE_FULL_SLEEP);
                continue;
            }

            match self.packet_receiver.recv_timeout(self.packet_recv_timeout) {
                Ok(packet) => {
                    self.handle_packet(&packet);
                    self.last_layer_packet = Some(packet);
                }
                Err(RecvTimeoutError::Timeout) => {
                    let last_layer_packet: Option<LayerPacket> = match &self.last_layer_packet {
                        Some(p) => Some(p.clone()),
                        None => None,
                    };
                    match last_layer_packet {
                        Some(packet) => {
                            warn!("No layer packet available, reusing last amplitudes.");
                            self.handle_packet(&packet);
                        }
                        None => {
                            warn!(
                                "No layer packet available or cached, retrying in {:?}",
                                SYNTH_NO_DATA_SLEEP
                            );
                            self.send_silence(SYNTH_NO_DATA_SLEEP);
                            thread::sleep(SYNTH_NO_DATA_SLEEP);
                        }
                    }
                }
                Err(RecvTimeoutError::Disconnected) => {
                    error!("Layer packet channel hung up.");
                    break;
                }
            }
        }
    }

    fn send_silence(&mut self, duration: Duration) {
        let num_samples = Self::samples_needed_for(self.sample_rate, duration);
        let samples = vec![0.; num_samples];
        match &self.samples_sender.send(samples.clone()) {
            Ok(_) => {}
            Err(_e) => error!("Failed to send mixed samples"),
        };
        self.handle_preview_packet_samples(samples);
    }

    /// Generate samples for a layer packet and send them down the `samples_sender` channel.
    #[inline]
    fn handle_packet(&mut self, packet: &LayerPacket) {
        debug_assert!(
            packet.amplitudes.len() == self.segment_synths.len(),
            "Expected packet to have same number of amplitudes as synth \
             segment interpreters."
        );
        match &self.active_layer_amp_transition {
            Some(transition) => {
                self.layer_amp = transition.current_val();
                if transition.is_complete() {
                    self.active_layer_amp_transition = None;
                }
            }
            None => {}
        }
        let num_samples = Self::samples_needed_for(self.sample_rate, packet.duration);
        let mut mixed_samples = vec![0.; num_samples];
        if self.layer_amp > 0.0 {
            for (amplitude, segment_synth) in packet.amplitudes.iter().zip(&mut self.segment_synths)
            {
                let section_samples =
                    segment_synth.get_samples(num_samples, (*amplitude) * self.layer_amp);
                mixer::add_chunk_to(&section_samples, &mut mixed_samples);
            }
        }
        match &self.samples_sender.send(mixed_samples.clone()) {
            Ok(_) => {}
            Err(_e) => error!("Failed to send mixed samples"),
        };
        self.handle_preview_packet_samples(mixed_samples);
    }

    fn handle_preview_packet_samples(&mut self, mut samples: Vec<f32>) {
        self.accumulated_preview_packet_samples.append(&mut samples);
        if (self.accumulated_preview_packet_samples.len() as f32 / self.sample_rate as f32)
            > (1.0 / MAX_PREVIEW_SAMPLE_PACKETS_PER_SEC)
        {
            match &self
                .preview_samples_sender
                .send(self.accumulated_preview_packet_samples.drain(..).collect())
            {
                Ok(_) => {}
                Err(_e) => error!("Failed to send preview mixed samples"),
            }
        }
    }

    fn samples_needed_for(sample_rate: u32, duration: Duration) -> usize {
        ((duration.as_micros() * sample_rate as u128) / 1_000_000) as usize
    }

    pub fn launch_thread(mut self) -> Result<JoinHandle<()>, io::Error> {
        thread::Builder::new()
            .name("LayerSynth".to_string())
            .spawn(move || {
                self.begin();
            })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    mod layer_synth {
        use super::*;
        use crate::transition::{Interpolation, Transition};

        fn dummy_layer_synth() -> (
            LayerSynth,
            Sender<LayerPacket>,
            Receiver<Vec<f32>>,
            Receiver<Vec<f32>>,
        ) {
            let (_control_sender, control_receiver) = channel::<LayerSynthControlMessage>();
            let (packet_sender, packet_receiver) = channel::<LayerPacket>();
            let (samples_sender, samples_receiver) = channel::<Vec<f32>>();
            let (preview_samples_sender, preview_samples_receiver) = channel::<Vec<f32>>();
            let segment_synths = vec![];
            let layer_synth = LayerSynth::new(
                control_receiver,
                packet_receiver,
                samples_sender,
                preview_samples_sender,
                segment_synths,
                1234,
                Duration::from_millis(1000),
                Transition::starting_now(Duration::from_millis(0), 0.0, 1.0, Interpolation::Linear),
            );

            (
                layer_synth,
                packet_sender,
                samples_receiver,
                preview_samples_receiver,
            )
        }

        #[test]
        fn test_layer_synth_launch_thread() {
            let (layer_synth, layer_packet_sender, _, _) = dummy_layer_synth();
            let join_handle = layer_synth.launch_thread().unwrap();
            drop(layer_packet_sender);
            join_handle.join().unwrap();
        }
    }
}
