use std::f32::consts;

use rand::{thread_rng, Rng};

use crate::arrays;
use crate::controller::VoiceDescription;

const TWO_PI: f32 = consts::PI * 2.;
const SINGLE_SIGNAL_MIN: f32 = -1.;
const SINGLE_SIGNAL_MAX: f32 = 1.;

fn period_length(frequency: f32, sample_rate: u32) -> u32 {
    return ((sample_rate as f32) / frequency) as u32;
}

trait PeriodGenerator {
    fn generate_period(&self, frequency: f32, sample_rate: u32) -> Vec<f32>;
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum Waveform {
    Sine,
    Square,
    Triangle,
    Sawtooth,
}

#[inline]
fn populate_sine_period(period: &mut Vec<f32>) {
    let x_scale = TWO_PI / (period.capacity() as f32);
    let y_scale = SINGLE_SIGNAL_MAX;
    for i in 0..period.capacity() {
        period.push(((i as f32) * x_scale).sin() * (y_scale));
    }
}

#[inline]
fn populate_square_period(period: &mut Vec<f32>) {
    let high_len = period.capacity() / 2;
    let low_len = period.capacity() - high_len;
    for _ in 0..high_len {
        period.push(SINGLE_SIGNAL_MAX);
    }
    for _ in 0..low_len {
        period.push(SINGLE_SIGNAL_MIN);
    }
}

#[inline]
fn populate_triangle_period(period: &mut Vec<f32>) {
    let slope = (SINGLE_SIGNAL_MAX - SINGLE_SIGNAL_MIN) / (period.capacity() / 2) as f32;
    let mut y = SINGLE_SIGNAL_MIN;
    let asc_len = period.capacity() / 2;
    for _ in 0..asc_len {
        period.push(y);
        y += slope;
    }
    let desc_len = period.capacity() - asc_len;
    y = SINGLE_SIGNAL_MAX;
    for _ in 0..desc_len {
        period.push(y);
        y -= slope;
    }
}

#[inline]
fn populate_sawtooth_period(period: &mut Vec<f32>) {
    let slope = (SINGLE_SIGNAL_MAX - SINGLE_SIGNAL_MIN) / period.capacity() as f32;
    let mut y = SINGLE_SIGNAL_MIN;
    for _ in 0..period.capacity() {
        period.push(y);
        y += slope;
    }
}

impl PeriodGenerator for Waveform {
    fn generate_period(&self, frequency: f32, sample_rate: u32) -> Vec<f32> {
        assert!(frequency > 0., "Invalid frequency: {}", frequency);
        let samples_needed = period_length(frequency, sample_rate);
        let mut period = Vec::<f32>::with_capacity(samples_needed as usize);

        match self {
            &Waveform::Sine => populate_sine_period(&mut period),
            &Waveform::Square => populate_square_period(&mut period),
            &Waveform::Triangle => populate_triangle_period(&mut period),
            &Waveform::Sawtooth => populate_sawtooth_period(&mut period),
        };
        return period;
    }
}

pub struct Oscillator {
    pub period_cache: Vec<f32>,
    pub amplitude_factor: f32,
    pub phase: usize,
}

impl Oscillator {
    pub fn new(voice_description: VoiceDescription, sample_rate: u32) -> Oscillator {
        let VoiceDescription {
            waveform,
            frequency,
            amplitude_factor,
        } = voice_description;
        let period_cache = waveform.generate_period(frequency, sample_rate);
        let phase = thread_rng().gen_range(0, period_cache.len());
        Oscillator {
            period_cache,
            amplitude_factor,
            phase,
        }
    }

    pub fn get_samples_with_interpolated_amp(
        &mut self,
        num: usize,
        start_amplitude: f32,
        end_amplitude: f32,
    ) -> Vec<f32> {
        let resolved_start_amplitude = start_amplitude * self.amplitude_factor;
        let resolved_end_amplitude = end_amplitude * self.amplitude_factor;
        let mut samples = arrays::roll_vec(&self.period_cache, self.phase, num);
        arrays::multiply_over_linspace(
            samples.as_mut_slice(),
            resolved_start_amplitude,
            resolved_end_amplitude,
        );
        self.phase = (&self.phase + num) % &self.period_cache.len();
        samples
    }
}

#[cfg(test)]
mod tests {
    extern crate time;
    use super::*;
    use crate::test_utils::*;

    mod waveforms {
        use super::*;

        mod sine {
            use super::*;

            #[test]
            fn compare_against_known_good_output() {
                let actual = Waveform::Sine.generate_period(2250., 44100);
                #[rustfmt::skip]
                let expected = vec![
                    // these values verified as sane by plotting and doing an eyeball check
                    0.0, 0.32469946, 0.6142127, 0.8371665, 0.9694003, 0.9965845,
                    0.91577333, 0.7357239, 0.4759474, 0.16459462, -0.16459456,
                    -0.47594735, -0.7357239, -0.9157734, -0.9965845,
                    -0.96940035, -0.8371665, -0.6142126, -0.32469952,
                ];
                assert_almost_eq_by_element(actual, expected);
            }

            #[test]
            fn capacity_used() {
                let period = Waveform::Sine.generate_period(10., 44100);
                assert_eq!(period.len(), period.capacity());
            }
        }

        mod square {
            use super::*;

            #[test]
            fn compare_against_known_good_output() {
                let actual = Waveform::Square.generate_period(4410., 44100);
                #[rustfmt::skip]
                let expected = vec![
                    // these values verified as sane by plotting and doing an eyeball check
                    1.0, 1.0, 1.0, 1.0, 1.0,
                    -1.0, -1.0, -1.0, -1.0, -1.0,
                ];
                assert_almost_eq_by_element(actual, expected);
            }

            #[test]
            fn capacity_used() {
                let period = Waveform::Square.generate_period(10., 44100);
                assert_eq!(period.len(), period.capacity());
            }
        }

        mod triangle {
            use super::*;

            #[test]
            fn compare_against_known_good_output() {
                let actual = Waveform::Triangle.generate_period(4410., 44100);
                #[rustfmt::skip]
                let expected = vec![
                    // these values verified as sane by plotting and doing an eyeball check
                    -1.0, -0.6, -0.20000002, 0.19999999, 0.6, 
                    1.0, 0.6, 0.20000002, -0.19999999, -0.6
                ];
                assert_almost_eq_by_element(actual, expected);
            }

            #[test]
            fn capacity_used() {
                let period = Waveform::Triangle.generate_period(10., 44100);
                assert_eq!(period.len(), period.capacity());
            }
        }

        mod sawtooth {
            use super::*;

            #[test]
            fn compare_against_known_good_output() {
                let actual = Waveform::Sawtooth.generate_period(4410., 44100);
                #[rustfmt::skip]
                let expected = vec![
                    // these values verified as sane by plotting and doing an eyeball check
                    -1.0, -0.8, -0.6, -0.40000004, -0.20000003,
                    -0.000000029802322, 0.19999997, 0.39999998,
                    0.59999996, 0.79999995
                ];
                assert_almost_eq_by_element(actual, expected);
            }

            #[test]
            fn capacity_used() {
                let period = Waveform::Sawtooth.generate_period(10., 44100);
                assert_eq!(period.len(), period.capacity());
            }
        }
    }

    mod oscillator {
        use super::*;
        extern crate test;

        #[test]
        fn get_samples_with_interpolated_amp_preserves_phase() {
            let voice = VoiceDescription::new(Waveform::Sine, 4410., 1.0);
            let mut osc = Oscillator::new(voice, 44100);
            osc.phase = 0;
            let mut samples = osc.get_samples_with_interpolated_amp(10, 1., 1.);
            samples.append(&mut osc.get_samples_with_interpolated_amp(10, 1., 1.));
            #[rustfmt::skip]
            let expected: Vec<f32> = vec![
                0.0, 0.58778524, 0.95105654, 0.9510565, 0.5877852,
                -0.00000008742278, -0.58778536, -0.9510565, -0.9510565, -0.58778495,
                0.0, 0.58778524, 0.95105654, 0.9510565, 0.5877852,
                -0.00000008742278, -0.58778536, -0.9510565, -0.9510565, -0.58778495,
            ];
            assert_almost_eq_by_element(samples, expected);
        }

        #[test]
        fn get_samples_with_interpolated_amp() {
            let voice = VoiceDescription::new(Waveform::Sine, 4410., 1.0);
            let mut osc = Oscillator::new(voice, 44100);
            osc.phase = 0;
            let samples = osc.get_samples_with_interpolated_amp(20, 0., 1.);
            #[rustfmt::skip]
            let expected: Vec<f32> = vec![
                0.0, 0.029389262, 0.095105655, 0.14265847, 0.11755704, 
                -0.000000021855694, -0.17633562, -0.33286977, -0.3804226, 
                -0.2645032, 0.0, 0.32328188, 0.57063395, 0.6181867, 0.4114496,
                -0.00000006556708, -0.47022828, -0.808398, -0.85595083, -0.5583957];
            assert_almost_eq_by_element(samples, expected);
        }

        #[bench]
        fn bench_generating_1_second_of_samples(b: &mut test::Bencher) {
            let voice = VoiceDescription::new(Waveform::Sine, 440., 1.0);
            let mut osc = Oscillator::new(voice, 44100);
            osc.phase = 0;
            b.iter(|| {
                osc.get_samples_with_interpolated_amp(44100, 1., 0.);
            });
        }
    }
}
