extern crate libc;


use std::mem;

use crate::math::lerp;


#[inline]
fn unsafe_memcpy(write_ptr: usize, src_ptr: usize, bytes: usize) {
    unsafe {
        libc::memcpy(
            write_ptr as *mut libc::c_void,
            src_ptr as *mut libc::c_void,
            bytes,
        );
    }
}

//#[inline]
pub fn roll_vec<T>(src_vec: &Vec<T>, src_offset: usize, elements: usize) -> Vec<T> {
    let element_size = mem::size_of::<T>();
    let roll_offset = src_offset % src_vec.len();
    let mut rolled = Vec::<T>::with_capacity(elements);
    unsafe {
        rolled.set_len(elements);
    }

    let mut src_ptr;
    let mut write_ptr = rolled.as_slice().as_ptr() as usize;

    let mut copied_elements = 0;

    if (src_vec.len() - roll_offset) > elements {
        // Handle case where the requested number of samples is smaller than the
        // remaining elements in src_vec after src_offset
        src_ptr = src_vec.as_slice().split_at(roll_offset).1.as_ptr() as usize;
        let bytes = elements * element_size;
        unsafe_memcpy(write_ptr, src_ptr, bytes);
        return rolled;
    } else if roll_offset != 0 {
        src_ptr = src_vec.as_slice().split_at(roll_offset).1.as_ptr() as usize;
        let bytes = (src_vec.len() - roll_offset) * element_size;
        unsafe_memcpy(write_ptr, src_ptr, bytes);
        write_ptr += bytes;
        copied_elements = src_vec.len() - roll_offset;
    }

    // Use repeated memcpys of compounding size to fill out as many bytes as possible
    let remaining_bytes = (elements - copied_elements) * element_size;
    let mut bytes_written = 0;
    src_ptr = src_vec.as_slice().as_ptr() as usize;
    let src_vec_bytes = src_vec.len() * element_size;

    if remaining_bytes > src_vec_bytes {
        unsafe_memcpy(write_ptr, src_ptr, src_vec_bytes);
        bytes_written = src_vec_bytes;
        src_ptr = write_ptr;
        write_ptr += bytes_written;
        while remaining_bytes - bytes_written >= bytes_written {
            unsafe_memcpy(write_ptr, src_ptr, bytes_written);
            write_ptr += bytes_written;
            bytes_written *= 2;
        }
    }

    // One last memcpy needed for any remaining bytes
    if bytes_written < remaining_bytes {
        unsafe_memcpy(write_ptr, src_ptr, remaining_bytes - bytes_written);
    }

    rolled
}

#[inline]
pub fn multiply_over_linspace(data: &mut [f32], start: f32, end: f32) {
    unsafe {
        for i in 0..data.len() {
            let progress = i as f32 / data.len() as f32;
            *data.get_unchecked_mut(i) *= lerp(start, end, progress);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    extern crate test;

    use crate::test_utils::*;

    #[test]
    fn single_complete_copy() {
        let original = vec![1, 2, 3];
        let rolled = roll_vec(&original, 0, 3);
        assert_eq!(rolled, original);
    }

    #[test]
    fn with_head() {
        let original = vec![1, 2, 3];
        let rolled = roll_vec(&original, 2, 4);
        assert_eq!(rolled, [3, 1, 2, 3]);
    }

    #[test]
    fn with_head_body_and_tail() {
        let original = vec![1, 2, 3];
        let rolled = roll_vec(&original, 1, 4);
        assert_eq!(rolled, [2, 3, 1, 2]);
    }

    #[test]
    fn with_head_body_and_tail_multiple_bodies() {
        let original = vec![1, 2, 3];
        let rolled = roll_vec(&original, 1, 9);
        assert_eq!(rolled, [2, 3, 1, 2, 3, 1, 2, 3, 1]);
    }

    #[test]
    fn with_head_body_and_tail_multiple_bodies_different_dtype() {
        let original = vec![1., 2., 3.];
        let rolled = roll_vec(&original, 1, 9);
        assert_eq!(rolled, [2., 3., 1., 2., 3., 1., 2., 3., 1.]);
    }

    #[test]
    fn test_multiply_over_linespace_constant() {
        let mut vec = vec![1., 2., 3.];
        multiply_over_linspace(vec.as_mut_slice(), 1., 1.);
        assert_almost_eq_by_element(vec, vec![1., 2., 3.]);
    }

    #[test]
    fn test_multiply_over_linespace_slope_1() {
        let mut vec = vec![1., 2., 3.];
        multiply_over_linspace(vec.as_mut_slice(), 0., 1.);
        assert_almost_eq_by_element(vec, vec![0., 0.66666666666, 2.0]);
    }

    #[bench]
    fn bench_multiply_over_linspace(b: &mut test::Bencher) {
        let vec = random_f32_vec(44100);
        b.iter(|| {
            let mut vec_copy = vec.clone();
            let slice = vec_copy.as_mut_slice();
            multiply_over_linspace(slice, 1.0, 0.0)
        });
    }

    #[bench]
    fn bench_roll_vec(b: &mut test::Bencher) {
        let vec = random_f32_vec(10);
        b.iter(|| roll_vec(&vec, 5, 44100));
    }
}
