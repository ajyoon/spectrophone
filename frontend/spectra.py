import itertools
from pytest import approx, raises
from fractions import Fraction

def harmonic_series(fundamental, num_partials):
    return [
        fundamental * o for o in range(1, num_partials + 1)
    ]


def subharmonic_series(fundamental, num_partials):
    return [
        fundamental / o for o in range(1, num_partials + 1)
    ]


def harmonic_fractal(fundamental, num_partials, depth):
    return _recursive_series(harmonic_series, fundamental, num_partials, depth)


def subharmonic_fractal(fundamental, num_partials, depth):
    return _recursive_series(subharmonic_series, fundamental, num_partials, depth)


def _recursive_series(series_fn, fundamental, num_partials, depth):
    base_series = series_fn(fundamental, num_partials)
    if depth < 2:
        return base_series
    all_frequencies = list(itertools.chain.from_iterable([
        _recursive_series(series_fn, freq, num_partials, depth - 1)
        for freq in base_series
    ]))
    all_frequencies.sort()
    return all_frequencies


def just_intonation_combinatoric(fundamental, ratio_components):
    """Make a just-intonation pitch collection with all given ratio components

    `fundamental` is multiplied by all unique ratios formed by each ordered
    pair of elements in `ratio_components`. The returned pitches are guaranteed
    to be unique and in descending order.

    Assumes that all ratio components are positive integers greater than 0.
    """
    unique_ratios = set()
    for numerator in ratio_components:
        for denominator in ratio_components:
            unique_ratios.add(Fraction(numerator, denominator))
    return sorted([
        fundamental * ratio for ratio in unique_ratios
    ])


def linspace(start, end, elements):
    delta  = end - start
    step = delta / elements
    return [
        start + i * step
        for i in range(elements)
    ]


def test_harmonic_series_no_partials():
    assert harmonic_series(220, 0) == []

def test_harmonic_series_one_partial():
    assert harmonic_series(220, 1) == approx([220])

def test_harmonic_series_many_partials():
    assert harmonic_series(220, 7) == approx([220, 440, 660, 880, 1100, 1320, 1540])

def test_subharmonic_series_no_partials():
    assert subharmonic_series(220, 0) == []

def test_subharmonic_series_one_partial():
    assert subharmonic_series(220, 1) == approx([220])

def test_subharmonic_series_many_partials():
    assert subharmonic_series(440, 3) == approx([440, 220, 146.666667])

def test_harmonic_fractal_depth_one_equivalent_to_harmonic_series():
    result_harmonic_fractal = harmonic_fractal(5, 10, 1)
    result_harmonic_series = harmonic_series(5, 10)
    assert result_harmonic_fractal == approx(result_harmonic_series)

def test_harmonic_fractal_higher_depth():
    actual = harmonic_fractal(1, 4, 2)
    expected = [1, 2, 2, 3, 3, 4, 4, 4, 6, 6, 8, 8, 9, 12, 12, 16,]
    assert actual == approx(expected)

def test_subharmonic_fractal_depth_one_equivalent_to_subharmonic_series():
    result_subharmonic_fractal = subharmonic_fractal(5, 10, 1)
    result_subharmonic_series = subharmonic_series(5, 10)
    assert result_subharmonic_fractal == approx(result_subharmonic_series)

def test_subharmonic_fractal_higher_depth():
    actual = subharmonic_fractal(880, 4, 2)
    expected = [55, 73.333336, 73.333336, 97.77778, 110, 110, 146.66667,
                146.66667, 220, 220, 220, 293.33334, 293.33334, 440, 440,
                880.0]
    assert actual == approx(expected)

def test_just_intonation_combinatoric_identity():
    assert just_intonation_combinatoric(5, [2]) == approx([5])

def test_just_intonation_combinatoric_overlapping_ratios():
    assert just_intonation_combinatoric(5, [1, 2, 4]) == approx(
        [1.25, 2.5, 5, 10, 20]
    )

def test_just_intonation_combinatoric_panics_with_div_by_zero_factor():
    with raises(ZeroDivisionError):
        just_intonation_combinatoric(5, [0, 1])
