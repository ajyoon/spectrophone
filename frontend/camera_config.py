from enum import Enum

import jsons


class WhiteBalance(Enum):

    AUTOMATIC = 0
    DAYLIGHT = 1
    FLUORESCENT = 2
    TUNGSTEN = 3
    FLASH = 4
    CLOUDY = 5
    SHADE = 6
    PRESET = 7

    def as_json(self):
        return jsons.dump({
            'value': self.value
        })
