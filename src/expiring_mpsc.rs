use std::cmp;
use std::sync::atomic;
use std::sync::atomic::AtomicIsize;
use std::sync::mpsc;
use std::sync::Arc;
use std::time::{Duration, SystemTime};

use stopwatch::Stopwatch;

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct SendError<T>(pub T);

#[derive(Clone, Debug)]
pub struct QueueDepth {
    depth: Arc<AtomicIsize>,
}

impl QueueDepth {
    fn incr(&self) {
        self.depth.fetch_add(1, atomic::Ordering::SeqCst);
    }

    fn decr(&self) {
        self.depth.fetch_sub(1, atomic::Ordering::SeqCst);
    }

    /// Get the approximate current depth of the queue.
    ///
    /// Note that this is not guaranteed to be the exact current queue depth,
    /// nor is it guaranteed to be a positive number since its management is not
    /// performed atomically with the pushing and popping of items off their
    /// queues.
    pub fn get_approx(&self) -> isize {
        self.depth.load(atomic::Ordering::SeqCst)
    }
}

struct Expiration {
    created_time: SystemTime,
    time_to_live: Duration,
}

struct Message<T> {
    payload: T,
    expiration: Option<Expiration>,
}

impl<T> Message<T> {
    fn new(payload: T) -> Message<T> {
        Message {
            payload,
            expiration: None,
        }
    }

    fn with_expiration(payload: T, time_to_live: Duration) -> Message<T> {
        Message {
            payload,
            expiration: Some(Expiration {
                time_to_live,
                created_time: SystemTime::now(),
            }),
        }
    }

    fn is_expired(&self) -> bool {
        match &self.expiration {
            Some(expiration) => {
                expiration.created_time.elapsed().unwrap_or_default() > expiration.time_to_live
            }
            None => false,
        }
    }
}

#[derive(Debug)]
pub struct Sender<T> {
    underlier: mpsc::Sender<Message<T>>,
    pub queue_depth: QueueDepth,
}

impl<T> Sender<T> {
    pub fn send(&self, t: T) -> Result<(), SendError<T>> {
        match self.underlier.send(Message::<T>::new(t)) {
            Ok(()) => {
                self.queue_depth.incr();
                Result::Ok(())
            }
            Err(e) => Result::Err(SendError(e.0.payload)),
        }
    }

    pub fn send_with_expiration(&self, t: T, time_to_live: Duration) -> Result<(), SendError<T>> {
        match self
            .underlier
            .send(Message::<T>::with_expiration(t, time_to_live))
        {
            Ok(()) => {
                self.queue_depth.incr();
                Result::Ok(())
            }
            Err(e) => Result::Err(SendError(e.0.payload)),
        }
    }

    /// Send an object down the channel with a given TTL only if the queue depth
    /// is below `max_depth`.
    ///
    /// If there was no room in the queue, the given object is returned back in
    /// an `Ok(Some(T))`.
    ///
    /// `SendError<T>` will be returned if there was room in the queue, but
    /// sending failed for another reason.
    pub fn send_if_depth_below(
        &self,
        t: T,
        time_to_live: Duration,
        max_depth: isize,
    ) -> Result<Option<T>, SendError<T>> {
        match self.queue_depth.get_approx().cmp(&max_depth) {
            cmp::Ordering::Less => match self.send_with_expiration(t, time_to_live) {
                Ok(()) => Ok(None),
                Err(e) => Err(e),
            },
            _ => Ok(Some(t)),
        }
    }
}

unsafe impl<T: Send> Send for Sender<T> {}

impl<T> !Sync for Sender<T> {}

impl<T> Clone for Sender<T> {
    fn clone(&self) -> Sender<T> {
        Sender {
            underlier: self.underlier.clone(),
            queue_depth: self.queue_depth.clone(),
        }
    }
}

pub type RecvError = mpsc::RecvError;

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub enum TryRecvError {
    Empty,
    Disconnected,
}

impl TryRecvError {
    fn from_mpsc_error(mpsc_error: mpsc::TryRecvError) -> TryRecvError {
        match mpsc_error {
            mpsc::TryRecvError::Empty => TryRecvError::Empty,
            mpsc::TryRecvError::Disconnected => TryRecvError::Disconnected,
        }
    }
}

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub enum RecvTimeoutError {
    Timeout,
    Disconnected,
}

impl RecvTimeoutError {
    fn from_mpsc_error(mpsc_error: mpsc::RecvTimeoutError) -> RecvTimeoutError {
        match mpsc_error {
            mpsc::RecvTimeoutError::Timeout => RecvTimeoutError::Timeout,
            mpsc::RecvTimeoutError::Disconnected => RecvTimeoutError::Disconnected,
        }
    }
}

#[derive(Debug)]
pub struct Receiver<T> {
    underlier: mpsc::Receiver<Message<T>>,
    pub queue_depth: QueueDepth,
}

impl<T> Receiver<T> {
    pub fn try_recv(&self) -> Result<T, TryRecvError> {
        loop {
            match self.underlier.try_recv() {
                Ok(expiring_message) => {
                    self.queue_depth.decr();
                    if !expiring_message.is_expired() {
                        return Ok(expiring_message.payload);
                    }
                }
                Err(e) => {
                    return Err(TryRecvError::from_mpsc_error(e));
                }
            }
        }
    }

    pub fn recv(&self) -> Result<T, RecvError> {
        loop {
            let expiring_message = self.underlier.recv()?;
            self.queue_depth.decr();
            if !expiring_message.is_expired() {
                return Ok(expiring_message.payload);
            }
        }
    }

    pub fn recv_timeout(&self, timeout: Duration) -> Result<T, RecvTimeoutError> {
        let sw = Stopwatch::start_new();
        loop {
            match self.underlier.recv_timeout(timeout) {
                Ok(expiring_message) => {
                    self.queue_depth.decr();
                    if !expiring_message.is_expired() {
                        return Ok(expiring_message.payload);
                    }
                }
                Err(e) => {
                    return Err(RecvTimeoutError::from_mpsc_error(e));
                }
            }
            if sw.elapsed() > timeout {
                return Err(RecvTimeoutError::Timeout);
            }
        }
    }

    pub fn iter(&self) -> Iter<T> {
        Iter { rx: self }
    }
}

#[derive(Debug)]
pub struct Iter<'a, T: 'a> {
    rx: &'a Receiver<T>,
}

#[derive(Debug)]
pub struct IntoIter<T> {
    rx: Receiver<T>,
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        self.rx.recv().ok()
    }
}

impl<T> Iterator for IntoIter<T> {
    type Item = T;
    fn next(&mut self) -> Option<T> {
        self.rx.recv().ok()
    }
}

impl<'a, T> IntoIterator for &'a Receiver<T> {
    type Item = T;
    type IntoIter = Iter<'a, T>;

    fn into_iter(self) -> Iter<'a, T> {
        self.iter()
    }
}

impl<T> IntoIterator for Receiver<T> {
    type Item = T;
    type IntoIter = IntoIter<T>;

    fn into_iter(self) -> IntoIter<T> {
        IntoIter { rx: self }
    }
}

pub fn channel<T>() -> (Sender<T>, Receiver<T>) {
    let (sender_underlier, receiver_underlier) = mpsc::channel::<Message<T>>();
    let queue_depth = QueueDepth {
        depth: Arc::new(AtomicIsize::new(0)),
    };
    let receiver_queue_depth = queue_depth.clone();
    let sender: Sender<T> = Sender {
        underlier: sender_underlier,
        queue_depth: queue_depth,
    };
    let receiver: Receiver<T> = Receiver {
        underlier: receiver_underlier,
        queue_depth: receiver_queue_depth,
    };
    (sender, receiver)
}

#[cfg(test)]
mod test_message_expiration {
    use super::*;
    use std::thread::sleep;

    const DUMMY_PAYLOAD: &str = "dummy payload";

    #[test]
    fn test_new_with_expiration_but_not_expired() {
        let message = Message::with_expiration(DUMMY_PAYLOAD, Duration::from_millis(500));
        assert!(!message.is_expired());
    }

    #[test]
    fn test_new_with_expiration_and_immediately_expired() {
        // Expiration dates are determined by comparing `SystemTime::now()` from message init time
        // to `time_in_message.elapsed()`, which is guaranteed to be nonzero.
        // This means that a duration of 0 will always be immediately expired.
        let message = Message::with_expiration(DUMMY_PAYLOAD, Duration::from_millis(0));
        assert!(message.is_expired());
    }

    #[test]
    fn test_new_with_expiration_but_expired_after_small_delay() {
        let message = Message::with_expiration(DUMMY_PAYLOAD, Duration::from_millis(5));
        sleep(Duration::from_millis(6));
        assert!(message.is_expired());
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::thread;

    #[test]
    fn test_send_no_expiration() {
        let (sender, receiver) = channel::<usize>();
        sender.send(1).unwrap();
        assert_eq!(receiver.recv().unwrap(), 1);
    }

    #[test]
    fn test_expired_messages_not_returned_by_recv() {
        let (sender, receiver) = channel::<usize>();
        sender
            .send_with_expiration(1, Duration::from_millis(1))
            .unwrap();
        sender
            .send_with_expiration(2, Duration::from_millis(1000))
            .unwrap();
        thread::sleep(Duration::from_millis(2));
        assert_eq!(receiver.recv().unwrap(), 2);
    }

    #[test]
    fn test_try_recv_fails_fast_when_all_messages_are_expired() {
        let (sender, receiver) = channel::<usize>();
        sender
            .send_with_expiration(1, Duration::from_millis(1))
            .unwrap();
        thread::sleep(Duration::from_millis(2));
        assert!(receiver.try_recv().is_err());
    }

    #[test]
    fn test_queue_depth_tracking() {
        let (sender, receiver) = channel::<usize>();
        expect_queue_depths_to_eq(&sender, &receiver, 0);
        sender.send(1).unwrap();
        expect_queue_depths_to_eq(&sender, &receiver, 1);
        receiver.recv().unwrap();
        expect_queue_depths_to_eq(&sender, &receiver, 0);
    }

    #[test]
    fn test_queue_depth_doesnt_change_when_send_fails() {
        let (sender, receiver) = channel::<usize>();
        drop(receiver);
        assert!(sender.send(1).is_err());
        assert_eq!(sender.queue_depth.get_approx(), 0);
    }

    #[test]
    fn test_queue_depth_doesnt_change_when_send_with_expiration_fails() {
        let (sender, receiver) = channel::<usize>();
        drop(receiver);
        assert!(sender
            .send_with_expiration(1, Duration::from_millis(1))
            .is_err());
        assert_eq!(sender.queue_depth.get_approx(), 0);
    }

    #[test]
    fn test_queue_depth_doesnt_change_when_recv_fails() {
        let (sender, receiver) = channel::<usize>();
        drop(sender);
        assert!(receiver.recv().is_err());
        assert_eq!(receiver.queue_depth.get_approx(), 0);
    }

    #[test]
    fn test_queue_depth_doesnt_change_when_try_recv_fails() {
        let (sender, receiver) = channel::<usize>();
        drop(sender);
        assert!(receiver.try_recv().is_err());
        assert_eq!(receiver.queue_depth.get_approx(), 0);
    }

    #[test]
    fn test_send_if_depth_below_when_ok_depth_0() {
        let (sender, _receiver) = channel::<usize>();
        let result = sender.send_if_depth_below(1, Duration::from_millis(1), 1);
        assert_eq!(result, Ok(None));
    }

    #[test]
    fn test_send_if_depth_below_when_ok_depth_1() {
        let (sender, _receiver) = channel::<usize>();
        sender.send(1).unwrap();
        let result = sender.send_if_depth_below(1, Duration::from_millis(1), 2);
        assert_eq!(result, Ok(None));
    }

    #[test]
    fn test_send_if_depth_below_when_no_room() {
        let (sender, _receiver) = channel::<usize>();
        sender.send(1).unwrap();
        let result = sender.send_if_depth_below(2, Duration::from_millis(1), 1);
        assert_eq!(result, Ok(Some(2)));
    }

    #[test]
    fn test_send_if_depth_below_when_error() {
        let (sender, receiver) = channel::<usize>();
        sender.send(1).unwrap();
        drop(receiver);
        let result = sender.send_if_depth_below(2, Duration::from_millis(1), 2);
        assert!(result.is_err());
    }

    fn expect_queue_depths_to_eq<T>(
        sender: &Sender<T>,
        receiver: &Receiver<T>,
        expected_val: isize,
    ) {
        assert_eq!(sender.queue_depth.get_approx(), expected_val);
        assert_eq!(receiver.queue_depth.get_approx(), expected_val);
    }
}
