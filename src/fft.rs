use crate::math::clamp;
use rustfft::num_complex::Complex;
use rustfft::num_traits::Zero;
use rustfft::FFTplanner;

pub struct FFTBin {
    pub freq: u32,
    /// The intensity of the frequency bin. Between 0 and 1 (inclusive).
    pub power: f32,
}

/// Calculate an FFT over a series of samples
///
/// The result is a vec of FFT result bins, each with a frequency and a power.
/// The result vec of bins will have half the length as the given number of samples.
///
/// Some rationale around this implementation at http://siciarz.net/24-days-rust-hound/
pub fn compute_fft(samples: Vec<f32>, sample_rate: u32) -> Vec<FFTBin> {
    let sample_size = samples.len();
    let bin_width = sample_rate as f32 / sample_size as f32;
    let mut fft_input: Vec<Complex<f32>> =
        samples.into_iter().map(|s| Complex::new(s, 0.0)).collect();
    let mut fft_output: Vec<Complex<f32>> = vec![Complex::zero(); sample_size];

    let mut planner = FFTplanner::new(false);
    let fft = planner.plan_fft(sample_size);
    fft.process(&mut fft_input, &mut fft_output);

    let result: Vec<FFTBin> = fft_output
        .iter()
        .take(sample_size / 2)
        .map(|c| c.norm())
        .enumerate()
        .map(|(i, intensity)| FFTBin {
            freq: (i as f32 * bin_width) as u32,
            // power: intensity,
            // I don't fully understand the math behind this, but I believe the max intensity for any
            // frequency bin is sample_size / 2.
            power: clamp(intensity / ((sample_size / 2) as f32), 0.0, 1.0),
        })
        .collect();
    result
}

#[cfg(test)]
mod test {

    use super::*;
    use crate::controller::VoiceDescription;
    use crate::synth::{Oscillator, Waveform};
    use crate::test_utils::*;

    #[test]
    fn fft_experiment() {
        let sample_rate = 44100;
        let size = 1000;

        let voice = VoiceDescription::new(Waveform::Sine, 4000.0, 1.0);
        let mut osc = Oscillator::new(voice, sample_rate);
        osc.phase = 0;
        let samples = osc.get_samples_with_interpolated_amp(size, 1.0, 1.0);

        let result = compute_fft(samples, sample_rate);
        let max_bin = result
            .iter()
            .max_by_key(|bin| (bin.power * 10000.0) as i32)
            .unwrap();
        assert_eq!(max_bin.freq, 4013);
        assert_almost_eq(max_bin.power, 0.9866773);
    }
}
