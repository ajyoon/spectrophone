from math import ceil, sqrt

from PIL import Image
import click

"""
Hacky script for taking long spectrogram output images
and wrapping them into images of different sizes.
"""

@click.command()
@click.option('--width', required=True, type=int)
@click.option('--height', required=True, type=int)
@click.argument('src', required=True, type=click.File('rb'))
@click.argument('dst', required=True, type=click.File('wb'))
def main(width, height, src, dst):
    src_img = Image.open(src)
    dst_img = Image.new('RGB', (width, height))

    aspect_ratio = width / height

    pre_scale_width, pre_scale_height = compute_min_size_at_aspect_ratio(
        src_img.width, src_img.height, aspect_ratio)

    pre_scale_img = Image.new('RGB', (pre_scale_width, pre_scale_height))

    num_rows = ceil(src_img.width / pre_scale_img.width)
    for row in range(num_rows):
        src_start_x = (pre_scale_img.width * row)
        row_width = min(pre_scale_img.width, src_img.width - src_start_x)
        row_height = src_img.height
        row_y = src_img.height * row
        copy_src = src_img.crop((src_start_x, 0,
                                 src_start_x + row_width, row_height - 1))
        pre_scale_img.paste(copy_src,
                            (0, row_y, row_width, row_y + row_height - 1))

    dst_img = pre_scale_img.resize((width, height), Image.BILINEAR)
    dst_img.save(dst)


def compute_min_size_at_aspect_ratio(src_width, src_height, aspect_ratio):
    '''Find the minimum possible size of an image at an aspect ratio into which
    a source image can be wrapped.

    This is based on a bit of math from a few observations:

    Where min_w is the minimum width of the resulting image:

        ceil(src_width / min_w) * src_height == min_h

    and since

        min_w / min_h == aspect_ratio

    rearranges to

        min_w / ((src_width / min_w) * src_height) == aspect_ratio

    we can solve for min_w as

        min_w = +sqrt(src_width) * +sqrt(src_height) * +sqrt(aspect_ratio)
    '''
    width = ceil(sqrt(src_height) * sqrt(src_width) * sqrt(aspect_ratio))
    height = ceil(width / aspect_ratio)
    return width, height


if __name__ == '__main__':
    main()
