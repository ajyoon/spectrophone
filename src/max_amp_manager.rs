use crate::transition::Transition;

pub struct MaxAmpManager {
    basis_max_amp: f32,
    active_delta_transitions: Vec<Transition>,
}

/// temporarily disabling use of this
#[allow(dead_code)]
impl MaxAmpManager {
    pub fn new(basis_max_amp: f32) -> MaxAmpManager {
        MaxAmpManager {
            basis_max_amp,
            active_delta_transitions: vec![],
        }
    }

    pub fn current_max_amp(&mut self) -> f32 {
        if self.active_delta_transitions.is_empty() {
            return self.basis_max_amp;
        }
        let mut any_transitions_complete = false;
        let mut total_max_amp = self.basis_max_amp;
        for transition in &self.active_delta_transitions {
            total_max_amp += transition.current_val();
            any_transitions_complete = any_transitions_complete || transition.is_complete();
        }
        if any_transitions_complete {
            let mut new_active_delta_transitions = vec![];
            for transition in self.active_delta_transitions.drain(..) {
                if transition.is_complete() {
                    self.basis_max_amp += transition.current_val();
                } else {
                    new_active_delta_transitions.push(transition);
                }
            }
            self.active_delta_transitions = new_active_delta_transitions;
        }
        return total_max_amp;
    }

    pub fn add_delta_transition(&mut self, delta_transition: Transition) {
        self.active_delta_transitions.push(delta_transition);
    }
}
