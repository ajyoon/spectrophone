use std::time::{Duration, SystemTime};

/// A simple heartbeat logger.
///
/// Given a heartbeat interval, this will use the `log` crate API to log a heartbeat
/// whenever `mark()` is called and the interval has been reached.
///
/// The primary purpose of this helper is to be used within long-running loops for
/// easily checking whether the loop died or is still running.
///
/// # Example
///
/// ```no_run
/// use std::time::Duration;
/// use spectrophone::heartbeat::Heartbeat;
/// let mut heartbeat = Heartbeat::new(Duration::from_secs(1), "example".to_string());
/// loop {
///     heartbeat.mark();  // every ~1 second, log "Heartbeat: example"
///
///     // do things...
/// }
/// ```
pub struct Heartbeat {
    interval: Duration,
    last_log: SystemTime,
    name: String,
}

impl Heartbeat {
    pub fn new(interval: Duration, name: String) -> Heartbeat {
        Heartbeat {
            last_log: SystemTime::now() - interval,
            interval,
            name,
        }
    }

    pub fn mark(&mut self) {
        let now = SystemTime::now();
        if now.duration_since(self.last_log).unwrap_or_default() >= self.interval {
            self.last_log = now;
            info!("Heartbeat: {}", self.name);
        }
    }
}
