import math
import re
from enum import Enum
import itertools

from pytest import raises, approx

from blur.rand import weighted_rand, weighted_choice


class UnsupportedTuningError(Exception):
    def __init__(self, given_tuning_instance, given_pitch_instance):
        super().__init__(f'Tuning {given_tuning_instance} not '
                         + f'supported by {given_pitch_instance}')


class Tuning:
    """Abstract class for all tuning types"""
    pass


class Pitch:
    def freq(self, tuning):
        """Abstract method for converting pitches to frequencies.

        Args:
            tuning (Tuning): Note that not all tunings are compatible for all
            pitch types. For instance, the various 12-tone temperaments do not
            make sense in the context of non-12-EDO pitch systems (like 13-EDO).
            If such an incompatible or otherwise unsupported tuning instance
            is given, an UnsupportedTuningError is raised.
        """
        raise NotImplementedError


class EdoTuning(Tuning):
    def __init__(self, edo_size, pitch_integer_0_freq):
        self.edo_size = edo_size
        self.pitch_integer_0_freq = pitch_integer_0_freq

    @classmethod
    def twelve_tone_with_a(cls, a_freq):
        """Create an EdoTuning for common 12-EDO relative to a given concert A.

        The general formula of the frequency of a midi number relative
        to a given A is:

            (2 ** ((midi_number - 69) / 12)) * a_freq

        Courtesy of https://newt.phys.unsw.edu.au/jw/notes.html
        """
        pitch_integer_0_freq = 2**(-69 / 12) * a_freq
        return cls(12, pitch_integer_0_freq)

    def freq_at_pitch_integer(self, pitch_integer):
        return (
            2 ** (pitch_integer / self.edo_size)
        ) * self.pitch_integer_0_freq

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return f'EdoTuning {{ edo_size: {self.edo_size}, pitch_integer_0_freq: {self.pitch_integer_0_freq} }}'


class EdoPitch(Pitch):

    def __init__(self, octave, pitch_integer_in_octave):
        self.octave = octave
        self.pitch_integer_in_octave = pitch_integer_in_octave

    def as_absolute_integer_pitch(self, tuning):
        return AbsoluteIntegerPitch(
            (tuning.edo_size * self.octave) + self.pitch_integer_in_octave)

    def freq(self, tuning):
        if isinstance(tuning, EdoTuning):
            return AbsoluteIntegerPitch(
                self.as_absolute_integer_pitch(tuning).pitch_integer
            ).freq(tuning)
        else:
            raise UnsupportedTuningError(tuning, self)

    def copy(self, octave=None, pitch_integer_in_octave=None):
        return EdoPitch(
            octave if octave is not None
            else self.octave,
            pitch_integer_in_octave if pitch_integer_in_octave is not None
            else self.pitch_integer_in_octave
        )

    def __repr__(self):
        return f'EdoPitch({self.octave}, {self.pitch_integer_in_octave})'

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return (
                self.octave == other.octave and
                self.pitch_integer_in_octave == other.pitch_integer_in_octave)
        return False


class AbsoluteIntegerPitch(Pitch):
    """A pitch represented by a natural number where 0 is the lowest possible pitch."""

    def __init__(self, pitch_integer):
        self.pitch_integer = pitch_integer

    def freq(self, tuning):
        if isinstance(tuning, EdoTuning):
            return tuning.freq_at_pitch_integer(self.pitch_integer)
        else:
            raise UnsupportedTuningError(tuning, self)

    def copy(self, pitch_integer=None):
        return AbsoluteIntegerPitch(
            pitch_integer if pitch_integer is not None else self.pitch_integer)

    def __add__(self, delta):
        if isinstance(delta, int):
            return self.copy(pitch_integer=self.pitch_integer + delta)
        elif isinstance(delta, AbsoluteIntegerPitch):
            return self.copy(pitch_integer=self.pitch_integer + delta.pitch_integer)
        else:
            raise TypeError()

    def __sub__(self, delta):
        if isinstance(delta, int):
            return self.copy(pitch_integer=self.pitch_integer - delta)
        elif isinstance(delta, AbsoluteIntegerPitch):
            return self.copy(pitch_integer=self.pitch_integer - delta.pitch_integer)
        else:
            raise TypeError()


class Accidental(Enum):
    DoubleFlat = -2
    Flat = -1
    Natural = 0
    Sharp = 1
    DoubleSharp = 2

    @staticmethod
    def from_str(string):
        return _ACCIDENTAL_FROM_STR_MAP.get(string)

    def as_str(self):
        return _ACCIDENTAL_TO_STR_MAP[self]


_ACCIDENTAL_FROM_STR_MAP = {
    'ff': Accidental.DoubleFlat,
    'bb': Accidental.DoubleFlat,
    'f': Accidental.Flat,
    'b': Accidental.Flat,
    'n': Accidental.Natural,
    None: Accidental.Natural,
    's': Accidental.Sharp,
    '#': Accidental.Sharp,
    'ss': Accidental.DoubleSharp,
    '##': Accidental.DoubleSharp,
    'x': Accidental.DoubleSharp,
}

_ACCIDENTAL_TO_STR_MAP = {
    Accidental.DoubleFlat: 'bb',
    Accidental.Flat: 'b',
    Accidental.Natural: '',
    Accidental.Sharp: '#',
    Accidental.DoubleSharp: 'x'
}


class PitchLetter(Enum):
    C = 0
    D = 2
    E = 4
    F = 5
    G = 7
    A = 9
    B = 11


class WesternClassicalPitch(Pitch):

    _lilypond_re = re.compile("^(?P<letter>[a-gA-G])(?P<accidental>ff|bb|f|b|n|s|#|ss|##|x)?(?P<octave>,*|'*)?$")

    def __init__(self, letter, accidental, octave):
        self.letter = letter
        self.accidental = accidental
        self.octave = octave

    @classmethod
    def from_lilypond_str(cls, lilypond_str):
        match = cls._lilypond_re.match(lilypond_str)
        if match is None:
            raise ValueError
        letter = PitchLetter[match.group('letter').upper()]
        accidental = Accidental.from_str(match.group('accidental'))
        octave_ticks = match.group('octave')
        octave = 4
        if octave_ticks:
            octave += (len(octave_ticks) * (-1 if octave_ticks[0] == ',' else 1))
        return WesternClassicalPitch(letter, accidental, octave)

    @property
    def midi_number(self):
        return self.letter.value + self.octave * 12 + self.accidental.value

    @property
    def lilypond_str(self):
        letter = self.letter.name.lower()
        accidental = self.accidental.as_str()
        octave = ''
        if self.octave > 4:
            octave += "'" * (self.octave - 3)
        elif self.octave < 4:
            octave += "," * (4 + (self.octave * -1))
        return letter + accidental + octave

    def __repr__(self):
        return f'WesternClassicalPitch <{self.lilypond_str}>'

    def freq(self, tuning):
        if isinstance(tuning, EdoTuning) and tuning.edo_size == 12:
            return tuning.freq_at_pitch_integer(self.midi_number)
        else:
            raise UnsupportedTuningError(tuning, self)

    def copy(self, letter, accidental, octave):
        return WesternClassicalPitch(
            letter if letter else self.letter,
            accidental if accidental else self.accidental,
            octave if octave else self.octave,
        )

    def as_edo_pitch(self):
        return EdoPitch(self.octave, self.letter.value + self.accidental.value)

    def as_absolute_integer_pitch(self):
        # Base frequency of tuning doesn't matter - we just need any 12EDO
        tuning = EdoTuning.twelve_tone_with_a(440)
        return self.as_edo_pitch().as_absolute_integer_pitch(tuning)

    def __eq__(self, other):
        if isinstance(other, type(self)):
            return (self.letter == other.letter and
                    self.accidental == other.accidental and
                    self.octave == other.octave)
        return False


def lilypitch(lilypond_str):
    """Shorthand for `WesternClassicalPitch.from_lilypond_str()`"""
    return WesternClassicalPitch.from_lilypond_str(lilypond_str)

def lilyfreq(lilypond_str, tuning=None):
    return lilypitch(lilypond_str).freq(
                     tuning if tuning else EdoTuning.twelve_tone_with_a(440))

def peak_around(lilypond_str, baseline_prob, peak_prob, tolerance=1, tuning=None):
    center_pitch = (WesternClassicalPitch.from_lilypond_str(lilypond_str)
                    .as_absolute_integer_pitch())
    tuning = tuning if tuning else EdoTuning.twelve_tone_with_a(440)
    # compute frequency bounds
    #
    # this function supports non-integral tolerances (given as semitones)
    # so to approximate that for each bound we linearly interpolate between
    # the nearest integral semitones
    nearest_lower_freq = (center_pitch - math.floor(tolerance)).freq(tuning)
    nearest_upper_freq = (center_pitch - math.ceil(tolerance)).freq(tuning)
    lower_bound_freq = nearest_lower_freq + (
        (nearest_upper_freq - nearest_lower_freq) * (tolerance % 1))

    nearest_lower_freq = (center_pitch + math.floor(tolerance)).freq(tuning)
    nearest_upper_freq = (center_pitch + math.ceil(tolerance)).freq(tuning)
    upper_bound_freq = nearest_lower_freq + (
        (nearest_upper_freq - nearest_lower_freq) * (tolerance % 1))

    return [
        (lower_bound_freq, baseline_prob),
        (center_pitch.freq(tuning), peak_prob),
        (upper_bound_freq, baseline_prob),
    ]


def as_scale(pitches, start_octave, num_pitches):
    scale = []
    if not pitches:
        return scale
    octave_offset = start_octave - min(pitches, key=lambda p: p.octave).octave
    while True:
        for pitch in pitches:
            scale.append(WesternClassicalPitch(
                pitch.letter, pitch.accidental, pitch.octave + octave_offset))
            if len(scale) >= num_pitches:
                return scale
        octave_offset += 1


def as_edo_scale(edo_pitches, start_octave, num_pitches):
    scale = []
    if not edo_pitches:
        return scale
    octave_offset = start_octave - min(edo_pitches, key=lambda p: p.octave).octave
    while True:
        for edo_pitch in edo_pitches:
            scale.append(
                EdoPitch(edo_pitch.octave + octave_offset,
                         edo_pitch.pitch_integer_in_octave))
            if len(scale) >= num_pitches:
                return scale
        octave_offset += 1


def detune_freqs(freqs, len_multiplier=10, detune_factor=0.2, lower_bias_offset=5):
    detune_weights = [(w[0] * detune_factor, w[1]) for w in [
        (-20, 0), (-5, 1), (-1, 3), (0, 15), (1, 3), (5, 1), (20, 0)]]
    # base_freq_weights = [
    #     (f, ((i + lower_bias_offset) / len(freqs)))
    #     for (i, f) in enumerate(reversed(freqs))
    # ]

    base_freq_weights = [
        (f, 1)
        for f in freqs
    ]
    return [
        weighted_choice(base_freq_weights) + weighted_rand(detune_weights)
        for _ in range(len_multiplier * len(freqs))
    ]


def add_multiplier_to_each_freq(freqs, multiplier, above=True):
    """Add a freq with a given multiple before every freq in a list

    If `above` is `True`, the multiplied frequency comes after each
    original one. Otherwise, it goes before.
    """
    out = []
    for freq in freqs:
        if not above:
            out.append(multiplier * freq)
        out.append(freq)
        if above:
            out.append(multiplier * freq)
    return out


def test_incompatible_tuning_error_message():
    class SomeTuning:
        def __str__(self):
            return 'foo'

    class SomePitch(Pitch):
        def __str__(self):
            return 'bar'

        def raise_error(self):
            raise UnsupportedTuningError(SomeTuning(), self)

    with raises(UnsupportedTuningError,
                match="Tuning foo not supported by bar"):
        SomePitch().raise_error()

def test_edo_tuning_freq_at_pitch_integer_0():
    assert EdoTuning(10, 100).freq_at_pitch_integer(0) == 100

def test_edo_tuning_freq_at_pitch_integer_one_octave_above_lowest():
    assert EdoTuning(123, 5.67).freq_at_pitch_integer(123) == approx(5.67 * 2)

def test_edo_tuning_freq_at_pitch_integer_two_octaves_above_lowest():
    assert EdoTuning(123, 5.67).freq_at_pitch_integer(123 * 2) == approx(5.67 * 4)

def test_edo_tuning_freq_at_pitch_integer_arbitrary_pitch():
    assert EdoTuning(15, 5.67).freq_at_pitch_integer(42) == approx(39.48817)

def test_from_lilypond_str_invalid_pitch():
    with raises(ValueError):
        assert WesternClassicalPitch.from_lilypond_str("ts,") == None

def test_from_lilypond_str_invalid_accidental():
    with raises(ValueError):
        assert WesternClassicalPitch.from_lilypond_str("gt,") == None

def test_from_lilypond_str_invalid_octave_not_comma_or_quote():
    with raises(ValueError):
        assert WesternClassicalPitch.from_lilypond_str("ast") == None

def test_from_lilypond_str_invalid_octave_mixed_commas_and_quotes():
    with raises(ValueError):
        assert WesternClassicalPitch.from_lilypond_str("as,'") == None

def assert_from_lilypond_str(lilypond_str, expected_letter,
                             expected_accidental, expected_octave):
    pitch = WesternClassicalPitch.from_lilypond_str(lilypond_str)
    assert pitch.letter == expected_letter
    assert pitch.accidental == expected_accidental
    assert pitch.octave == expected_octave

def assert_midi_number(letter, accidental, octave, expected_midi_number):
    pitch = WesternClassicalPitch(letter, accidental, octave)
    assert pitch.midi_number == expected_midi_number

def test_from_lilypond_str_valid_no_accidental():
    assert_from_lilypond_str("a,", PitchLetter.A, Accidental.Natural, 3)

def test_from_lilypond_str_valid_no_octave():
    assert_from_lilypond_str("c", PitchLetter.C, Accidental.Natural, 4)

def test_from_lilypond_str_valid_with_accidental():
    assert_from_lilypond_str("as,", PitchLetter.A, Accidental.Sharp, 3)

def test_from_lilypond_str_valid_multiple_lower_octaves():
    assert_from_lilypond_str("c,,", PitchLetter.C, Accidental.Natural, 2)

def test_from_lilypond_str_valid_multiple_upper_octaves():
    assert_from_lilypond_str("c''", PitchLetter.C, Accidental.Natural, 6)

def test_midi_number_middle_c():
    assert_midi_number(PitchLetter.C, Accidental.Natural, 5, 60)

def test_midi_number_middle_c_flat():
    assert_midi_number(PitchLetter.C, Accidental.Flat, 5, 59)

def test_midi_number_middle_c_sharp():
    assert_midi_number(PitchLetter.C, Accidental.Sharp, 5, 61)

def test_midi_number_middle_c_double_flat():
    assert_midi_number(PitchLetter.C, Accidental.DoubleFlat, 5, 58)

def test_midi_number_middle_c_double_sharp():
    assert_midi_number(PitchLetter.C, Accidental.DoubleSharp, 5, 62)

def test_midi_number_b_sharp_enharmonic_with_middle_c():
    assert_midi_number(PitchLetter.B, Accidental.Sharp, 4, 60)

def test_edo_tuning_a_440_identity():
    tuning = EdoTuning.twelve_tone_with_a(440)
    pitch = WesternClassicalPitch.from_lilypond_str("a'")
    # Sanity check
    assert pitch.midi_number == 69
    # Test identity
    assert pitch.freq(tuning) == approx(440)

def test_peak_around_integral_tolerance_half_step():
    default_tuning = EdoTuning.twelve_tone_with_a(440)
    probs = peak_around("c", 1, 10, 1)
    assert probs == [
        (lilypitch("b,").freq(default_tuning), 1),
        (lilypitch("c").freq(default_tuning), 10),
        (lilypitch("db").freq(default_tuning), 1),
    ]

def test_peak_around_integral_tolerance_whole_step():
    default_tuning = EdoTuning.twelve_tone_with_a(440)
    probs = peak_around("c", 1, 10, 2)
    assert probs == [
        (lilypitch("bb,").freq(default_tuning), 1),
        (lilypitch("c").freq(default_tuning), 10),
        (lilypitch("d").freq(default_tuning), 1),
    ]

def test_peak_around_integral_tolerance_fractional():
    default_tuning = EdoTuning.twelve_tone_with_a(440)
    probs = peak_around("c", 2, 20, 0.5)
    assert probs == [
        (127.14180398216516, 2),
        (lilypitch("c").freq(default_tuning), 20),
        (134.70204906936766, 2),
    ]

def test_as_scale_empty():
        assert as_scale([], 0, 1) == []

def test_as_scale_single_full_roll_at_same_starting_octave():
    actual = as_scale([
            WesternClassicalPitch(PitchLetter.A, Accidental.Natural, 5),
            WesternClassicalPitch(PitchLetter.B, Accidental.Natural, 5),
        ], 5, 2)
    expected = [
        WesternClassicalPitch(PitchLetter.A, Accidental.Natural, 5),
        WesternClassicalPitch(PitchLetter.B, Accidental.Natural, 5),
    ]
    assert expected == actual

def test_as_scale_single_full_roll_at_different_starting_octave():
    actual = as_scale([
        WesternClassicalPitch(PitchLetter.A, Accidental.Natural, 5),
        WesternClassicalPitch(PitchLetter.B, Accidental.Natural, 5),
    ], 3, 2)
    expected = [
        WesternClassicalPitch(PitchLetter.A, Accidental.Natural, 3),
        WesternClassicalPitch(PitchLetter.B, Accidental.Natural, 3),
    ]
    assert expected == actual

def test_as_scale_lowest_octave_used_as_offset_reference_regardless_of_order():
    actual = as_scale([
        WesternClassicalPitch(PitchLetter.A, Accidental.Natural, 5),
        WesternClassicalPitch(PitchLetter.B, Accidental.Natural, 2),
    ], 3, 2)
    expected = [
        WesternClassicalPitch(PitchLetter.A, Accidental.Natural, 6),
        WesternClassicalPitch(PitchLetter.B, Accidental.Natural, 3),
    ]
    assert expected == actual

def test_as_scale_multiple_full_roll_at_different_starting_octave():
    actual = as_scale([
        WesternClassicalPitch(PitchLetter.A, Accidental.Natural, 5),
        WesternClassicalPitch(PitchLetter.B, Accidental.Natural, 5),
    ], 3, 4)
    expected = [
        WesternClassicalPitch(PitchLetter.A, Accidental.Natural, 3),
        WesternClassicalPitch(PitchLetter.B, Accidental.Natural, 3),
        WesternClassicalPitch(PitchLetter.A, Accidental.Natural, 4),
        WesternClassicalPitch(PitchLetter.B, Accidental.Natural, 4),
    ]
    assert expected == actual

def test_as_scale_single_partial_roll():
    actual = as_scale([
        WesternClassicalPitch(PitchLetter.A, Accidental.Natural, 5),
        WesternClassicalPitch(PitchLetter.B, Accidental.Natural, 5),
    ], 3, 1)
    expected = [
        WesternClassicalPitch(PitchLetter.A, Accidental.Natural, 3),
    ]
    assert expected == actual

def test_as_scale_multiple_full_rolls_and_partial_roll():
    actual = as_scale([
        WesternClassicalPitch(PitchLetter.A, Accidental.Natural, 5),
        WesternClassicalPitch(PitchLetter.B, Accidental.Natural, 5),
    ], 3, 5)
    expected = [
        WesternClassicalPitch(PitchLetter.A, Accidental.Natural, 3),
        WesternClassicalPitch(PitchLetter.B, Accidental.Natural, 3),
        WesternClassicalPitch(PitchLetter.A, Accidental.Natural, 4),
        WesternClassicalPitch(PitchLetter.B, Accidental.Natural, 4),
        WesternClassicalPitch(PitchLetter.A, Accidental.Natural, 5),
    ]
    assert expected == actual


def test_edo_scale_single_full_roll_at_same_starting_octave():
    actual = as_edo_scale([
        EdoPitch(5, 0),
        EdoPitch(5, 3),
        ], 5, 2)
    expected = [
        EdoPitch(5, 0),
        EdoPitch(5, 3),
    ]
    assert expected == actual


def test_edo_scale_single_full_roll_at_different_starting_octave():
    actual = as_edo_scale([
        EdoPitch(5, 0),
        EdoPitch(5, 3),
    ], 3, 2)
    expected = [
        EdoPitch(3, 0),
        EdoPitch(3, 3),
    ]
    assert expected == actual


def test_edo_scale_lowest_octave_used_as_offset_reference_regardless_of_order():
    actual = as_edo_scale([
        EdoPitch(5, 0),
        EdoPitch(2, 3),
    ], 3, 2)
    expected = [
        EdoPitch(6, 0),
        EdoPitch(3, 3),
    ]
    assert expected == actual


def test_edo_scale_multiple_full_roll_at_different_starting_octave():
    actual = as_edo_scale([
        EdoPitch(5, 0),
        EdoPitch(5, 3),
    ], 3, 4)
    expected = [
        EdoPitch(3, 0),
        EdoPitch(3, 3),
        EdoPitch(4, 0),
        EdoPitch(4, 3),
    ]
    assert expected == actual


def test_edo_scale_single_partial_roll():
    actual = as_edo_scale([
        EdoPitch(5, 0),
        EdoPitch(5, 3),
    ], 3, 1)
    expected = [
        EdoPitch(3, 0),
    ]
    assert expected == actual


def test_edo_scale_multiple_full_rolls_and_partial_roll():
    actual = as_edo_scale([
        EdoPitch(5, 0),
        EdoPitch(5, 3),
    ], 3, 5)
    expected = [
        EdoPitch(3, 0),
        EdoPitch(3, 3),
        EdoPitch(4, 0),
        EdoPitch(4, 3),
        EdoPitch(5, 0),
    ]
    assert expected == actual


def test_add_multiplier_to_each_freq_above():
    freqs = [1, 5, 11]
    result = add_multiplier_to_each_freq(freqs, 1.5, above=True)
    expected = [1, 1.5, 5, 5 * 1.5, 11, 11 * 1.5]


def test_add_multiplier_to_each_freq_below():
    freqs = [1, 5, 11]
    result = add_multiplier_to_each_freq(freqs, 1.5, above=False)
    expected = [1.5, 1, 5 * 1.5, 5, 11 * 1.5, 11]
