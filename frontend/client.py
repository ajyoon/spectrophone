import copy
import math
import time
import typing
import random

import requests
import jsons

from layer import Layer, Transition, Interpolation, Waveform, now
from camera_config import WhiteBalance
from api_requests import PutLayerRequest, DeleteLayerRequest


ROOT = 'http://localhost:8000/'
CAMERA_CONFIG_ROOT = ROOT + 'camera/config/'

# { client_side_id: (server_side_id, Layer) }
layer_states = {}


def get_status():
    raw_status = requests.get(ROOT + 'status').content
    return jsons.loads(raw_status, typing.Dict[int, Layer])


def print_stats():
    layers = get_status().values()
    for layer in layers:
        voice_count = len(layer.voices)
        sine_ratio = (
            len([v for v in layer.voices if v.waveform == Waveform.Sine])
            / voice_count)
        square_ratio = (
            len([v for v in layer.voices if v.waveform == Waveform.Square])
            / voice_count)
        triangle_ratio = (
            len([v for v in layer.voices if v.waveform == Waveform.Triangle])
            / voice_count)
        max_amp = math.ceil(sum((v.amplitude_factor for v in layer.voices)))
        print(f"Layer: {layer.name}")
        print(f"Voice count: {voice_count}")
        print(f"Max amp: {max_amp}")
        print("Waveform Mix (sine, square, tri): "
              f"{sine_ratio:.2f}, {square_ratio:.2f}, {triangle_ratio:.2f}")
        print()

    total_voice_count = sum(len(layer.voices) for layer in layers)
    total_max_amp = math.ceil(sum(
        sum((v.amplitude_factor for v in layer.voices))
        for layer in layers
    ))
    total_samples_per_sec = total_voice_count * 44_100

    print(f"Total Layer Count: {len(layers)}")
    print(f"Total Voice Count: {total_voice_count}")
    print(f"Total Max Amp: {total_max_amp}")
    print(f"Total Samples / Sec: {total_samples_per_sec:,d}")


def approx_max_amplitude():
    return sum(
        (sum(v.amplitude_factor for v in layer.voices))
        for layer in get_status().values()
    )


def count_voices():
    return sum(len(layer.voices) for layer in get_status().values())


def shutdown():
    return requests.post(ROOT + 'shutdown')


def put_layer(layer_id, layer, transition_specifier=None):
    if layer_id in layer_states:
        delete_layer(layer_id, transition_specifier)
    server_side_id = _random_id()
    send_layer = layer
    layer_states[layer_id] = (server_side_id, copy.deepcopy(send_layer))
    transition = _resolve_transition(transition_specifier)
    request = PutLayerRequest(send_layer, transition)
    return requests.put(ROOT + f'layer/{server_side_id}',
                        json=request.as_json())


def delete_layer(layer_id, transition_specifier=None):
    server_side_id = layer_states.pop(layer_id)[0]
    transition = _resolve_transition(transition_specifier)
    request = DeleteLayerRequest(transition)
    return requests.delete(ROOT + f'layer/{server_side_id}',
                           json=request.as_json())


def set_white_balance(white_balance):
    return requests.put(CAMERA_CONFIG_ROOT + 'whitebalance',
                        json=white_balance.as_json())


def reset():
    layer_states = {}


def restore():
    for layer_id, (_, layer) in layer_states.items():
        put_layer(layer_id, layer)
        time.sleep(1)


def _random_id():
    return random.randint(0, 4294967295)


_DEFAULT_TRANSITION_OFFSET = 4


def _resolve_transition(specifier):
    if specifier is None:
        return Transition(now() + _DEFAULT_TRANSITION_OFFSET, 1000,
                          Interpolation.SquareRoot)
    elif isinstance(specifier, int):
        return Transition(now() + _DEFAULT_TRANSITION_OFFSET, specifier,
                          Interpolation.SquareRoot)
    elif isinstance(specifier, tuple) and len(specifier) == 2:
        return Transition(specifier[0], specifier[1], Interpolation.SquareRoot)
    elif isinstance(specifier, Transition):
        return specifier
    else:
        raise ValueError("Invalid transition specifier")

