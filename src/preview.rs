use std::process::Command;

use opencv::core::Mat;
use opencv::imgcodecs;
use opencv::prelude::*;
use opencv::types;
use tempfile;

use crate::color_key::{color_similarity_filter, HsvKey};
use crate::opencv_helpers::convert_u8_img_to_f32;

pub fn preview_image(img_path: &str, key: HsvKey) {
    let distance_preview = generate_similarity_preview(img_path, key);
    let temp_preview_file = tempfile::Builder::new().suffix(".png").tempfile().unwrap();
    let temp_path = temp_preview_file.into_temp_path();
    let temp_path_str = temp_path.to_str().unwrap();
    imgcodecs::imwrite(temp_path_str, &distance_preview, &types::VectorOfint::new()).unwrap();
    Command::new("eog")
        .args(&[temp_path_str])
        .output()
        .expect("Failed to preview image");
}

fn generate_similarity_preview(img_path: &str, key: HsvKey) -> Mat {
    let mut src_u8 = imgcodecs::imread(img_path, imgcodecs::IMREAD_UNCHANGED).unwrap();
    let mut src_f32 = convert_u8_img_to_f32(&mut src_u8, 1.0, 0.0);
    color_similarity_filter(&mut src_f32, key)
}
