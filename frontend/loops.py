import threading


class Loop(threading.Thread):
    """Thread class with a stop() method. The thread itself has to check
    regularly for the stopped() condition.

    via https://stackoverflow.com/a/325528/5615927
    """

    def __init__(self, target):
        super(Loop, self).__init__(target=target)
        self._stop_event = threading.Event()

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()
