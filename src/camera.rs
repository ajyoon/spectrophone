use std::convert::From;
use std::fmt;
use std::io::BufReader;
use std::io::Read;
use std::process::{ChildStdout, Command, Stdio};

use captrs::Capturer;
use opencv;
use opencv::core;
use opencv::core::Mat;
use opencv::imgcodecs;
use opencv::imgproc;
use opencv::videoio::VideoCapture;

use crate::opencv_helpers;
use crate::opencv_helpers::convert_u8_img_to_f32;

pub const DEFAULT_CAMERA_DEVICE_ID: i32 = 0;
pub const DEFAULT_SCREEN_CAPTURE_SRC: usize = 0;

#[derive(Debug)]
pub struct CaptureError {
    pub message: String,
}

impl CaptureError {
    pub fn from_str(err_str: &str) -> CaptureError {
        CaptureError {
            message: err_str.to_string(),
        }
    }

    pub fn from_string(err_string: String) -> CaptureError {
        CaptureError {
            message: err_string,
        }
    }
}

impl From<gphoto::Error> for CaptureError {
    fn from(error: gphoto::Error) -> CaptureError {
        CaptureError {
            message: error.message().to_string(),
        }
    }
}

impl From<String> for CaptureError {
    fn from(error_string: String) -> CaptureError {
        CaptureError {
            message: error_string,
        }
    }
}

impl From<opencv::Error> for CaptureError {
    fn from(error: opencv::Error) -> CaptureError {
        CaptureError {
            message: error.message,
        }
    }
}

impl From<captrs::CaptureError> for CaptureError {
    fn from(error: captrs::CaptureError) -> CaptureError {
        CaptureError {
            message: format!("{:?}", error),
        }
    }
}

impl fmt::Display for CaptureError {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        fmt.write_str(&self.message)
    }
}

#[derive(Debug)]
pub struct CameraConfigError {
    pub message: String,
}

impl CameraConfigError {
    pub fn from_str(err_str: &str) -> CameraConfigError {
        CameraConfigError {
            message: err_str.to_string(),
        }
    }

    pub fn from_string(err_string: String) -> CameraConfigError {
        CameraConfigError {
            message: err_string,
        }
    }
}

impl From<gphoto::Error> for CameraConfigError {
    fn from(error: gphoto::Error) -> CameraConfigError {
        CameraConfigError {
            message: error.message().to_string(),
        }
    }
}

impl fmt::Display for CameraConfigError {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        fmt.write_str(&self.message)
    }
}

pub trait Camera {
    fn capture_frame(&mut self) -> Result<Mat, CaptureError>;
    fn get_config_numeric(&mut self, name: &str) -> Result<i32, CameraConfigError>;
    fn set_config_numeric(&mut self, name: &str, value: i32) -> Result<(), CameraConfigError>;
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum OpencvVideoSource {
    DeviceId(Option<i32>),
    Filename(String),
}

impl fmt::Display for OpencvVideoSource {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

/// Originally built trying to work around an opencv issue,
/// but no longer needed
pub struct FfmpegVideo {
    pub source: String,
    width: usize,
    height: usize,
    bytes_per_pixel: usize,
    stdout_reader: BufReader<ChildStdout>,
}

impl FfmpegVideo {
    pub fn new(source: &str, width: usize, height: usize) -> FfmpegVideo {
        let child_process = Command::new("ffmpeg")
            .arg("-i")
            .arg(source)
            .arg("-pix_fmt")
            .arg("bgr24")
            .arg("-vcodec")
            .arg("rawvideo")
            .arg("-an")
            .arg("-sn")
            .arg("-f")
            .arg("image2pipe")
            .arg("-")
            .stdout(Stdio::piped())
            .spawn()
            .expect("Failed to launch ffmpeg child process");
        let child_stdout = child_process
            .stdout
            .expect("Failed to open ffmpeg stdout pipe");
        let stdout_reader = BufReader::new(child_stdout);
        FfmpegVideo {
            source: source.to_owned(),
            width,
            height,
            bytes_per_pixel: 3,
            stdout_reader,
        }
    }

    fn bytes_per_frame(&self) -> usize {
        self.width * self.height * self.bytes_per_pixel
    }
}

impl Camera for FfmpegVideo {
    fn capture_frame(&mut self) -> Result<Mat, CaptureError> {
        let mut buffer = vec![0u8; self.bytes_per_frame()];
        match self.stdout_reader.read_exact(&mut buffer) {
            Ok(_) => {}
            Err(e) => {
                error!("failed to capture frame. {:?}", e);
            }
        };
        let mut mat = opencv_helpers::vec_bgr_buffer_to_mat(buffer, self.width, self.height);
        Ok(convert_u8_img_to_f32(&mut mat, 1.1, -0.15))
    }

    fn get_config_numeric(&mut self, _name: &str) -> Result<i32, CameraConfigError> {
        Err(CameraConfigError::from_str(
            "Cannot get numeric config for webcams",
        ))
    }

    fn set_config_numeric(&mut self, _name: &str, _value: i32) -> Result<(), CameraConfigError> {
        Err(CameraConfigError::from_str(
            "Cannot set numeric config for webcams",
        ))
    }
}

/// A connection to a hardware camera device capable of capturing images as opencv matrices.
///
/// Note that for the lifetime of this struct, a connection to the device is
/// held which blocks simultaneous connections to the same device. This means
/// that calling `OpencvVideo::new(n)` twice in a row will fail on the second call,
/// returning `None`.
///
/// Because this camera is a wrapper around an opencv `VideoCapture`, which
/// cannot be safely sent between threads, this struct too is not `Send`.
pub struct OpencvVideo {
    pub video_source: OpencvVideoSource,
    underlier: VideoCapture,
}

impl OpencvVideo {
    pub fn from_video_source(video_source: OpencvVideoSource) -> Option<OpencvVideo> {
        let mut underlier = match &video_source {
            OpencvVideoSource::DeviceId(maybe_device_id) => {
                let resolved_device_id = (*maybe_device_id).unwrap_or(DEFAULT_CAMERA_DEVICE_ID);
                Self::connect_with_opencv_call(|| VideoCapture::new(resolved_device_id))
            }
            OpencvVideoSource::Filename(filename) => {
                Self::connect_with_opencv_call(|| VideoCapture::new_from_file(&filename))
            }
        }?;
        opencv_helpers::log_video_capture_debug_info(&mut underlier);
        Some(OpencvVideo {
            video_source,
            underlier,
        })
    }

    fn connect_with_opencv_call<F>(closure: F) -> Option<VideoCapture>
    where
        F: Fn() -> Result<VideoCapture, opencv::Error>,
    {
        match closure() {
            Ok(video_capture) => match VideoCapture::is_opened(&video_capture) {
                Result::Ok(_) => Some(video_capture),
                Result::Err(_) => None,
            },
            Err(_) => None,
        }
    }
}

impl Drop for OpencvVideo {
    fn drop(&mut self) {
        self.underlier.release().unwrap();
    }
}

impl fmt::Debug for OpencvVideo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "OpencvVideo {{ video_source: {} }}", self.video_source,)
    }
}

impl Camera for OpencvVideo {
    fn capture_frame(&mut self) -> Result<Mat, CaptureError> {
        let mut frame = core::Mat::default().unwrap();
        self.underlier.read(&mut frame)?;
        let size = frame.size().unwrap();
        if size.width == 0 || size.height == 0 {
            Err(CaptureError::from_string(format!(
                "Captured frame with invalid size: {:?}",
                size
            )))
        } else {
            Ok(convert_u8_img_to_f32(&mut frame, 1.1, -0.15))
        }
    }

    fn get_config_numeric(&mut self, _name: &str) -> Result<i32, CameraConfigError> {
        Err(CameraConfigError::from_str(
            "Cannot get numeric config for webcams",
        ))
    }

    fn set_config_numeric(&mut self, _name: &str, _value: i32) -> Result<(), CameraConfigError> {
        Err(CameraConfigError::from_str(
            "Cannot set numeric config for webcams",
        ))
    }
}

pub struct GphotoCamera {
    context: gphoto::Context,
    underlier: gphoto::Camera,
}

impl GphotoCamera {
    pub fn new() -> Result<GphotoCamera, gphoto::Error> {
        let mut context = gphoto::Context::new().unwrap();
        let underlier = gphoto::Camera::autodetect(&mut context)?;
        Ok(GphotoCamera { context, underlier })
    }
}

impl Camera for GphotoCamera {
    fn capture_frame(&mut self) -> Result<Mat, CaptureError> {
        let capture_virtual_file = self.underlier.capture_image_preview(&mut self.context)?;
        let mut frame = imgcodecs::imread(
            capture_virtual_file.path().to_str().unwrap(),
            imgcodecs::IMREAD_UNCHANGED,
        )
        .unwrap();
        let size = frame.size().unwrap();
        if size.width == 0 || size.height == 0 {
            Err(CaptureError::from_string(format!(
                "Captured frame with invalid size: {:?}",
                size
            )))
        } else {
            Ok(convert_u8_img_to_f32(&mut frame, 1.0, 0.0))
        }
    }

    fn get_config_numeric(&mut self, name: &str) -> Result<i32, CameraConfigError> {
        Ok(self.underlier.get_config_numeric(&mut self.context, name)?)
    }

    fn set_config_numeric(&mut self, name: &str, value: i32) -> Result<(), CameraConfigError> {
        Ok(self
            .underlier
            .set_config_numeric(&mut self.context, name, value)?)
    }
}

/// A (theoretically) cross-platform screen capture input.
///
/// Given a display number at creation time, the camera will provide frames from it.
/// Note this is relatively expensive since it involves a couple steps of copying
/// data around, but is still much faster (10-20ms per frame) than hardware cameras
pub struct ScreenCamera {
    capturer: Capturer,
    scale_to: Option<(usize, usize)>,
}

impl ScreenCamera {
    pub fn from_settings(
        capture_src: Option<usize>,
        scale_to: Option<(usize, usize)>,
    ) -> Result<ScreenCamera, String> {
        Ok(ScreenCamera {
            capturer: Capturer::new(capture_src.unwrap_or(DEFAULT_SCREEN_CAPTURE_SRC))?,
            scale_to,
        })
    }
}

impl Camera for ScreenCamera {
    fn capture_frame(&mut self) -> Result<Mat, CaptureError> {
        self.capturer.capture_store_frame()?;
        let frame = self
            .capturer
            .get_stored_frame()
            .expect("failed to retrieve frame");
        let (width, height) = self.capturer.geometry();
        let mat = opencv_helpers::slice_x11bgr_buffer_to_bgrf32_mat(frame, width, height);
        Ok(match self.scale_to {
            Some(scale) => opencv_helpers::resize(&mat, scale.0, scale.1, imgproc::INTER_NEAREST),
            None => mat,
        })
    }

    fn get_config_numeric(&mut self, _name: &str) -> Result<i32, CameraConfigError> {
        Err(CameraConfigError::from_str(
            "Cannot get numeric config for screen captures",
        ))
    }

    fn set_config_numeric(&mut self, _name: &str, _value: i32) -> Result<(), CameraConfigError> {
        Err(CameraConfigError::from_str(
            "Cannot set numeric config for screen captures",
        ))
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum CameraParams {
    Opencv {
        video_source: OpencvVideoSource,
    },
    Gphoto,
    Screen {
        capture_src: Option<usize>,
        scale_to: Option<(usize, usize)>,
    },
}

pub fn init_camera(camera_params: CameraParams) -> Result<Box<dyn Camera>, ()> {
    match camera_params {
        CameraParams::Gphoto => {
            let result = GphotoCamera::new().or(Err(()));
            match result {
                Ok(camera) => Ok(Box::new(camera)),
                Err(()) => Err(()),
            }
        }
        CameraParams::Opencv { video_source } => {
            // match video_source {
            //     OpencvVideoSource::DeviceId(_) => {}
            //     OpencvVideoSource::Filename(filename) => {
            //         // huge hack testing this...
            //         return Ok(Box::new(FfmpegVideo::new(&filename, 640, 480)));
            //     }
            // }
            match OpencvVideo::from_video_source(video_source) {
                Some(camera) => Ok(Box::new(camera)),
                None => Err(()),
            }
        }
        CameraParams::Screen {
            capture_src,
            scale_to,
        } => match ScreenCamera::from_settings(capture_src, scale_to) {
            Ok(camera) => Ok(Box::new(camera)),
            Err(msg) => {
                error!("Failed to create ScreenCamera: {}", msg);
                Err(())
            }
        },
    }
}
