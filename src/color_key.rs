use crate::color::{euclidean_distance, Hsv, Luv, Rgb};
use crate::math::partial_min;
use crate::opencv_helpers;
use opencv::core;
use opencv::core::Mat;
use serde::{Deserialize, Deserializer};
use std::fmt::Debug;

#[typetag::serde(tag = "type")]
pub trait ColorKey: Send + Debug {
    /// Approximate the perceptual similarity between this and a given RGB color
    ///
    /// The resulting distance will be an absolute value between 0 and 1,
    /// where 0 is least similar and 1 is most similar
    fn similarity(&self, other: Rgb) -> f32;

    fn as_rgb(&self) -> Rgb;

    fn box_clone(&self) -> Box<dyn ColorKey>;
}

impl Clone for Box<dyn ColorKey> {
    fn clone(&self) -> Box<dyn ColorKey> {
        self.box_clone()
    }
}

// --------------------------------------- UTILS -------------------------------------------

/// Create an 8-bit grayscale image from an input f32 BGR image
/// filtered by similarity to a given color.
pub fn color_similarity_filter(src: &mut Mat, key: HsvKey) -> Mat {
    let rows = src.size().unwrap().height;
    let cols = src.size().unwrap().width;
    let mut dest = unsafe { Mat::new_rows_cols(rows, cols, core::CV_8UC1).unwrap() };
    for y in 0..rows {
        for x in 0..cols {
            let src_f32_bgr_pixel =
                opencv_helpers::read_rgbf32_pixel_from_bgr_mat(src, x as usize, y as usize);
            let src_u8_rgb_pixel = Rgb(
                (src_f32_bgr_pixel.2 * 255.0) as u8,
                (src_f32_bgr_pixel.1 * 255.0) as u8,
                (src_f32_bgr_pixel.0 * 255.0) as u8,
            );
            let similarity = key.similarity(src_u8_rgb_pixel);
            unsafe { *dest.at_2d_mut_unchecked::<u8>(y, x).unwrap() = (similarity * 255.0) as u8 };
        }
    }
    dest
}

fn wrapping_difference(left: f32, right: f32, wrapping_length: f32) -> f32 {
    partial_min(
        (left - right).abs(),
        (left - (wrapping_length - right)).abs(),
    )
}

// ----------------------------------------- HSV ---------------------------------------------

#[derive(Serialize, Debug, Clone, Copy)]
pub struct HsvKey {
    pub h: f32,
    pub s: f32,
    pub v: f32,
    pub h_tolerance: f32,
    pub s_tolerance: f32,
    pub v_tolerance: f32,
    #[serde(skip)]
    max_distance: f32,
}

#[derive(Deserialize)]
struct IntermediateHsvKey {
    h: f32,
    s: f32,
    v: f32,
    h_tolerance: f32,
    s_tolerance: f32,
    v_tolerance: f32,
}

impl IntermediateHsvKey {
    fn compute_max_distance(&self) -> f32 {
        (self.h_tolerance.powi(2) + self.s_tolerance.powi(2) + self.v_tolerance.powi(2)).sqrt()
    }
}

impl HsvKey {
    pub fn new(
        h: f32,
        s: f32,
        v: f32,
        h_tolerance: f32,
        s_tolerance: f32,
        v_tolerance: f32,
    ) -> HsvKey {
        Self::from_intermediate_key(IntermediateHsvKey {
            h,
            s,
            v,
            h_tolerance,
            s_tolerance,
            v_tolerance,
        })
    }

    pub fn hsv(&self) -> Hsv {
        Hsv::new(self.h, self.s, self.v)
    }

    fn from_intermediate_key(intermediate_key: IntermediateHsvKey) -> HsvKey {
        HsvKey {
            max_distance: intermediate_key.compute_max_distance(),
            h: intermediate_key.h,
            s: intermediate_key.s,
            v: intermediate_key.v,
            h_tolerance: intermediate_key.h_tolerance,
            s_tolerance: intermediate_key.s_tolerance,
            v_tolerance: intermediate_key.v_tolerance,
        }
    }
}

impl<'de> Deserialize<'de> for HsvKey {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let intermediate_key = IntermediateHsvKey::deserialize(deserializer)?;
        Ok(Self::from_intermediate_key(intermediate_key))
    }
}

#[typetag::serde]
impl ColorKey for HsvKey {
    fn similarity(&self, other: Rgb) -> f32 {
        let other_hsv = Hsv::from(other);
        let h_distance_raw = wrapping_difference(other_hsv.h, self.h, 1.0);
        let s_distance_raw = other_hsv.s - self.s;
        let v_distance_raw = other_hsv.v - self.v;

        if h_distance_raw.abs() > self.h_tolerance
            || s_distance_raw.abs() > self.s_tolerance
            || v_distance_raw.abs() > self.v_tolerance
        {
            return 0.0;
        }

        let total_distance =
            (h_distance_raw.powi(2) + s_distance_raw.powi(2) + v_distance_raw.powi(2)).sqrt();

        let similarity = (total_distance / -self.max_distance) + 1.0;
        if !(similarity >= 0.0 && similarity <= 1.0) {
            panic!(
                "unexpected similarity result. color: {:?}, key: {:?}, similarity: {:?}",
                other, self, similarity
            );
        }
        similarity
    }

    fn as_rgb(&self) -> Rgb {
        Hsv::new(self.h, self.s, self.v).into()
    }

    fn box_clone(&self) -> Box<dyn ColorKey> {
        Box::new((*self).clone())
    }
}

#[cfg(test)]
mod test_hsv_key {
    extern crate test;
    use super::*;
    use crate::test_utils::*;

    #[test]
    fn test_zero_distance() {
        assert_almost_eq(
            HsvKey::new(0.1, 0.15, 0.2, 0.5, 0.5, 0.5).similarity(Hsv::new(0.1, 0.15, 0.20).into()),
            0.97918737, // Not quite 1.0 because of imprecise conversion
        );
    }

    #[test]
    fn test_black_and_white() {
        assert_almost_eq(
            HsvKey::new(0.0, 0.0, 0.0, 1.0, 1.0, 1.0).similarity(Hsv::new(0.0, 0.0, 1.0).into()),
            0.42264973081037416,
        );
    }

    #[test]
    fn test_tolerance_scaling() {
        let tolerance = 0.7;
        assert_almost_eq(
            HsvKey::new(0.2, 0.4, 0.5, tolerance, tolerance, tolerance)
                .similarity(Hsv::new(0.0, 0.0, 0.0).into()),
            0.4467166648275118,
        );
    }
}

// ----------------------------------------- LUV ---------------------------------------------

/// 0 <= L <= 100
/// -134 <= u <= 220
/// -140 <= v <= 122
const MAX_LUV_EUCLIDEAN_DISTANCE: f32 = 451.61932;

#[derive(Debug, Clone, Copy, Serialize)]
pub struct LuvKey {
    pub l: f32,
    pub u: f32,
    pub v: f32,
    pub tolerance: f32,
}

#[derive(Deserialize)]
pub struct IntermediateLuvKey {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub tolerance: f32,
}

impl LuvKey {
    fn from_intermediate_key(intermediate_key: IntermediateLuvKey) -> Self {
        let luv = Luv::from(Rgb(
            intermediate_key.r,
            intermediate_key.g,
            intermediate_key.b,
        ));
        LuvKey {
            l: luv.0,
            u: luv.1,
            v: luv.2,
            tolerance: intermediate_key.tolerance,
        }
    }
}

impl<'de> Deserialize<'de> for LuvKey {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let intermediate_key = IntermediateLuvKey::deserialize(deserializer)?;
        Ok(Self::from_intermediate_key(intermediate_key))
    }
}

#[typetag::serde]
impl ColorKey for LuvKey {
    fn similarity(&self, other: Rgb) -> f32 {
        debug_assert!(0.0 < self.tolerance && self.tolerance <= 1.0);
        let other_luv = Luv::from(other);
        let distance = euclidean_distance((self.l, self.u, self.v), other_luv.as_tuple())
            / MAX_LUV_EUCLIDEAN_DISTANCE;
        debug_assert!(0.0 <= distance && distance <= 1.0);
        if distance >= self.tolerance {
            0.0
        } else {
            let similarity_before_scale = 1.0 - distance;
            let result = (self.tolerance - distance) / self.tolerance;
            debug_assert!(result <= 1.0);
            result
        }
    }

    fn as_rgb(&self) -> Rgb {
        Luv(self.l, self.u, self.v).into()
    }

    fn box_clone(&self) -> Box<dyn ColorKey> {
        Box::new((*self).clone())
    }
}

#[cfg(test)]
mod test_luv_key {
    use super::*;
    extern crate test;
    use crate::test_utils::*;

    #[test]
    fn max_luv_distance() {
        let expected_max = euclidean_distance((0.0, -134.0, -140.0), (100.0, 220.0, 122.0));
        assert_almost_eq(expected_max, MAX_LUV_EUCLIDEAN_DISTANCE);
    }
}
