//! Image dispatch from live video feeds

use std::collections::HashMap;
use std::thread;
use std::time::Duration;

use stopwatch::Stopwatch;

use crate::camera::{init_camera, CameraParams};
use crate::constants::MAX_IMG_QUEUE_DEPTH;
use crate::control_messages::ImgDispatcherControlMessage;
use crate::expiring_mpsc::{Receiver, Sender, TryRecvError};
use crate::heartbeat::Heartbeat;
use crate::ids::LayerId;
use crate::img_dispatch::{ImgDispatcher, LayerDispatcher};
use crate::live_img_preview::ImgPacket;

/// An image dispatcher which streams image data from a `Camera`.
pub struct LiveImgDispatcher {
    time_between_frames: Duration,
    layer_dispatchers: HashMap<LayerId, LayerDispatcher>,
    camera_params: CameraParams,
}

impl LiveImgDispatcher {
    pub fn new(fps: f32, camera_params: CameraParams) -> LiveImgDispatcher {
        let time_between_frames = Duration::from_millis((1000.0 / fps) as u64);
        let layer_dispatchers = HashMap::new();
        LiveImgDispatcher {
            time_between_frames,
            layer_dispatchers,
            camera_params,
        }
    }

    fn disconnect_layer(&mut self, id: LayerId) {
        self.layer_dispatchers.remove(&id);
    }

    fn connect_layer(&mut self, id: LayerId, layer_dispatcher: LayerDispatcher) {
        self.layer_dispatchers.insert(id, layer_dispatcher);
    }
}

impl ImgDispatcher for LiveImgDispatcher {
    fn begin_dispatch(
        &mut self,
        control_channel: Receiver<ImgDispatcherControlMessage>,
        preview_sender: Option<Sender<ImgPacket>>,
    ) {
        let mut camera = init_camera(self.camera_params.clone()).unwrap();
        let mut heartbeat =
            Heartbeat::new(Duration::from_secs(5), "live image dispatch".to_string());
        let mut sw = Stopwatch::start_new();
        let frame_ttl = self.time_between_frames * 10;
        loop {
            heartbeat.mark();
            match control_channel.try_recv() {
                Ok(message) => match message {
                    ImgDispatcherControlMessage::Shutdown => {
                        info!("Received shutdown signal. Shutting down.");
                        break;
                    }
                    ImgDispatcherControlMessage::DisconnectLayer { id } => {
                        self.disconnect_layer(id);
                    }
                    ImgDispatcherControlMessage::ConnectLayer {
                        id,
                        layer_dispatcher,
                    } => {
                        self.connect_layer(id, layer_dispatcher);
                    }
                    ImgDispatcherControlMessage::SetNumericCameraConfig { name, value } => {
                        match camera.set_config_numeric(&name, value) {
                            Err(e) => {
                                error!("Got error when setting numeric config: {:?}", e);
                            }
                            _ => {}
                        };
                    }
                },
                Err(e) => match e {
                    TryRecvError::Empty => (),
                    TryRecvError::Disconnected => {
                        break;
                    }
                },
            };
            match camera.capture_frame() {
                Ok(mut frame) => {
                    for layer_dispatcher in self.layer_dispatchers.values() {
                        // TODO tweaking the duration of the packet
                        //      here may help decrease latency
                        layer_dispatcher.dispatch_layer(
                            &mut frame,
                            self.time_between_frames,
                            Some(frame_ttl),
                        );
                    }
                    if let Some(preview_dest) = &preview_sender {
                        match preview_dest.send_if_depth_below(
                            ImgPacket::from_mat(frame),
                            frame_ttl,
                            MAX_IMG_QUEUE_DEPTH,
                        ) {
                            Err(_) => {
                                error!("Got error while sending preview packet");
                            }
                            Ok(maybe_rejected_packet) => match maybe_rejected_packet {
                                Some(_) => {
                                    warn!("Could not send packet because queue was backed up.");
                                }
                                None => (),
                            },
                        };
                    }
                    if let Some(sleep_dur) = self.time_between_frames.checked_sub(sw.elapsed()) {
                        thread::sleep(sleep_dur);
                    }
                }
                Err(e) => {
                    error!("Got error while capturing frame from camera: {:?}", e);
                    thread::sleep(self.time_between_frames);
                    info!("attempting to reconnect camera");
                    match init_camera(self.camera_params.clone()) {
                        Ok(new_camera) => {
                            info!("successfully reconnected with camera");
                            camera = new_camera;
                        }
                        Err(_) => {
                            warn!("failed to reconnect with camera");
                        }
                    }
                }
            }
            sw.restart();
        }
    }
}
