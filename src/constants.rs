use std::time::Duration;

pub const MAX_IMG_QUEUE_DEPTH: isize = 15;
pub const DEFAULT_FRAME_TTL: Duration = Duration::from_millis(3_000);

pub const TEMP_HARD_CODED_MAX_AMP: f32 = 300f32;
