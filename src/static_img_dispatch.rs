use std::collections::HashMap;
use std::path::Path;
use std::thread;
use std::thread::JoinHandle;
use std::time::{Duration, SystemTime};

use opencv;
use opencv::core;
use opencv::core::Mat;
use opencv::highgui;

use ndarray::prelude::*;

use control_messages::ImgDispatcherControlMessage;
use expiring_mpsc::{channel, Receiver, Sender};
use img_dispatch::{ImgDispatcher, LayerDispatcher};
use live_img_preview::ImgPacket;
use opencv_helpers::{convert_u8_img_to_f32};

pub struct StaticImgDispatcher {
    img_path: String,
    img_height: usize,
    chunk_width: u16,
    chunk_duration: Duration,
}

impl StaticImgDispatcher {
    pub fn create(
        img_path: String,
        chunk_width: u16,
        chunk_duration: Duration,
    ) -> StaticImgDispatcher {
        let img_height = Self::determine_img_height(&img_path);
        StaticImgDispatcher {
            img_path,
            img_height,
            chunk_width,
            chunk_duration,
        }
    }

    fn determine_img_height(img_path: &String) -> usize {
        // Huge hack - could do this just by reading image metadata too
        let img = highgui::imread(&img_path, highgui::CV_LOAD_IMAGE_COLOR).unwrap();
        img.size().unwrap().height as usize
    }

    fn dispatch_slice(
        img: &Mat,
        layer_dispatchers: &Vec<LayerDispatcher>,
        start_x: u16,
        width: u16,
        chunk_duration: Duration,
    ) {
        let img_height = img.size().unwrap().height;
        let row_range = &core::Range::new(0, img_height).unwrap();
        let col_range = &core::Range::new(start_x as i32, (start_x + width) as i32).unwrap();
        let mut slice = Mat::rowscols(img, row_range, col_range).unwrap();
        for layer_dispatcher in layer_dispatchers {
            layer_dispatcher.dispatch_layer(&mut slice, chunk_duration, None);
        }
    }

    fn img_height(&self) -> usize {
        self.img_height
    }
}

/// TODO rework static image dispatch as a form of live image dispatch
///      by feeding image columns as if they were coming from a camera

impl ImgDispatcher for StaticImgDispatcher {
    /// Send chunks of image data through layers until the image is fully consumed.
    fn begin_dispatch(
        &mut self,
        control_channel: Receiver<ImgDispatcherControlMessage>,
        layer_dispatchers: &Vec<LayerDispatcher>,
        preview_sender: Option<Sender<ImgPacket>>,
    ) {
        if preview_sender.is_some() {
            warn!(
                "StaticImgDispatcher got a preview sender, \
                 but preview on static images is not currently supported."
            );
        }
        let mut u8_img = highgui::imread(&self.img_path, highgui::CV_LOAD_IMAGE_COLOR).unwrap();
        let img = convert_u8_img_to_f32(&mut u8_img, 1.0, 0.0);
        let mut current_x = 0;
        let chunk_width = self.chunk_width;
        let img_width = img.size().unwrap().width as u16;
        loop {
            match control_channel.try_recv() {
                Ok(message) => match message {
                    ImgDispatcherControlMessage::Shutdown => {
                        info!("Received shutdown signal. Shutting down.");
                        break;
                    }
                },
                Err(_) => (),
            };
            if current_x + self.chunk_width >= img_width {
                Self::dispatch_slice(
                    &img,
                    layer_dispatchers,
                    current_x,
                    img_width - current_x,
                    self.chunk_duration,
                );
                info!("Finished dispatching image slices");
                break;
            } else {
                Self::dispatch_slice(
                    &img,
                    layer_dispatchers,
                    current_x,
                    chunk_width,
                    self.chunk_duration,
                );
                current_x += chunk_width;
            }
        }
    }
}
