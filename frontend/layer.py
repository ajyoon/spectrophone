from abc import ABC, abstractmethod
from enum import Enum, auto
import time
import re

import jsons

import pitch


MIN_AUDIBLE_FREQ = 20
MAX_AUDIBLE_FREQ = 20_000


class ColorKey(ABC):
    @property
    @abstractmethod
    def rust_type(self):
        pass


class HsvKey(ColorKey):
    rust_type = 'HsvKey'

    def __init__(self, h, s, v, h_tolerance, s_tolerance, v_tolerance):
        """
        Color dimensions and tolerances are specified in:
        - for hue: degrees out of 360
        - for saturation and value: percent out of 100

        Note that all channels are scaled to a range of 0-1 when sent
        to the server
        """
        self.h = h
        self.s = s
        self.v = v
        self.h_tolerance = h_tolerance
        self.s_tolerance = s_tolerance
        self.v_tolerance = v_tolerance

    def _serialize(obj, **kwargs):
        return {
            'type': obj.rust_type,
            'h': obj.h / 360,
            's': obj.s / 100,
            'v': obj.v / 100,
            'h_tolerance': obj.h_tolerance / 360,
            's_tolerance': obj.s_tolerance / 100,
            'v_tolerance': obj.v_tolerance / 100
        }


class LuvKey(ColorKey):
    """
    A key for use this with perceptual LUV color space.

    Note that for ease of use, the key color is given in RGB,
    and converted to LUV on the server.
    """

    rust_type = 'LuvKey'

    def __init__(self, r, g, b, tolerance):
        self.r = r
        self.g = g
        self.b = b
        self.tolerance = tolerance

    def _serialize(obj, **kwargs):
        return {
            'type': obj.rust_type,
            'r': obj.r,
            'g': obj.g,
            'b': obj.b,
            'tolerance': obj.tolerance
        }


jsons.set_serializer(HsvKey._serialize, HsvKey)
jsons.set_serializer(LuvKey._serialize, LuvKey)


class Waveform(Enum):
    Sine = auto()
    Square = auto()
    Triangle = auto()
    Sawtooth = auto()


class Voice:
    def __init__(self, waveform, frequency, amplitude_factor=1):
        self.waveform = (waveform if isinstance(waveform, Waveform)
                         else Waveform[waveform])
        self.frequency = frequency
        self.amplitude_factor = amplitude_factor


class Layer:
    def __init__(self,
                 key,
                 voices,
                 horizontal_regions=5,
                 highest_similarity_region_weight=0.65,
                 sort_voices=True,
                 name=None):
        self.key = Layer._resolve_key(key)
        resolved_voices = Layer._resolve_voices(voices)
        self.voices = Layer.filter_voices(Layer.sort_voices(resolved_voices)
                                          if sort_voices else resolved_voices)
        self.horizontal_regions = horizontal_regions
        self.highest_similarity_region_weight = (
            highest_similarity_region_weight)
        self.name = name if name else Layer.default_name(self.key)

    @staticmethod
    def _resolve_voices(voices):
        resolved_voices = []
        for voice in voices:
            if isinstance(voice, tuple):
                resolved_voices.append(Voice(*voice))
            elif isinstance(voice, Voice):
                resolved_voices.append(voice)
            elif isinstance(voice, dict):
                resolved_voices.append(Voice(**voice))
            else:
                raise ValueError(f"Can't construct voice from: {voice}")
        return resolved_voices

    @staticmethod
    def _resolve_key(key):
        if isinstance(key, (HsvKey, LuvKey)):
            return key
        elif isinstance(key, tuple):
            if len(key) == 6:
                return HsvKey(*key)
            elif len(key) == 4:
                return LuvKey(*key)
        raise ValueError(f"Can't construct ColorKey from {key}")

    @staticmethod
    def sort_voices(voices):
        return sorted(voices, key=lambda v: v.frequency)

    @staticmethod
    def filter_voices(voices):
        return [v for v in voices
                if v.frequency >= MIN_AUDIBLE_FREQ
                and v.frequency <= MAX_AUDIBLE_FREQ]

    @staticmethod
    def default_name(key):
        return f'layer ({key.h}, {key.s}, {key.v})'

    def as_json(self):
        print(jsons.dump(self))
        return jsons.dump(self)


class Interpolation(Enum):
    Linear = auto()
    SquareRoot = auto()


class Transition:
    def __init__(self, start_timestamp, duration_ms, interpolation):
        self.start_timestamp = start_timestamp
        self.duration_ms = duration_ms
        self.interpolation = interpolation

    def as_json(self):
        return jsons.dump(self)


def now():
    return int(time.time())


_QUICK_VOICE_RE = re.compile(r"^(?P<waveform>[nqtw])(?P<pitch>.+?)(?P<amp>\d+)$")


def quick_voices(voice_specifier, amp_mul=1, tuning=None):
    specifiers = voice_specifier.split()
    voices = []
    for s in specifiers:
        match = _QUICK_VOICE_RE.match(s)
        if match is None:
            print("skipping invalid voice specifier: " + s)
            continue
        (waveform_spec, pitch_spec, amp_spec) = match.groups()
        if waveform_spec == 'n':
            resolved_waveform = Waveform.Sine
        elif waveform_spec == 'q':
            resolved_waveform = Waveform.Square
        elif waveform_spec == 't':
            resolved_waveform = Waveform.Triangle
        elif waveform_spec == 'w':
            resolved_waveform = Waveform.Sawtooth
        else:
            raise RuntimeError("should not be reachable")
        resolved_freq = pitch.lilyfreq(pitch_spec, tuning)
        resolved_amp = float(amp_spec) * amp_mul
        voices.append((resolved_waveform, resolved_freq, resolved_amp))
    return voices
