use captrs::Bgr8;
use opencv::core;
use opencv::core::Mat;
use opencv::imgproc;
use opencv::videoio;
use opencv::videoio::VideoCapture;

use crate::color::{Luv, Rgb, RgbF32};

pub fn convert_u8_img_to_f32(src: &mut Mat, extra_factor: f32, extra_shift: f32) -> Mat {
    debug_assert_eq!(src.typ().unwrap(), core::CV_8UC3);
    let size = src.size().unwrap();
    let mut dst = unsafe { Mat::new_rows_cols(size.height, size.width, core::CV_32FC3).unwrap() };
    src.convert_to(
        &mut dst,
        core::CV_32FC3,
        extra_factor as f64 * (1.0 / 255.0),
        extra_shift as f64,
    )
    .unwrap();
    dst
}

pub fn convert_f32_img_to_u8(src: &mut Mat) -> Mat {
    debug_assert_eq!(src.typ().unwrap(), core::CV_32FC3);
    let size = src.size().unwrap();
    let mut dst = unsafe { Mat::new_rows_cols(size.height, size.width, core::CV_8UC3).unwrap() };
    src.convert_to(&mut dst, core::CV_8UC3, 255.0, 0.0).unwrap();
    dst
}

pub fn write_rgbf32_pixel_to_bgr_mat(mat: &mut Mat, x: usize, y: usize, pixel: RgbF32) {
    debug_assert_eq!(mat.typ().unwrap(), core::CV_32FC3);
    unsafe {
        *mat.at_2d_mut_unchecked::<core::Vec3f>(y as i32, x as i32)
            .unwrap() = core::Vec3f::from([pixel.2, pixel.1, pixel.0]);
    }
}

pub fn read_rgbf32_pixel_from_bgr_mat(mat: &mut Mat, x: usize, y: usize) -> RgbF32 {
    debug_assert_eq!(mat.typ().unwrap(), core::CV_32FC3);
    unsafe {
        let src = *mat
            .at_2d_mut_unchecked::<core::Vec3f>(y as i32, x as i32)
            .unwrap();
        (src[2], src[1], src[0])
    }
}

pub fn write_rgbu8_pixel_to_bgrf32_mat(mat: &mut Mat, x: usize, y: usize, pixel: Rgb) {
    debug_assert_eq!(mat.typ().unwrap(), core::CV_32FC3);
    unsafe {
        *mat.at_2d_mut_unchecked::<core::Vec3f>(y as i32, x as i32)
            .unwrap() = core::Vec3f::from([
            pixel.2 as f32 / 255.0,
            pixel.1 as f32 / 255.0,
            pixel.0 as f32 / 255.0,
        ]);
    }
}

pub fn sum_mats(mut mats: Vec<Mat>) -> Mat {
    debug_assert!(!mats.is_empty(), "At least 1 Mat is required");
    let size = mats.get(0).unwrap().size().unwrap();
    let mask = Mat::default().unwrap(); // No mask
    let mut summed = mats.remove(0);
    for mat in mats {
        debug_assert_eq!(mat.typ().unwrap(), core::CV_32FC3);
        let mut output =
            unsafe { Mat::new_rows_cols(size.height, size.width, core::CV_32FC3).unwrap() };
        core::add(&summed, &mat, &mut output, &mask, core::CV_32FC3).unwrap();
        summed = output;
    }
    summed
}

#[allow(dead_code)]
pub fn resize(mat: &Mat, width: usize, height: usize, mode: i32) -> Mat {
    let mut output_image = Mat::default().unwrap();
    let output_size = core::Size {
        width: width as i32,
        height: height as i32,
    };
    imgproc::resize(&mat, &mut output_image, output_size, 0.0, 0.0, mode).unwrap();
    output_image
}

pub fn combine_columns(mats: Vec<Mat>) -> Mat {
    debug_assert!(!mats.is_empty(), "At least 1 Mat is required");
    let height = mats.get(0).unwrap().size().unwrap().height;
    let width = mats.len() as i32;
    let mut combined = unsafe { Mat::new_rows_cols(height, width, core::CV_32FC3).unwrap() };
    for (x, mut column) in mats.into_iter().enumerate() {
        debug_assert_eq!(column.typ().unwrap(), core::CV_32FC3);
        debug_assert_eq!(column.size().unwrap().height, height);
        debug_assert_eq!(column.size().unwrap().width, 1);
        for y in 0..height {
            write_rgbf32_pixel_to_bgr_mat(
                &mut combined,
                x,
                y as usize,
                read_rgbf32_pixel_from_bgr_mat(&mut column, 0, y as usize),
            );
        }
    }
    combined
}

/// Convert a 3-channel f32 Mat to a 2D Vec, where the outer vec is of rows.
#[allow(unused)]
pub fn mat_to_vec_f32(mat: Mat) -> Vec<Vec<(f32, f32, f32)>> {
    debug_assert_eq!(mat.typ().unwrap(), core::CV_32FC3);
    let mut rows = vec![];
    let size = mat.size().unwrap();
    for y in 0..size.height {
        let mut row = vec![];
        for x in 0..size.width {
            unsafe {
                let src = *mat
                    .at_2d_unchecked::<core::Vec3f>(y as i32, x as i32)
                    .unwrap();
                row.push((src[0], src[1], src[2]));
            }
        }
        rows.push(row)
    }
    rows
}

/// Convert a 3-channel u8 Mat to a 2D Vec, where the outer vec is of rows.
pub fn mat_32fc3_to_vec_u8(mat: Mat) -> Vec<Vec<(u8, u8, u8)>> {
    debug_assert_eq!(mat.typ().unwrap(), core::CV_32FC3);
    let mut rows = vec![];
    let size = mat.size().unwrap();
    for y in 0..size.height {
        let mut row = vec![];
        for x in 0..size.width {
            unsafe {
                let pixel = *mat.at_2d_unchecked::<core::Vec3f>(y, x).unwrap();
                row.push((
                    (pixel[0] * 255.0) as u8,
                    (pixel[1] * 255.0) as u8,
                    (pixel[2] * 255.0) as u8,
                ));
            }
        }
        rows.push(row)
    }
    rows
}

pub fn vec_bgr_buffer_to_mat(vec: Vec<u8>, width: usize, height: usize) -> Mat {
    let mut buffer_idx = 0;
    let mut mat =
        unsafe { Mat::new_rows_cols(height as i32, width as i32, core::CV_8UC3).unwrap() };
    for y in 0..(height as i32) {
        for x in 0..(width as i32) {
            unsafe {
                *mat.at_2d_mut_unchecked::<core::Vec3b>(y, x).unwrap() =
                    core::Vec3b::from([vec[buffer_idx], vec[buffer_idx + 1], vec[buffer_idx + 2]]);
                buffer_idx += 3;
            }
        }
    }
    mat
}

pub fn slice_x11bgr_buffer_to_bgrf32_mat(slice: &[Bgr8], width: u32, height: u32) -> Mat {
    let mut buffer_idx = 0;
    let mut mat =
        unsafe { Mat::new_rows_cols(height as i32, width as i32, core::CV_32FC3).unwrap() };
    for y in 0..(height as i32) {
        for x in 0..(width as i32) {
            unsafe {
                let pixel = slice[buffer_idx];
                *mat.at_2d_mut_unchecked::<core::Vec3f>(y, x).unwrap() = core::Vec3f::from([
                    pixel.b as f32 / 255.0,
                    pixel.g as f32 / 255.0,
                    pixel.r as f32 / 255.0,
                ]);
                buffer_idx += 1;
            }
        }
    }
    mat
}

pub fn convert_luv_to_rgb(luv: &Luv) -> Rgb {
    let mut luv_mat = unsafe { Mat::new_rows_cols(1, 1, core::CV_32FC3).unwrap() };
    unsafe {
        *luv_mat.at_2d_mut_unchecked::<core::Vec3f>(0, 0).unwrap() =
            core::Vec3f::from([luv.0, luv.1, luv.2]);
    }
    let mut rgb_mat = unsafe { Mat::new_rows_cols(1, 1, core::CV_32FC3).unwrap() };
    imgproc::cvt_color(&luv_mat, &mut rgb_mat, imgproc::COLOR_Luv2RGB, 0).unwrap();
    debug_assert_eq!(rgb_mat.typ().unwrap(), core::CV_32FC3);
    unsafe {
        let src = *rgb_mat.at_2d_mut_unchecked::<core::Vec3f>(0, 0).unwrap();
        Rgb(
            (src[0] * 255.0) as u8,
            (src[1] * 255.0) as u8,
            (src[2] * 255.0) as u8,
        )
    }
}

pub fn log_video_capture_debug_info(video_capture: &mut VideoCapture) {
    debug!(
        "VideoCapture CV_CAP_PROP_POS_MSEC: {}",
        video_capture
            .get(videoio::CAP_PROP_POS_MSEC)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_POS_FRAMES: {}",
        video_capture
            .get(videoio::CAP_PROP_POS_FRAMES)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_POS_AVI_RATIO: {}",
        video_capture
            .get(videoio::CAP_PROP_POS_AVI_RATIO)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_FRAME_WIDTH: {}",
        video_capture
            .get(videoio::CAP_PROP_FRAME_WIDTH)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_FRAME_HEIGHT: {}",
        video_capture
            .get(videoio::CAP_PROP_FRAME_HEIGHT)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_FPS: {}",
        video_capture.get(videoio::CAP_PROP_FPS).unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_FOURCC: {}",
        video_capture
            .get(videoio::CAP_PROP_FOURCC)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_FRAME_COUNT: {}",
        video_capture
            .get(videoio::CAP_PROP_FRAME_COUNT)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_FORMAT: {}",
        video_capture
            .get(videoio::CAP_PROP_FORMAT)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_MODE: {}",
        video_capture.get(videoio::CAP_PROP_MODE).unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_BRIGHTNESS: {}",
        video_capture
            .get(videoio::CAP_PROP_BRIGHTNESS)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_CONTRAST: {}",
        video_capture
            .get(videoio::CAP_PROP_CONTRAST)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_SATURATION: {}",
        video_capture
            .get(videoio::CAP_PROP_SATURATION)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_HUE: {}",
        video_capture.get(videoio::CAP_PROP_HUE).unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_GAIN: {}",
        video_capture.get(videoio::CAP_PROP_GAIN).unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_EXPOSURE: {}",
        video_capture
            .get(videoio::CAP_PROP_EXPOSURE)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_CONVERT_RGB: {}",
        video_capture
            .get(videoio::CAP_PROP_CONVERT_RGB)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_WHITE_BALANCE_BLUE_U: {}",
        video_capture
            .get(videoio::CAP_PROP_WHITE_BALANCE_BLUE_U)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_WHITE_BALANCE_RED_V: {}",
        video_capture
            .get(videoio::CAP_PROP_WHITE_BALANCE_RED_V)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_RECTIFICATION: {}",
        video_capture
            .get(videoio::CAP_PROP_RECTIFICATION)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_ISO_SPEED: {}",
        video_capture
            .get(videoio::CAP_PROP_ISO_SPEED)
            .unwrap_or(-9999.0)
    );
    debug!(
        "VideoCapture CV_CAP_PROP_BUFFERSIZE: {}",
        video_capture
            .get(videoio::CAP_PROP_BUFFERSIZE)
            .unwrap_or(-9999.0)
    );
}

pub fn log_opencv_build_info() {
    match core::get_build_information() {
        Ok(build_info) => {
            debug!("Opencv build info: {}", build_info);
        }
        Err(e) => {
            debug!("Failed to look up opencv build info: {}", e);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::test_utils::*;

    fn single_pixel_mat(color: RgbF32) -> Mat {
        let mut mat = unsafe { Mat::new_rows_cols(1, 1, core::CV_32FC3).unwrap() };
        write_rgbf32_pixel_to_bgr_mat(&mut mat, 0, 0, color);
        mat
    }

    #[test]
    fn test_read_write_pixel() {
        let mut mat = unsafe { Mat::new_rows_cols(4, 3, core::CV_32FC3).unwrap() };
        write_rgbf32_pixel_to_bgr_mat(&mut mat, 2, 3, (0.1, 0.2, 0.3));
        assert_almost_eq_3_tuple(
            read_rgbf32_pixel_from_bgr_mat(&mut mat, 2, 3),
            (0.1, 0.2, 0.3),
        );
    }

    #[test]
    fn test_sum_mats() {
        let mat_1 = single_pixel_mat((0.1, 0.2, 0.3));
        let mat_2 = single_pixel_mat((0.11, 0.12, 0.13));
        let mut sum = sum_mats(vec![mat_1, mat_2]);
        assert_almost_eq_3_tuple(
            read_rgbf32_pixel_from_bgr_mat(&mut sum, 0, 0),
            (0.21, 0.32, 0.43),
        );
    }

    #[test]
    fn test_resize() {
        let mat = single_pixel_mat((0.1, 0.2, 0.3));
        let mut resized = resize(&mat, 2, 2, imgproc::INTER_NEAREST);
        assert_almost_eq_3_tuple(
            read_rgbf32_pixel_from_bgr_mat(&mut resized, 0, 0),
            (0.1, 0.2, 0.3),
        );
        assert_almost_eq_3_tuple(
            read_rgbf32_pixel_from_bgr_mat(&mut resized, 0, 1),
            (0.1, 0.2, 0.3),
        );
        assert_almost_eq_3_tuple(
            read_rgbf32_pixel_from_bgr_mat(&mut resized, 1, 0),
            (0.1, 0.2, 0.3),
        );
        assert_almost_eq_3_tuple(
            read_rgbf32_pixel_from_bgr_mat(&mut resized, 1, 1),
            (0.1, 0.2, 0.3),
        );
    }

    #[test]
    fn test_combine_columns() {
        let mat_1 = single_pixel_mat((0.1, 0.2, 0.3));
        let mat_2 = single_pixel_mat((0.11, 0.12, 0.13));
        let mut combined = combine_columns(vec![mat_1, mat_2]);
        assert_almost_eq_3_tuple(
            read_rgbf32_pixel_from_bgr_mat(&mut combined, 0, 0),
            (0.1, 0.2, 0.3),
        );
        assert_almost_eq_3_tuple(
            read_rgbf32_pixel_from_bgr_mat(&mut combined, 1, 0),
            (0.11, 0.12, 0.13),
        );
    }

    #[test]
    fn test_mat_to_vec_f32() {
        let mut mat = unsafe { Mat::new_rows_cols(3, 2, core::CV_32FC3).unwrap() };
        write_rgbf32_pixel_to_bgr_mat(&mut mat, 0, 0, (0., 0., 0.));
        write_rgbf32_pixel_to_bgr_mat(&mut mat, 1, 0, (1., 1., 1.));
        write_rgbf32_pixel_to_bgr_mat(&mut mat, 0, 1, (2., 2., 2.));
        write_rgbf32_pixel_to_bgr_mat(&mut mat, 1, 1, (3., 3., 3.));
        write_rgbf32_pixel_to_bgr_mat(&mut mat, 0, 2, (4., 4., 4.));
        write_rgbf32_pixel_to_bgr_mat(&mut mat, 1, 2, (5., 5., 5.));

        let vec = mat_to_vec_f32(mat);
        assert_eq!(
            vec,
            vec![
                vec![(0., 0., 0.), (1., 1., 1.)],
                vec![(2., 2., 2.), (3., 3., 3.)],
                vec![(4., 4., 4.), (5., 5., 5.)],
            ]
        );
    }
}
